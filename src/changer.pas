unit changer;

interface

uses
  Windows, Messages, INIFILES, StdCtrls, Controls, Grids, DBGrids, DB,
  ADODB, Classes,SysUtils, Variants, Graphics,  Forms,
  Dialogs, XPMan;

type
  TFChanger = class(TForm)
    ADOConnection1: TADOConnection;
    ADOQuery1: TADOQuery;
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    Label1: TLabel;
    Label2: TLabel;
    edadmin: TEdit;
    edMember: TEdit;
    Button1: TButton;
    edIPserver: TEdit;
    edPortServer: TEdit;
    Edinterval: TEdit;
    edRent: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    RBT: TRadioButton;
    RBF: TRadioButton;
    EdChat: TEdit;
    Label8: TLabel;
    XPManifest1: TXPManifest;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
  procedure connectAdo;
    { Public declarations }
  end;

var
  FChanger: TFChanger;
  interval,//chatServerPort,
  JinakMode, chatClientPort,
  DefaultServerPort2,
  DefaultServerPort,interval2:integer;
  defaultIP,defaultIP2:string[15];
  defaultPassword,defaultPassword2:string;
  jamrental:ttime;
  path:string;


function EnDeCrypt(const Value : String) : String;

implementation

{$R *.dfm}
procedure TFChanger.connectAdo;
begin
 ADOConnection1.Connected:=false;

 ADOConnection1.ConnectionString:='Provider=Microsoft.Jet.OLEDB.4.0;'+'Data Source='+ExtractFilePath(Application.ExeName)+'\db.mdb;'+'Persist Security Info=False';

ADOConnection1.Connected:=true;
end;

procedure TFChanger.FormCreate(Sender: TObject);
var appIni:TInifile;
begin
connectAdo;
ADOQuery1.Active:=true;
path:=ExtractFilePath(Application.ExeName);

appINI := TIniFile.Create(path+'\svchostx.ini') ;
    try
      defaultIP2:=appIni.ReadString('IP','IPAddress2','192.168.1.2');
      defaultserverport2:=appini.ReadInteger('Port','ServerPort2',7767);
      interval2:=300;
      //appini.ReadInteger('Interval','Interval2',300);
      defaultIP:=appIni.ReadString('IP','IPAddress','192.168.1.2');
      defaultserverport:=appini.ReadInteger('Port','ServerPort',7676);
      //chatServerPort:=appini.ReadInteger('Port','ChatServerPort',200);
      chatClientPort:=appini.ReadInteger('Port','ChatClientPort',300);
      JinakMode:=appini.ReadInteger('Mode','Jinak',0);
      Jamrental:=strtoTime(appini.ReadString('JamRental','Sebelum','18:00:00'));
      interval:=appini.ReadInteger('Interval','Interval',10);
      defaultPassword:=EnDeCrypt(appini.ReadString('Password','admin','123'));
      defaultPassword2:=EnDeCrypt(appini.ReadString('Password','user','456'));
      //chatServerPort:=330;
    finally
      appINI.Free;
    end;

if JinakMode=1 then
     RBT.Checked:=true
    else RBF.Checked:=true;
end;

function EnDeCrypt(const Value : String) : String;
var
  CharIndex : integer;
begin
  Result := Value;
  for CharIndex := 1 to Length(Value) do
    Result[CharIndex] := chr(not(ord(Value[CharIndex])));
end;

procedure TFChanger.FormShow(Sender: TObject);
begin
edadmin.Text:=defaultPassword;
edMember.Text:=defaultPassword2;
edIPserver.Text:=defaultIP;
edPortServer.Text:=inttostr(DefaultServerPort);
Edinterval.Text:=inttostr(interval);
edRent.Text:=timeTostr(jamrental);
EdChat.Text:=inttostr(chatClientPort); //EnDeCrypt()
end;

procedure TFChanger.Button1Click(Sender: TObject);
var appini:TIniFile;
begin
defaultPassword:=edadmin.Text;
defaultPassword2:=edMember.Text;
defaultIP:=edIPserver.Text;
DefaultServerPort:=STRTOINT(edPortServer.Text);
Interval:=StrToInt(Edinterval.Text);
jamrental:=strTotime(edRent.Text);
chatClientPort:=strtoint(EdChat.Text); //EnDeCrypt()

if RBT.Checked then
    JinakMode:=1
else JinakMode:=0;


  appINI := TIniFile.Create(path+'\svchostx.ini') ;
     try
      appIni.writeString('IP','IPAddress',defaultIP);
      appini.writeInteger('Port','ServerPort',defaultserverport);
      appini.writeInteger('Interval','Interval',interval);
      appIni.writeString('IP','IPAddress2',defaultIP2);
      appini.writeInteger('Port','ServerPort2',defaultserverport2);
      appini.writeInteger('Interval','Interval2',interval2);
      appini.writeInteger('Mode','Jinak',JinakMode);
      appini.writeInteger('Port','ChatClientPort',chatClientPort);
      appini.writeString('Password','admin',EnDeCrypt(defaultPassword));
      appini.writeString('Password','user',EnDeCrypt(defaultPassword2));
      appini.writeString('JamRental','Sebelum',timetostr(jamrental));

    finally
      appINI.Free;
    end;
close;
end;

end.
