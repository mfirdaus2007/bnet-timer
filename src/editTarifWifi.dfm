object FWifi: TFWifi
  Left = 387
  Top = 261
  Width = 527
  Height = 324
  Caption = 'Edit Tarif Wifi'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 0
    Top = 33
    Width = 519
    Height = 257
    Align = alClient
    DataSource = DM1.DSTarifWifi
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'jam'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Keterangan'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'reguler'
        Width = 68
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'anggota'
        Visible = True
      end>
  end
  object DBNavigator1: TDBNavigator
    Left = 0
    Top = 0
    Width = 519
    Height = 33
    DataSource = DM1.DSTarifWifi
    Align = alTop
    TabOrder = 1
  end
end
