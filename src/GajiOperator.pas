unit GajiOperator;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, DB, ADODB, StdCtrls, Buttons, ComCtrls;

type
  TFgaji = class(TForm)
    ComboBox1: TComboBox;
    DBGrid1: TDBGrid;
    BtOK: TBitBtn;
    BtPrint: TBitBtn;
    BtClose: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    dtpAwal: TDateTimePicker;
    dtpAkhir: TDateTimePicker;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    BitBtn1: TBitBtn;
    procedure FormShow(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtPrintClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BitBtn1Click(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Fgaji: TFgaji;
  bulan, tanggal, tahun:word;
implementation

uses reportgaji, MainForm, DMserver, rekapGaji;

{$R *.dfm}

procedure TFgaji.FormShow(Sender: TObject);
begin
decodedate(date,tahun,bulan, tanggal);
if bulan-1=0 then
 begin
   bulan:=12;
   tahun:=tahun-1;
   dtpAwal.Date:=EncodeDate(tahun,bulan,1);
   dtpAkhir.Date:=EncodeDate(tahun+1,1,1);
 end
 else
  begin
     bulan:=bulan-1;
     dtpAwal.Date:=EncodeDate(tahun,bulan,1);
     dtpAkhir.Date:=EncodeDate(tahun,bulan+1,1);
  end;

 dm1.qumum.Close;
 dm1.qumum.SQL.Text:='Select * from Operator where aktif='+QuotedStr('aktif');
 dm1.qumum.Open;
 ComboBox1.Items.Clear;
 ComboBox1.Text:='';
 while not (dm1.qumum.Eof) do
  begin
    ComboBox1.Items.Add(dm1.qumum['ID']+'. '+dm1.qumum['Nama']);
    dm1.qumum.Next;
  end;


end;

procedure TFgaji.BtOKClick(Sender: TObject);
var temp : string;
total : double;

begin
if (ComboBox1.Text='') then exit;


temp:= combobox1.Text;
    dm1.QREpGajiOp.Close;
    dm1.QREpGajiOp.Parameters.ParamByName('awal').Value:= dtpAwal.Date-1;//strtodate(bulandepan);
    dm1.QREpGajiOp.Parameters.ParamByName('akhir').Value:= dtpAkhir.Date-1;//strtodate(bulanini);
    dm1.QREpGajiOp.Parameters.ParamByName('nip').Value:=temp[1]+temp[2]+temp[3]+temp[4]+temp[5]+temp[6];
    dm1.QREpGajiOp.Parameters.ParamByName('awal2').Value:= dtpAwal.Date-1;
    dm1.QREpGajiOp.Parameters.ParamByName('akhir2').Value:= dtpAkhir.Date-1;
    dm1.QREpGajiOp.Parameters.ParamByName('nip2').Value:=temp[1]+temp[2]+temp[3]+temp[4]+temp[5]+temp[6];

    dm1.QREpGajiOp.Open;



    total:=0;
    label2.Caption:=format('%8.2m',[floattocurr(total)]);;
    if dm1.QREpGajiOp['fee'] = null then exit;

    dm1.QREpGajiOp.DisableControls;
    while Not (dm1.QREpGajiOp.Eof) do
    begin
        total:=total+dm1.QREpGajiOp['fee'];
        dm1.QREpGajiOp.Next;
    end;
    dm1.QREpGajiOp.First;
    dm1.QREpGajiOp.EnableControls;
    label2.Caption:=format('%8.2m',[floattocurr(total)]);;

btPrint.Enabled:=True;
end;

procedure TFgaji.BtPrintClick(Sender: TObject);
var
temp,nama,nip :string;
totaljam, totalmenit,i:integer;
FreportGaji:TFreportGaji;
begin
temp:=ComboBox1.Text;//[1]+ComboBox1.Text[2]+ComboBox1.Text[3]+ComboBox1.Text[4]+ComboBox1.Text[5]+ComboBox1.Text[6]
for i:=1 to length(temp) do
  begin
  if i<=6 then nip:=nip+temp[i]
  else if i>8 then nama:=nama+temp[i];
  end;


  FreportGaji:=tFreportGaji.Create(self);

 { FreportGaji.dm1.QREpGajiOp.Close;
  FreportGaji.dm1.QREpGajiOp.Parameters[0].Value:=dm1.QREpGajiOp.Parameters[1].Value;
  FreportGaji.dm1.QREpGajiOp.Parameters[1].Value:=dm1.QREpGajiOp.Parameters[2].Value;
  FreportGaji.dm1.QREpGajiOp.Parameters[2].Value:=dm1.QREpGajiOp.Parameters[3].Value;

  FreportGaji.dm1.QREpGajiOp.Open;
  }
  FreportGaji.QRLabel10.Caption:=label2.Caption;
  FreportGaji.QRLabel7.Caption:=nip;
  FreportGaji.QRLabel8.Caption:=nama;
  FreportGaji.QRLabel2.Caption:=fmain.nama_bulan[bulan];
  FreportGaji.QuickRep1.ReportTitle:='Slip Gaji '+Combobox1.Text;




totaljam:=0;totalmenit:=0;
while not (DM1.QREpGajiOp.Eof) do
  begin
      totaljam:=totaljam+dm1.QREpGajiOpJam.Value;
      totalmenit:=totalmenit+dm1.QREpGajiOpMenit.Value;
      dm1.QREpGajiOp.Next;
  end;
  totaljam:=totaljam+totalmenit div 60;
  totalmenit:=totalmenit mod 60;
  FreportGaji.qrjam.Caption:=inttostr(totaljam);
  FreportGaji.qrmenit.Caption:=inttostr(totalmenit);


  //FreportGaji.QuickRep1.print;


  FreportGaji.QuickRep1.Preview;
  FreportGaji.Free;
end;

procedure TFgaji.FormClose(Sender: TObject; var Action: TCloseAction);
begin
action:=caFree;
end;

procedure TFgaji.BitBtn1Click(Sender: TObject);
begin
  dm1.QGajiOP.Close;
  dm1.QGajiOP.Parameters.ParamByName('tanggal1').Value:=dtpAkhir.Date-1;
  dm1.QGajiOP.Parameters.ParamByName('tanggal2').Value:=dtpawal.Date-1;
  ShortDateFormat:='dd mmmm yyyy';
  QRRekap.Qrawal.Caption:=datetostr(dtpawal.Date);
  QRRekap.Qrakhir.Caption:=datetostr(dtpAkhir.Date);
  dm1.QGajiOP.Open;
  QRRekap.Preview;
end;

procedure TFgaji.DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin


if dm1.QREpGajiOp['JumlahTransaksi'] <10 then
         DBGrid1.Canvas.Brush.Color:=clSkyBlue
else
         DBGrid1.Canvas.Brush.Color:=clMedGray;

  DBGrid1.DefaultDrawColumnCell (Rect, DataCol, Column, State);
end;

end.

