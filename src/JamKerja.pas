unit JamKerja;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, StdCtrls, ExtCtrls, Grids, DBGrids;

type
  TFJamKerja = class(TForm)
    ComboBox1: TComboBox;
    Button1: TButton;
    Label2: TLabel;
    Label1: TLabel;
    DBGrid1: TDBGrid;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FJamKerja: TFJamKerja;

implementation

uses MainForm, DMserver;

{$R *.dfm}

procedure TFJamKerja.Button1Click(Sender: TObject);
var
NIP,temp,jam,menit,fee:string;
begin

if (ComboBox1.Enabled)and(ComboBox1.ItemIndex=-1) then exit;

 if button1.Caption='Start' then
  begin
    button1.Caption:='Stop';
    temp:=ComboBox1.Text;
    NIP:=temp[1]+temp[2]+temp[3]+temp[4]+temp[5]+temp[6];
    NIPoperator:=NIP;
    dm1.QStartJamKerja.Close;
    dm1.QStartJamKerja.Parameters[0].Value:=NIP;
    dm1.QStartJamKerja.Parameters[1].Value:=date;
    dm1.QStartJamKerja.Parameters[2].Value:=time;
    dm1.QStartJamKerja.ExecSQL;


    FormShow(self);
  end
 else
  begin
    button1.Caption:='Start';
    temp:=timetostr(time-strtotime(label9.Caption));
    if temp[2]=':' then temp:='0'+temp;
    jam   :=temp[1]+temp[2];
    menit :=temp[4]+temp[5];
    dm1.QUmum.Close;
    dm1.QUmum.SQL.Text:='select * from printing where id=3';
    dm1.QUmum.Open;
    fee:=floattostr(round((strtofloat(jam)+(strtofloat(menit)/60))*dm1.QUmum['harga']));
    dm1.QUmum.Close;
    NIPoperator:='001001';
    dm1.QStopJk.Close;
    dm1.QStopJk.Parameters[0].Value:=fee;
    dm1.QStopJk.Parameters[1].Value:=time;
    dm1.QStopJk.Parameters[2].Value:=strtoint(label7.Caption);
    dm1.QStopJk.Parameters[3].Value:=label4.Caption;
    dm1.QStopJk.ExecSQL;

    FormShow(self);
  end;


end;

procedure TFJamKerja.FormShow(Sender: TObject);
begin
dm1.QJamKerja.Close;
dm1.QJamKerja.Open;
ComboBox1.Enabled:=true;
 if not(dm1.QJamKerja['Mulai']=null) and (dm1.QJamKerja['Selesai']=null) then
 begin
  button1.caption:='Stop';
  ComboBox1.Enabled:=false;
  Label4.Caption:= dm1.QJamKerja['NIP'];
  Label5.Caption:= dm1.QJamKerja['Nama'];
  Label7.Caption:= dm1.QJamKerja['ID'];
  label9.Caption:= timetostr(dm1.QJamKerja['Mulai']);
  exit;
 end;
 dm1.QUmum.Close;
 dm1.QUmum.SQL.Text:='Select * from Operator where aktif='+quotedstr('aktif');
 dm1.QUmum.Open;
 ComboBox1.Items.Clear;
 ComboBox1.Text:='';
 while not (dm1.QUmum.Eof) do
  begin
    ComboBox1.Items.Add(dm1.QUmum['ID']+'. '+dm1.QUmum['Nama']);
    dm1.QUmum.Next;
  end;
  dm1.QUmum.Close;
end;

procedure TFJamKerja.ComboBox1Change(Sender: TObject);
var temp,nama:string;
i:integer;
begin

if (ComboBox1.ItemIndex=-1) or (ComboBox1.Text='') then exit;
temp:=ComboBox1.Text;
label4.Caption:=temp[1]+temp[2]+temp[3]+temp[4]+temp[5]+temp[6];

for i:=length(temp)downto 9 do
    nama:=nama+temp[length(temp)-i+9] ;
label5.Caption:=nama;
label7.Caption:='Menyusul';
end;

procedure TFJamKerja.FormClose(Sender: TObject; var Action: TCloseAction);
begin
action:=caFree;
end;

end.
