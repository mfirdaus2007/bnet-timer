unit editTarif;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, ExtCtrls, DBCtrls;

type
  TFEditTarif = class(TForm)
    DBNEdittarif: TDBNavigator;
    GEditTarif: TDBGrid;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FEditTarif: TFEditTarif;

implementation

uses DMserver, MainForm;

{$R *.dfm}

procedure TFEditTarif.FormCreate(Sender: TObject);
begin
dm1.QTarif.Close;
dm1.QTarif.Open;
end;

procedure TFEditTarif.FormClose(Sender: TObject; var Action: TCloseAction);
begin
fmain.loadTarif;
action:=cafree;
end;

end.
