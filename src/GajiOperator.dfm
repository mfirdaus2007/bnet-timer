object Fgaji: TFgaji
  Left = 609
  Top = 200
  BorderStyle = bsDialog
  Caption = 'Report Gaji Operator'
  ClientHeight = 414
  ClientWidth = 561
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 368
    Width = 24
    Height = 13
    Caption = 'Total'
  end
  object Label2: TLabel
    Left = 141
    Top = 368
    Width = 35
    Height = 13
    Alignment = taRightJustify
    Caption = 'Rp0,00'
  end
  object Label3: TLabel
    Left = 24
    Top = 54
    Width = 28
    Height = 13
    Caption = 'Nama'
  end
  object Label4: TLabel
    Left = 24
    Top = 83
    Width = 25
    Height = 13
    Caption = 'Mulai'
  end
  object Label5: TLabel
    Left = 24
    Top = 104
    Width = 35
    Height = 13
    Caption = 'Sampai'
  end
  object Label6: TLabel
    Left = 312
    Top = 83
    Width = 12
    Height = 13
    Caption = '>='
  end
  object Label7: TLabel
    Left = 312
    Top = 107
    Width = 6
    Height = 13
    Caption = '<'
  end
  object ComboBox1: TComboBox
    Left = 112
    Top = 52
    Width = 153
    Height = 21
    ItemHeight = 13
    TabOrder = 1
  end
  object DBGrid1: TDBGrid
    Left = 15
    Top = 176
    Width = 530
    Height = 177
    DataSource = DM1.DSRepGajiOp
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 7
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDrawColumnCell = DBGrid1DrawColumnCell
    Columns = <
      item
        Expanded = False
        FieldName = 'ID'
        Width = 58
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'tanggal'
        Width = 198
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Mulai'
        Width = 47
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Selesai'
        Width = 53
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Fee'
        Width = 60
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'JumlahTransaksi'
        Title.Caption = 'n Transaksi'
        Visible = True
      end>
  end
  object BtOK: TBitBtn
    Left = 112
    Top = 136
    Width = 65
    Height = 25
    Caption = 'OK'
    Default = True
    TabOrder = 4
    OnClick = BtOKClick
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333333333333333330000333333333333333333333333F33333333333
      00003333344333333333333333388F3333333333000033334224333333333333
      338338F3333333330000333422224333333333333833338F3333333300003342
      222224333333333383333338F3333333000034222A22224333333338F338F333
      8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
      33333338F83338F338F33333000033A33333A222433333338333338F338F3333
      0000333333333A222433333333333338F338F33300003333333333A222433333
      333333338F338F33000033333333333A222433333333333338F338F300003333
      33333333A222433333333333338F338F00003333333333333A22433333333333
      3338F38F000033333333333333A223333333333333338F830000333333333333
      333A333333333333333338330000333333333333333333333333333333333333
      0000}
    NumGlyphs = 2
  end
  object BtPrint: TBitBtn
    Left = 180
    Top = 136
    Width = 65
    Height = 25
    Caption = 'Print'
    Default = True
    Enabled = False
    TabOrder = 5
    OnClick = BtPrintClick
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000120B0000120B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00300000000000
      0003377777777777777308888888888888807F33333333333337088888888888
      88807FFFFFFFFFFFFFF7000000000000000077777777777777770F8F8F8F8F8F
      8F807F333333333333F708F8F8F8F8F8F9F07F333333333337370F8F8F8F8F8F
      8F807FFFFFFFFFFFFFF7000000000000000077777777777777773330FFFFFFFF
      03333337F3FFFF3F7F333330F0000F0F03333337F77773737F333330FFFFFFFF
      03333337F3FF3FFF7F333330F00F000003333337F773777773333330FFFF0FF0
      33333337F3F37F3733333330F08F0F0333333337F7337F7333333330FFFF0033
      33333337FFFF7733333333300000033333333337777773333333}
    NumGlyphs = 2
  end
  object BtClose: TBitBtn
    Left = 248
    Top = 136
    Width = 65
    Height = 25
    TabOrder = 6
    Kind = bkClose
  end
  object dtpAwal: TDateTimePicker
    Left = 112
    Top = 80
    Width = 186
    Height = 21
    Date = 40304.811557430550000000
    Time = 40304.811557430550000000
    DateFormat = dfLong
    TabOrder = 2
  end
  object dtpAkhir: TDateTimePicker
    Left = 112
    Top = 104
    Width = 186
    Height = 21
    Date = 40304.811557430550000000
    Time = 40304.811557430550000000
    DateFormat = dfLong
    TabOrder = 3
  end
  object BitBtn1: TBitBtn
    Left = 112
    Top = 16
    Width = 129
    Height = 25
    Caption = 'Print Rekapitulasi'
    Default = True
    TabOrder = 0
    OnClick = BitBtn1Click
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333333333333333330000333333333333333333333333F33333333333
      00003333344333333333333333388F3333333333000033334224333333333333
      338338F3333333330000333422224333333333333833338F3333333300003342
      222224333333333383333338F3333333000034222A22224333333338F338F333
      8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
      33333338F83338F338F33333000033A33333A222433333338333338F338F3333
      0000333333333A222433333333333338F338F33300003333333333A222433333
      333333338F338F33000033333333333A222433333333333338F338F300003333
      33333333A222433333333333338F338F00003333333333333A22433333333333
      3338F38F000033333333333333A223333333333333338F830000333333333333
      333A333333333333333338330000333333333333333333333333333333333333
      0000}
    NumGlyphs = 2
  end
end
