unit chatForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TFChat = class(TForm)
    Memo1: TMemo;
    Panel1: TPanel;
    Button1: TButton;
    Edit1: TEdit;
    procedure FormShow(Sender: TObject);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FChat: TFChat;
  Function  GetUserFromWindows: string;
implementation

uses mainform;

{$R *.dfm}

procedure TFChat.FormShow(Sender: TObject);
begin
SetWindowPos(Handle,HWND_TOPMOST,0, 0, 0, 0,SWP_NOMOVE or SWP_NOSIZE or SWP_SHOWWINDOW);
edit1.Text:='';
edit1.SetFocus;
end;

procedure TFChat.Edit1KeyPress(Sender: TObject; var Key: Char);
begin
if (key=#13) and not (TrimRight(edit1.Text)='') then button1Click(self);

end;



procedure TFChat.Button1Click(Sender: TObject);
var Chat:TRECChat;
begin

if Fmain.ClientsListBox.ItemIndex = -1 then
  begin
    ShowMessage('Pilih client dulu!');
    Exit;
  end;

if (TrimRight(edit1.Text)='') then exit;

chat.jam:=time;
chat.nama:=GetUserFromWindows;
chat.textChat:= TrimRight(edit1.Text);

  memo1.Lines.Add(timetostr(time)+' '+chat.nama+' to '+ chatwith+' -> '+TrimRight(edit1.Text));
  edit1.Clear;


  fmain.SendChat(chat);




end;

function GetUserFromWindows: string;
Var
   UserName : string;
   UserNameLen : Dword;
Begin
   UserNameLen := 255;
   SetLength(userName, UserNameLen) ;
   If GetUserName(PChar(UserName), UserNameLen) Then
     Result := Copy(UserName,1,UserNameLen - 1)
   Else
     Result := 'Unknown';
End;

end.
