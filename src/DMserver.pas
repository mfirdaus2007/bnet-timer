unit DMserver;

interface

uses
  SysUtils, Classes, DB, ADODB;

type
  TDM1 = class(TDataModule)
    ADOConnection1: TADOConnection;
    TJenis: TADOTable;
    QUmum: TADOQuery;
    DSFrontTable: TDataSource;
    DSHarianKeluar: TDataSource;
    DSDetailKeluar: TDataSource;
    DSGajiOP: TDataSource;
    QTarif: TADOQuery;
    QFront: TADOQuery;
    QFrontNo: TAutoIncField;
    QFrontStation: TStringField;
    QFrontID: TSmallintField;
    QFrontmulai: TDateTimeField;
    QFrontselesai: TDateTimeField;
    QFrontNilai: TBCDField;
    QFronttipe: TStringField;
    QHarianKeluar: TADOQuery;
    QHarianKeluarTanggal: TDateTimeField;
    QHarianKeluarOutcome: TBCDField;
    QDetailKeluar: TADOQuery;
    QDetailKeluarID: TAutoIncField;
    QDetailKeluarJam: TDateTimeField;
    QDetailKeluarketerangan: TWideStringField;
    QDetailKeluarjumlah: TBCDField;
    QGajiOP: TADOQuery;
    QGajiOPnip: TWideStringField;
    QGajiOPgaji: TBCDField;
    QGajiOPNama: TStringField;
    QHarianMasuk: TADOQuery;
    QHarianMasukTanggal: TDateTimeField;
    QHarianMasukIncome: TBCDField;
    QUpdateInc: TADOQuery;
    QSimpanSewa: TADOQuery;
    QSimpanKeluar: TADOQuery;
    DsPemasukan: TDataSource;
    QPemasukan: TADOQuery;
    QPemasukanNo: TAutoIncField;
    QPemasukanTanggal: TDateTimeField;
    QPemasukanStation: TStringField;
    QPemasukanID: TSmallintField;
    QPemasukanmulai: TDateTimeField;
    QPemasukanselesai: TDateTimeField;
    QPemasukanNilai: TBCDField;
    DsPengeluaran: TDataSource;
    QPengeluaran: TADOQuery;
    QPengeluaranID: TAutoIncField;
    QPengeluarantanggal: TDateTimeField;
    QPengeluaranJam: TDateTimeField;
    QPengeluaranKeterangan: TWideStringField;
    QPengeluaranJumlah: TBCDField;
    DsJamKerja: TDataSource;
    QJamKerja: TADOQuery;
    QJamKerjaID: TAutoIncField;
    QJamKerjaNIP: TWideStringField;
    QJamKerjaTanggal: TDateTimeField;
    QJamKerjaMulai: TDateTimeField;
    QJamKerjaSelesai: TDateTimeField;
    QJamKerjaFee: TBCDField;
    QJamKerjaNama: TStringField;
    QPemasukanNIP: TStringField;
    TOperator: TADOTable;
    QPemasukanPetugas: TStringField;
    QPemasukanJenis: TStringField;
    DSOperator: TDataSource;
    QStartJamKerja: TADOQuery;
    QStopJK: TADOQuery;
    DSRepGajiOp: TDataSource;
    QREpGajiOp: TADOQuery;
    DSHARIANmasuk: TDataSource;
    DSEditTarif: TDataSource;
    DSPrinting: TDataSource;
    QPrinting: TADOQuery;
    DSMember: TDataSource;
    DSTarifWifi: TDataSource;
    TTarifWifi: TADOTable;
    TTarifWifijam: TIntegerField;
    TTarifWifireguler: TFloatField;
    TTarifWifianggota: TFloatField;
    DsWTimer: TDataSource;
    TWifiTimer: TADOTable;
    TWifiTimerid: TLargeintField;
    TWifiTimermulai: TDateTimeField;
    TWifiTimertipe: TSmallintField;
    TWifiTimerJenis: TStringField;
    QMember: TADOQuery;
    QMemberID: TLargeintField;
    QMemberNama: TStringField;
    QMemberMAC: TStringField;
    QMemberMember: TStringField;
    QMemberIDAnggota: TStringField;
    TWifiTimeridAnggota: TStringField;
    TWifiTimerNama: TStringField;
    TWifiTimerlama: TIntegerField;
    tmember: TADOTable;
    QMemberstamp: TDateTimeField;
    TTarifWifiKeterangan: TStringField;
    TWifiTimerJam: TStringField;
    QREpGajiOptanggal: TDateTimeField;
    QREpGajiOpJumlahTransaksi: TIntegerField;
    QREpGajiOpID: TAutoIncField;
    QREpGajiOpNIP: TWideStringField;
    QREpGajiOpMulai: TDateTimeField;
    QREpGajiOpSelesai: TDateTimeField;
    QREpGajiOpFee: TBCDField;
    QREpGajiOpJam: TIntegerField;
    QREpGajiOpMenit: TIntegerField;
    procedure DataModuleCreate(Sender: TObject);
    procedure TMemberCalcFields(DataSet: TDataSet);
    procedure TWifiTimerCalcFields(DataSet: TDataSet);
    procedure QREpGajiOpCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM1: TDM1;

implementation

uses MainForm;

{$R *.dfm}

procedure TDM1.DataModuleCreate(Sender: TObject);
begin
FMAIN.loadTarif;
fmain.LoadOperator;
end;

procedure TDM1.TMemberCalcFields(DataSet: TDataSet);

var i:integer;
temp:string;
begin
  if qmember.State in [dsedit,dsinsert] then exit;

    temp:='';
    for  i:=1 to 6-length(inttostr(qmember['id'])) do
      temp:=temp+'0';
    qmember['idanggota']:=temp+inttostr(qmember['id']);

end;


procedure TDM1.TWifiTimerCalcFields(DataSet: TDataSet);
var jam, menit, detik, mdetik, i:word;
temp:string;
lamasewa:TDateTime;

begin
  if TWifiTImer.State in [dsedit,dsinsert] then exit;

    temp:='';
    for  i:=1 to 6-length(inttostr(TWifiTImer['id'])) do
      temp:=temp+'0';
    TWifiTImer['idanggota']:=temp+inttostr(TWifiTImer['id']);

    lamasewa:=now-dm1.TWifiTimer['mulai'];
    Decodetime(lamasewa,jam, menit, detik, mdetik);
    //menit:=menit+1;
    TWifiTimer['lama']:=jam*60+menit;
    if menit < 30 then menit := 30
      else if menit < 60 then
        menit := 60;
    TWifiTimer['jam']:=format('%8.2f',[jam+ menit/60]);
end;

procedure TDM1.QREpGajiOpCalcFields(DataSet: TDataSet);
var jam, menit, detik, mili:word;
begin
DecodeTime((QREpGajiOpselesai.Value-QREpGajiOpmUlai.Value),jam,menit,detik,mili);
 QREpGajiOpJam.Value:=jam;
QREpGajiOpMenit.Value:=menit;
end;

end.
