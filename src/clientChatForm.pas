unit clientChatForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TFChat = class(TForm)
    Panel1: TPanel;
    Memo1: TMemo;
    Edit1: TEdit;
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FChat: TFChat;

implementation

uses Unit2, Unit1;

{$R *.dfm}

procedure TFChat.Button1Click(Sender: TObject);
var Chat:TRECChat;
begin


if (TrimRight(edit1.Text)='') then exit;

chat.jam:=time;
chat.nama:=namaUser;
chat.textChat:= TrimRight(edit1.Text);

  memo1.Lines.Add(timetostr(time)+' '+chat.nama+' -> '+TrimRight(edit1.Text));
  edit1.Clear;
  form2.SendChat(chat);

end;

procedure TFChat.FormShow(Sender: TObject);
begin
edit1.Text:='';
edit1.SetFocus;
end;

procedure TFChat.Edit1KeyPress(Sender: TObject; var Key: Char);
begin
if (key=#13) and not (TrimRight(edit1.Text)='') then button1Click(self);
end;

end.
