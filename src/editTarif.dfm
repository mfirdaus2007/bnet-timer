object FEditTarif: TFEditTarif
  Left = 402
  Top = 262
  Width = 462
  Height = 329
  Caption = 'FEditTarif'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object DBNEdittarif: TDBNavigator
    Left = 0
    Top = 0
    Width = 454
    Height = 41
    DataSource = DM1.DSEditTarif
    Align = alTop
    TabOrder = 0
  end
  object GEditTarif: TDBGrid
    Left = 0
    Top = 41
    Width = 454
    Height = 254
    Align = alClient
    DataSource = DM1.DSEditTarif
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
end
