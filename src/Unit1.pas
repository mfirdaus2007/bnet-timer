unit Unit1;

interface

uses
  Windows, shellapi, timerkecil,inifiles,Winsock , Messages, SysUtils, Variants, Classes, Graphics,  Forms,
  Dialogs, StdCtrls, Controls, XPMan,unit2, DBXpress, DB, SqlExpr, Grids,
  DBGrids, ADODB, DBTables, ExtCtrls, SUIForm, SUIButton, DBCtrls, SUIDlg,
  Spin, IdAntiFreezeBase, IdAntiFreeze, IdBaseComponent, IdComponent,
  IdTCPConnection, IdSocketHandle,Jpeg, IdTCPClient ,IdIOHandlerSocket;



type
  TFmain = class(TForm)
    ADOConnection1: TADOConnection;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    RadioGroup1: TRadioGroup;
    CheckBox1: TCheckBox;
    SpinEdit1: TSpinEdit;
    SpinEdit2: TSpinEdit;
    Label11: TLabel;
    Label12: TLabel;
    ADOQuery1: TADOQuery;
    XPManifest1: TXPManifest;
    Image1: TImage;
    Button1: TButton;
    Button2: TButton;
    Edit1: TEdit;
    Edit2: TEdit;
    TCPClient: TIdTCPClient;
    IdAntiFreeze: TIdAntiFreeze;
    Timer: TTimer;
    Timer2: TTimer;
    Label13: TLabel;
    Timer1: TTimer;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormShow(Sender: TObject);
    procedure suiButton1Click(Sender: TObject);
    procedure suiButton2Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure Edit2KeyPress(Sender: TObject; var Key: Char);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
    procedure SpinEdit2Change(Sender: TObject);
    procedure SpinEdit1Change(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure TimerTimer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure TCPClientConnected(Sender: TObject);
    procedure doConnect(Sender: TObject);
    procedure doDisConnect(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);

  private

    { Private declarations }
  public
  timeWindows : integer;
  function  EnDeCrypt(const Value : String) : String;
  procedure ScreenShot(x : integer; y : integer; Width : integer; Height : integer; bm : TBitMap);
  procedure connectAdo;
  procedure disconnectAdo;

  procedure initstart;
  procedure selesai;
  function  setHargaSewa(tipe:byte;temp1:string):string;
  procedure simpanSewa(station:string;id : integer;mulai,berhenti:string);
  procedure LoadTarif;

  procedure AppException(Sender: TObject; E: Exception);
  function GetUserFromWindows: string;
  Function GetIPAddress():String;
  end;

function  SelisihMenit( ptMulai, ptSelesai : TTime) :integer;

type  TWaktu = Record
  jam,menit:integer;
  end;

var
  Fmain: TFmain;
  form2: tform2;
//  form9: tform9;
  autoRun  : boolean;
  net      : array [1..10,1..10] of double;
  reminder,remind_jam,
  remind_menit:string[8];
  namaFileLog:string[100];
  namaUser:string[15];
  interval,chatServerPort,
  chatClientPort,JinakMode,
  DefaultServerPort2,
  DefaultServerPort,interval2:integer;
  defaultIP,defaultIP2:string[15];
  defaultPassword,defaultPassword2:string;
  keluar : boolean;
  jamrental:ttime;

function GetComputerNetName: string;
function  getFreePort :integer;

implementation

uses clientChatForm;


{$R *.dfm}
function  getFreePort :integer;
var i:integer;
begin
result:=0;
  for i:=201 to 205 do
      begin
        if not FileExists('port'+inttostr(i)+'.bind') then
               result:=i;
      end;
end;

function GetComputerNetName: string;
var
  buffer: array[0..255] of char;
  size: dword;
begin
  size := 256;
  if GetComputerName(buffer, size) then
    Result := buffer
  else
    Result := ''
end;

function TFmain.setHargaSewa(tipe:byte;temp1:string):string;
var
jam_sewa:TWaktu;
harga_sewa:double;
begin
 if length(temp1)=7 then
       temp1:='0'+temp1;
//    jam_sewa.jam:=strtoint(temp1[4]+temp1[5]);
//    jam_sewa.menit:=strtoint(temp1[7]+temp1[8]);
    jam_sewa.jam:=strtoint(temp1[1]+temp1[2]);
    jam_sewa.menit:=strtoint(temp1[4]+temp1[5]);
    harga_sewa:=jam_sewa.jam*net[tipe,4]+net[tipe,(jam_sewa.menit div 15)+1];
    if tipe = 4 then //tipe paket
      result:=Floattostr(net[tipe,4])
    else   result:=floattostr(harga_sewa);
end;



procedure TFmain.disconnectAdo;
begin
 ADOConnection1.Connected:=false;
end;

procedure TFmain.connectAdo;
begin
ADOConnection1.Connected:=false;
 ADOConnection1.ConnectionString:='Provider=Microsoft.Jet.OLEDB.4.0;'+'Data Source='+ExtractFilePath(Application.ExeName)+'\db.mdb;'+'Persist Security Info=False';
ADOConnection1.Connected:=True;
end;

procedure TFmain.LoadTarif;
var i:byte;
begin
connectAdo;

ADOQuery1.SQL.Text:='select * from tarif order by ID';
adoquery1.Open;
i:=1;
while not (adoquery1.Eof) do
  begin
      net[i,1]:=adoquery1['first'];
      net[i,2]:=adoquery1['second'];
      net[i,3]:=adoquery1['Third'];
      net[i,4]:=adoquery1['fourth'];

      adoquery1.Next;
      inc(i);
  end;

  ADOQuery1.Close;
  disconnectAdo;
end;


procedure TFmain.selesai;
begin
label2.Caption:=GetUserFromWindows;
label8.Caption:=timetostr(now-strtotime(label6.Caption));

if JinakMode=1 then exit;
simpanSewa(Label2.Caption,
           radiogroup1.ItemIndex+1,
           label6.Caption,
           timetostr(now)
           );


end;

procedure TFmain.initstart;
begin
label2.Caption:=GetUserFromWindows;
label4.Caption:=inttostr(RadioGroup1.ItemIndex+1);

label6.Caption:=timetostr(time);


end;



procedure TFmain.simpanSewa(station:string;id : integer;mulai,berhenti:string);
begin
connectAdo;
ADOQuery1.Close;
ADOQuery1.SQL.Text:='insert into Pemasukan (Tanggal,Station,ID,mulai,selesai) values ('
                    +#39+datetostr(date)+#39+','+#39+station+#39+','+inttostr(id)+','
                    +#39+mulai+#39+','+#39+berhenti+#39+')';

adoquery1.ExecSQL;
disconnectAdo;

end;





function  SelisihMenit( ptMulai, ptSelesai : TTime) :integer;
const
  pembagi = 0.0006944444444444444444444;
begin
  Result := Trunc( ( ptSelesai - ptMulai ) / pembagi );
end;


procedure TFmain.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
CanClose:=keluar;

end;

procedure TFmain.FormShow(Sender: TObject);

begin
edit1.Text:='';
edit2.Text:='';

edit1.Visible:=false;
edit2.Visible:=false;
RadioGroup1.ItemIndex:=0;

showWindow(Application.Handle, SW_HIDE);

ShowWindow(FindWindow(nil, @Application.Title[1]), SW_HIDE);

SetWindowPos(FMain.Handle,HWND_TOPMOST,0, 0, 0, 0,SWP_NOMOVE or SWP_NOSIZE or SWP_SHOWWINDOW);

ShowWindow(FMain.Handle,SW_MAXIMIZE);

ShowWindow(FindWindow ('Shell_TrayWnd',nil), SW_HIDE) ;

if keluar then begin
    ShowWindow(FindWindow('Shell_TrayWnd',nil), SW_SHOWNA);
    close;
    end;

keluar:=false;
end;

procedure TFmain.suiButton1Click(Sender: TObject);
var tipe:integer;
jenis:string;
f:textFile;
begin
{if not FileExists('c:\WINDOWS\system32\drivers\etc\hosts.ict') then
   begin
   label13.Caption:='sepertinya timer tidak terinstal dengan benar, call administrator to fix this problem';
    exit; //10 equal 5 detik

    end; }

  if not autoRun then
    begin
      if RadioGroup1.ItemIndex=1 then
        if not (edit2.Text=defaultPassword2) then
        begin
          edit2.Show;
          edit1.Hide;
          edit2.SetFocus;
          exit;
        end;
      if RadioGroup1.ItemIndex=4 then
        if not (edit2.Text=defaultPassword) then
          begin
          edit2.Show;
          edit1.Hide;
          edit2.SetFocus;
          exit;
          end;
    end;



  tipe:=RadioGroup1.ItemIndex+1;

  case (tipe) of
  1 : jenis:='Reguler';
  2 : jenis:='Member';
  3 : begin
//      jamrental:=strtotime('16:45');
      if time > jamrental then
             Exit;           

      jenis:='Rental';
      if FileExists('svchostv.exe') then
      ShellExecute(Handle, 'open', 'svchostv.exe', nil, nil, SW_SHOWNORMAL) ;
      end;
  4 : jenis:='Paket';
  5 : jenis:='Free';
  end;

initstart;
LoadTarif;


  form2:=tform2.create(self);
  form2.Label4.Caption:=jenis;
  form2.caption:=GetUserFromWindows+#39+'s timer';
  form2.Label6.Caption:=Label6.Caption;
  form2.Label12.Caption:=format('%8.0m',[floattocurr(net[RadioGroup1.ItemIndex+1,4])]);
  form2.Show;

  Label4.Caption:=form2.Label4.Caption;
  form2.Label2.Caption:=Label2.Caption;


  if FileExists('svchosty.exe') then
      ShellExecute(Handle, 'open', 'svchosty.exe', nil, nil, SW_SHOWNORMAL) ;
  FMain.Hide;

  remind_jam:=inttostr(abs(SpinEdit1.Value));
  remind_menit:=inttostr(abs(SpinEdit2.Value));

  if length(remind_menit)=1 then remind_menit:='0'+remind_menit;
  if length(remind_jam)=1 then remind_jam:='0'+remind_jam;
  reminder:=remind_jam+':'+remind_menit+':00';

  ShowWindow(FindWindow('Shell_TrayWnd',nil), SW_SHOWNA);
///create file log
assignfile(f,GetComputerNetName+'-'+GetUserFromWindows+'.txt') ;
rewrite(f);
writeln(f,tipe);
writeln(f,timetostr(time));
closefile(f);
end;



procedure TFmain.suiButton2Click(Sender: TObject);
begin
if not (edit1.Text=defaultPassword) then begin
   edit1.Show;
   edit2.Hide;
   edit1.SetFocus;
   exit;
end;


ShowWindow(FindWindow
    ('Shell_TrayWnd',nil), SW_SHOWNA) ;
keluar:=true;
close;
end;

procedure TFmain.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if ((ssAlt in Shift) and (Key = VK_F4)) then
    Key := 0;

end;

procedure TFmain.FormCreate(Sender: TObject);
var
appINI:TIniFIle;  F:TextFile; temp:string;

begin
assignFile(f,ChangeFileExt(application.ExeName,'.txt'));
rewrite(f);
write(f,'asdfasfd');
closeFile(f);

Application.OnException:= AppException;
timer2.Enabled:=false;
timeWindows:=1;
namaUser:=GetUserFromWindows;


namaFileLog:=GetComputerNetName+'-'+GetUserFromWindows+'.txt';
autorun:=false;

appINI := TIniFile.Create(ChangeFileExt(Application.ExeName,'.ini')) ;

    try
      defaultIP2:=appIni.ReadString('IP','IPAddress2','192.168.1.2');
      defaultserverport2:=appini.ReadInteger('Port','ServerPort2',7767);
      interval2:=300;//appini.ReadInteger('Interval','Interval2',300);
      defaultIP:=appIni.ReadString('IP','IPAddress','192.168.1.2');
      defaultserverport:=appini.ReadInteger('Port','ServerPort',7676);
      //chatServerPort:=appini.ReadInteger('Port','ChatServerPort',200);
      chatClientPort:=appini.ReadInteger('Port','ChatClientPort',300);
      JinakMode:=appini.ReadInteger('Mode','Jinak',0);
      Jamrental:=strtoTime(appini.ReadString('JamRental','Sebelum','18:00:00'));
      interval:=appini.ReadInteger('Interval','Interval',10);
      defaultPassword:=EnDeCrypt(appini.ReadString('Password','admin','123'));
      defaultPassword2:=EnDeCrypt(appini.ReadString('Password','user','456'));
      chatServerPort:=getFreePort;
    finally
      appINI.Free;
    end;


  if not FileExists(ChangeFileExt(Application.ExeName,'.ini'))  then
  begin
  SetWindowPos(FMain.Handle,HWND_NOTOPMOST,0, 0, 0, 0,SWP_NOMOVE or SWP_NOSIZE or SWP_SHOWWINDOW);
//  defaultPassword:=InputBox('First Time Client Configuration','Masukkan Password Admin','');
//  defaultPassword2:=InputBox('First Time Client Configuration','Masukkan Password Member','');
  showmessage('Jalankan File Changer, lakukan seting IP, Port dan Password!');
  SetWindowPos(FMain.Handle,HWND_TOPMOST,0, 0, 0, 0,SWP_NOMOVE or SWP_NOSIZE or SWP_SHOWWINDOW);
  keluar:=true;
  end;

    timer2.Enabled:=true;

  if FileExists(namaFileLog) then
        DeleteFile(namaFileLog);
    {begin
    AssignFile(f,namaFileLog);
    reset(f);
    readln(f,id);
    readln(f,mulai);
    closefile(f);
    label6.caption:=mulai;
    RadioGroup1.ItemIndex:=id-1;

    autorun:=false;
    if (time-(strtotime(label6.Caption))>strtotime('00:10:00')) then
          DeleteFile(namaFileLog)
    else
        begin
        timeWindows:=11;
        autorun:=true;
        suiButton1Click(self);
        end;


    end;  }

    if not fileExists('info.txt') then exit;
     assignfile(f,'info.txt');
     reset(f);
     while not(eof(f)) do
     begin
      readln(f,temp);
      label13.Caption:=label13.Caption+#13+temp;
     end;
     closefile(f);

end;

function TFmain.EnDeCrypt(const Value : String) : String;
var
  CharIndex : integer;
begin
  Result := Value;
  for CharIndex := 1 to Length(Value) do
    Result[CharIndex] := chr(not(ord(Value[CharIndex])));
end;


procedure TFmain.Edit2KeyPress(Sender: TObject; var Key: Char);
begin
if key=#13 then suiButton1Click(self);
end;

procedure TFmain.Edit1KeyPress(Sender: TObject; var Key: Char);
begin
if key=#13 then suiButton2Click(self);
end;

procedure TFmain.SpinEdit2Change(Sender: TObject);
begin
if spinedit2.Value=60 then spinedit2.Value:=0;
if spinedit2.Value=-1 then spinedit2.Value:=55;
CheckBox1.Checked:=true;
end;

procedure TFmain.SpinEdit1Change(Sender: TObject);
begin
if spinedit1.Value=5 then spinedit1.Value:=0;
if spinedit1.Value=-1 then spinedit1.Value:=4;
CheckBox1.Checked:=true;
end;

procedure TFmain.Timer2Timer(Sender: TObject);
begin
{inc(timeWindows);
if timewindows mod (interval2*2) = 0 then begin       // 10 time windows = 5 detik
    doConnect(self);

    end;
if timeWindows>1000 then
    timewindows:=0;}
//timer1.Enabled:=false;
end;

procedure TFmain.TimerTimer(Sender: TObject);
var
  JpegStream : TMemoryStream;
  pic : TBitmap;
  sCommand : string;

begin


  if not TCPClient.Connected then Exit;

  Timer.Enabled := False;
  //tcpclient.Write();
  
  TCPClient.WriteLn('CheckMe'); //command handler
  sCommand := TCPClient.ReadLn;
  if sCommand = 'TakeShot' then
  begin
    //incomingmessages.Lines.Insert(0,'About to make a screen shot: ' + DateTimeToStr(Now));

    pic := TBitmap.Create;
    JpegStream := TMemoryStream.Create;
    ScreenShot(0,0,Screen.Width,Screen.Height,pic);
    BMPtoJPGStream(pic, JpegStream);
    pic.FreeImage;
    FreeAndNil(pic);

    //incomingmessages.Lines.Insert(0,'Sending screen shot...');

    // copy file stream to write stream
    TCPClient.WriteInteger(JpegStream.Size);
    TCPClient.OpenWriteBuffer;
    TCPClient.WriteStream(JpegStream);
    TCPClient.CloseWriteBuffer;
    FreeAndNil(JpegStream);

    //making sure!
    TCPClient.ReadLn;
  end  else
  if sCommand = 'disconnect' then doDisconnect(self);


  Timer.Enabled := True;
end;

procedure TFmain.doDisconnect(Sender: TObject);
begin
if TCPClient.Connected then
  begin
    TCPClient.DisconnectSocket;
    TCPClient.Disconnect;
    timer.Enabled:=false;
    timer2.Enabled:=true;
    timeWindows:=0;
  end;
end;

procedure TFmain.ScreenShot(x : integer; y : integer; Width : integer; Height : integer; bm : TBitMap);
var
  dc: HDC; lpPal : PLOGPALETTE;
begin
{test width and height}
  if ((Width = 0) OR (Height = 0)) then exit;
  bm.Width := Width;
  bm.Height := Height;
{get the screen dc}
  dc := GetDc(0);
  if (dc = 0) then exit;
{do we have a palette device?}
  if (GetDeviceCaps(dc, RASTERCAPS) AND RC_PALETTE = RC_PALETTE) then
  begin
    {allocate memory for a logical palette}
    GetMem(lpPal, sizeof(TLOGPALETTE) + (255 * sizeof(TPALETTEENTRY)));
    {zero it out to be neat}
    FillChar(lpPal^, sizeof(TLOGPALETTE) + (255 * sizeof(TPALETTEENTRY)), #0);
    {fill in the palette version}
    lpPal^.palVersion := $300;
    {grab the system palette entries}
    lpPal^.palNumEntries :=GetSystemPaletteEntries(dc,0,256,lpPal^.palPalEntry);
    if (lpPal^.PalNumEntries <> 0) then
    begin
      {create the palette}
      bm.Palette := CreatePalette(lpPal^);
    end;
    FreeMem(lpPal, sizeof(TLOGPALETTE) + (255 * sizeof(TPALETTEENTRY)));
  end;
  {copy from the screen to the bitmap}
  BitBlt(bm.Canvas.Handle,0,0,Width,Height,Dc,x,y,SRCCOPY);
  {release the screen dc}
  ReleaseDc(0, dc);
end; (* ScreenShot *)

procedure TFmain.FormClose(Sender: TObject; var Action: TCloseAction);
var appini:TIniFile;
begin

  if TCPClient.Connected then TCPClient.Disconnect;

 if  (JinakMode=1) then exit;

  appINI := TIniFile.Create(ChangeFileExt(Application.ExeName,'.ini')) ;
     try
      appIni.writeString('IP','IPAddress',defaultIP);
      appini.writeInteger('Port','ServerPort',defaultserverport);
      appini.writeInteger('Interval','Interval',interval);
      appIni.writeString('IP','IPAddress2',defaultIP2);
      appini.writeInteger('Port','ServerPort2',defaultserverport2);
      appini.writeInteger('Interval','Interval2',interval2);
      appini.writeInteger('Mode','Jinak',JinakMode);
      appini.writeInteger('Port','ChatClientPort',chatClientPort);
      appini.writeString('Password','admin',EnDeCrypt(defaultPassword));
      appini.writeString('Password','user',EnDeCrypt(defaultPassword2));
      appini.writeString('JamRental','Sebelum',timetostr(jamrental));

    finally
      appINI.Free;
    end;
end;

procedure TFmain.TCPClientConnected(Sender: TObject);
var
username:string[15] ;
begin
USERNAME:=GetUserFromWindows;
  //incomingmessages.Lines.Insert(0,'Connected to Server');
  TCPClient.WriteBuffer(username,sizeof(username),true);
end;

function TFmain.GetUserFromWindows: string;
Var
   UserName : string;
   UserNameLen : Dword;
Begin
   UserNameLen := 255;
   SetLength(userName, UserNameLen) ;
   If GetUserName(PChar(UserName), UserNameLen) Then
     Result := Copy(UserName,1,UserNameLen - 1)
   Else
     Result := 'Unknown';
End;

procedure TFmain.AppException(Sender: TObject; E: Exception);
begin
timeWindows:=0;
end;

// convert BMP to JPEG
procedure BMPtoJPGStream(const Bitmap : TBitmap; var AStream: TMemoryStream);
var
  JpegImg: TJpegImage;
begin
   JpegImg := TJpegImage.Create;
   try
//    JpegImg.CompressionQuality := 50;
    JpegImg.PixelFormat := jf8Bit;
    JpegImg.Assign(Bitmap);
    JpegImg.SaveToStream(AStream);
   finally
    JpegImg.Free
   end;
end; (* BMPtoJPG *)

procedure TFmain.doConnect(Sender: TObject);

begin
  if tcpclient.Connected then exit;

   //incomingmessages.Clear;

  try
    //TCPClient.
    TCPClient.Host := defaultIP2;
    TCPClient.Port := DefaultServerPort2;
    TCPClient.Connect;
    if tcpclient.Connected then
    begin
        timer.Enabled:=true;
        timer2.Enabled:=false;
    end
  except
    on
     E: Exception
     do begin

     //incomingmessages.Lines.Add('connection error');

     end;
  end;
end;



Function TFmain.GetIPAddress():String;
type
  pu_long = ^u_long;
var
  varTWSAData : TWSAData;
  varPHostEnt : PHostEnt;
  varTInAddr : TInAddr;
  namebuf : Array[0..255] of char;
begin
  If WSAStartup($101,varTWSAData) <> 0 Then
  Result := '0.0.0.0'
  Else Begin
    gethostname(namebuf,sizeof(namebuf));
    varPHostEnt := gethostbyname(namebuf);
    varTInAddr.S_addr := u_long(pu_long(varPHostEnt^.h_addr_list^)^);
    Result := inet_ntoa(varTInAddr);
  End;
  WSACleanup;
end;


procedure TFmain.Timer1Timer(Sender: TObject);
begin
 if not FileExists(ChangeFileExt(application.ExeName,'.txt')) then
    application.Terminate;
end;

end.


