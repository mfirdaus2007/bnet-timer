unit log;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, StdCtrls, ExtCtrls;

type
  TFlog = class(TForm)
    StringGrid1: TStringGrid;
    Panel1: TPanel;
    Button1: TButton;
    Memo1: TMemo;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Flog: TFlog;

implementation

uses mainform;

{$R *.dfm}

procedure TFlog.FormClose(Sender: TObject; var Action: TCloseAction);
begin
action:=cafree;
end;

procedure TFlog.FormCreate(Sender: TObject);
begin
  SetWindowPos(Handle, // handle to window
               HWND_TOPMOST, // placement-order handle {*}
               Left,  // horizontal position
               Top,   // vertical position
               Width,
               Height,
               // window-positioning options
               SWP_NOACTIVATE or SWP_NOMOVE or SWP_NOSIZE);

               StringGrid1.Cells[0,0]:='No';
               StringGrid1.Cells[1,0]:='Pengguna';
               StringGrid1.Cells[2,0]:='Tipe';
               StringGrid1.Cells[3,0]:='Mulai';
               StringGrid1.Cells[4,0]:='Jam Log';
               StringGrid1.Cells[5,0]:='Total Sewa';
end;

procedure TFlog.Button1Click(Sender: TObject);
var id,i:integer;

begin
if MessageDlg('Peringatan, sudahkah anda membaca message di bawah tombol simpan?',mtWarning,mbOKCancel,0)=idno then exit;

id:=0;
for i:=1 to StringGrid1.RowCount-1 do
  begin
    if stringGrid1.cells[2,i]='Reguler' then id:=1
    else
    if stringGrid1.cells[2,i]='Member' then id:=2
    else
    if stringGrid1.cells[2,i]='Rental' then id:=3
    else
    if stringGrid1.cells[2,i]='Paket' then id:=4
    else
    if stringGrid1.cells[2,i]='Free' then id:=5;

    fmain.simpanSewarecord(date,
                      stringGrid1.cells[1,i],NIPOperator,
                      id,
                      strtotime(stringGrid1.cells[3,i]),
                      strtotime(stringGrid1.cells[4,i])
                      );
  end;
  showmessage('Semua record telah tersimpan');
  fmain.updateIncome;
  close;
end;

end.
