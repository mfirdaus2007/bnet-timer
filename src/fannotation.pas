unit fannotation;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TForm8 = class(TForm)
    Button1: TButton;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public

    { Public declarations }
  end;

var
  Form8: TForm8;

implementation

{$R *.dfm}


procedure TForm8.FormCreate(Sender: TObject);
begin
  SetWindowPos(Handle, // handle to window
               HWND_TOPMOST, // placement-order handle {*}
               Left,  // horizontal position
               Top,   // vertical position
               Width,
               Height,
               // window-positioning options
               SWP_NOACTIVATE or SWP_NOMOVE or SWP_NOSIZE);
end;

procedure TForm8.Button1Click(Sender: TObject);
begin
close;
end;

procedure TForm8.FormClose(Sender: TObject; var Action: TCloseAction);
begin
Action:=cafree;
end;

end.
