object Fmain: TFmain
  Left = 346
  Top = 49
  Width = 798
  Height = 634
  Caption = 'B-NET TIMER ::....'
  Color = cl3DLight
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 790
    Height = 580
    ActivePage = TabSheet1
    Align = alClient
    MultiLine = True
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'REGULER'
      OnShow = TabSheet1Show
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 782
        Height = 41
        Align = alTop
        TabOrder = 0
        DesignSize = (
          782
          41)
        object Label3: TLabel
          Left = 8
          Top = 0
          Width = 289
          Height = 39
          AutoSize = False
          Caption = 'Bismillah NET Server'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -27
          Font.Name = 'Monotype Corsiva'
          Font.Style = []
          ParentFont = False
        end
        object Label7: TLabel
          Left = 548
          Top = 5
          Width = 99
          Height = 13
          Alignment = taRightJustify
          Anchors = [akTop, akRight]
          Caption = 'Pendapatan Hari ini :'
        end
        object Label1: TLabel
          Left = 632
          Top = 20
          Width = 59
          Height = 16
          Alignment = taRightJustify
          Anchors = [akTop, akRight]
          Caption = 'Sekarang'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Microsoft Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label2: TLabel
          Left = 712
          Top = 22
          Width = 46
          Height = 14
          Alignment = taRightJustify
          Anchors = [akTop, akRight]
          Caption = 'Sekarang'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object Label8: TLabel
          Left = 699
          Top = 5
          Width = 58
          Height = 13
          Alignment = taRightJustify
          Anchors = [akTop, akRight]
          Caption = 'Pendapatan'
        end
      end
      object pnlLeft: TPanel
        Left = 0
        Top = 41
        Width = 209
        Height = 511
        Align = alLeft
        BevelOuter = bvNone
        BorderWidth = 4
        TabOrder = 1
        object ClientsBox: TGroupBox
          Left = 4
          Top = 4
          Width = 201
          Height = 114
          Align = alClient
          Caption = 'Connected clients:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          object ClientsListBox: TListBox
            Left = 2
            Top = 15
            Width = 197
            Height = 97
            Align = alClient
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ItemHeight = 13
            ParentFont = False
            PopupMenu = PopupMenu2
            TabOrder = 0
            OnClick = ClientsListBoxClick
            OnDblClick = Chat1Click
          end
        end
        object DetailsBox: TGroupBox
          Left = 4
          Top = 118
          Width = 201
          Height = 88
          Align = alBottom
          Caption = 'Client details:'
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          object DetailsMemo: TMemo
            Left = 2
            Top = 15
            Width = 197
            Height = 71
            Align = alClient
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Lines.Strings = (
              'DetailsMemo')
            ParentFont = False
            TabOrder = 0
          end
        end
        object ActionPanel: TPanel
          Left = 4
          Top = 206
          Width = 201
          Height = 301
          Align = alBottom
          TabOrder = 2
          object Label21: TLabel
            Left = 8
            Top = 109
            Width = 118
            Height = 13
            Caption = 'Transaksi (sewa) terakhir'
          end
          object GetImageNowButton: TBitBtn
            Left = 9
            Top = 8
            Width = 190
            Height = 33
            Caption = 'Tangkap Gambar Klien'
            TabOrder = 0
            OnClick = GetImageNowButtonClick
            Glyph.Data = {
              66010000424D6601000000000000760000002800000014000000140000000100
              040000000000F000000000000000000000001000000000000000000000000000
              8000008000000080800080000000800080008080000080808000C0C0C0000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00300000000000
              00333333000030FFFFFFFFFFF03333330000330F0F0F0F0F0333333300000000
              FFFFFFF00003333300000FF800000008FF03333300000F9FFFFFFFF000033333
              00000FFFFFFFFFFFFF0333330000300000000000003333330000333000000000
              3333333300003330FFFF00703333333300003330F0000B307833333300003330
              F0CCC0BB0078333300003330F0CCC00BB300733300003330F00000F0BBB00733
              00003330FFFFFFF00BBB00830000333000000000BBB008330000333333333330
              0BBB00830000333333333333300BB008000033333333333333300B0000003333
              33333333333330000000}
          end
          object btnDisconnect: TBitBtn
            Left = 9
            Top = 43
            Width = 190
            Height = 25
            Caption = 'Paksa Klien Keluar'
            TabOrder = 1
            OnClick = kickKlien
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              04000000000000010000130B0000130B00001000000000000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
              3333333333FFFFF3333333333999993333333333F77777FFF333333999999999
              33333337777FF377FF3333993370739993333377FF373F377FF3399993000339
              993337777F777F3377F3393999707333993337F77737333337FF993399933333
              399377F3777FF333377F993339903333399377F33737FF33377F993333707333
              399377F333377FF3377F993333101933399377F333777FFF377F993333000993
              399377FF3377737FF7733993330009993933373FF3777377F7F3399933000399
              99333773FF777F777733339993707339933333773FF7FFF77333333999999999
              3333333777333777333333333999993333333333377777333333}
            NumGlyphs = 2
          end
          object GFront: TDBGrid
            Left = 1
            Top = 128
            Width = 199
            Height = 172
            Align = alBottom
            DataSource = DM1.DSFrontTable
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
            ParentFont = False
            ReadOnly = True
            TabOrder = 3
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnDrawColumnCell = GFrontDrawColumnCell
            Columns = <
              item
                Expanded = False
                FieldName = 'Station'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nilai'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'tipe'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'mulai'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'selesai'
                Width = 45
                Visible = True
              end>
          end
          object BitBtn2: TBitBtn
            Left = 9
            Top = 70
            Width = 190
            Height = 25
            Caption = 'Sembunyikan Hasil Tangkapan'
            TabOrder = 2
            OnClick = BitBtn2Click
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              04000000000000010000120B0000120B00001000000000000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
              5555555555FFFFF5555555555000005555555555F777775FF555555008877700
              55555557755FFF775F55550887000777055555755F777FFF75F550880FBFBF07
              705557F57755577FF7F55080F00000F07055575755777557F75F087F00B3300F
              77057F5757555757FF7F080B0B3B330B07057F757F5555757F7F0F0F0BBBB30F
              07057F757F5555757F7F0F0B0FBB3B0B08057F757FF55575757F0F7F00FFB00F
              780575F757FFF757F5755080F00000F0805557F75577755757F550F80FBFBF08
              8055575F775557755755550F8700078805555575FF77755F755555500FFF8800
              5555555775FFFF77555555555000005555555555577777555555}
            NumGlyphs = 2
          end
        end
      end
      object pnlMain: TPanel
        Left = 209
        Top = 41
        Width = 573
        Height = 511
        Align = alClient
        BevelOuter = bvLowered
        TabOrder = 2
        object ImageScrollBox: TScrollBox
          Left = 1
          Top = 18
          Width = 571
          Height = 435
          HorzScrollBar.Smooth = True
          VertScrollBar.Smooth = True
          Align = alClient
          Color = clWindow
          ParentColor = False
          TabOrder = 0
          object Image: TImage
            Left = 0
            Top = 0
            Width = 153
            Height = 177
            AutoSize = True
          end
          object Label22: TLabel
            Left = 40
            Top = 40
            Width = 361
            Height = 273
            AutoSize = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -24
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            Visible = False
            WordWrap = True
          end
        end
        object InfoLabel: TStaticText
          Left = 1
          Top = 1
          Width = 571
          Height = 17
          Align = alTop
          Alignment = taCenter
          Caption = 'InfoLabel'
          Color = clMedGray
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 1
        end
        object Protocol: TMemo
          Left = 1
          Top = 453
          Width = 571
          Height = 57
          Align = alBottom
          ReadOnly = True
          ScrollBars = ssVertical
          TabOrder = 2
        end
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'HOTSPOT'
      ImageIndex = 3
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 377
        Height = 552
        Align = alLeft
        TabOrder = 0
        object GMember: TDBGrid
          Left = 1
          Top = 42
          Width = 375
          Height = 164
          Align = alClient
          DataSource = DM1.DSMember
          PopupMenu = PopupMenu3
          ReadOnly = True
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = StartRent1Click
          Columns = <
            item
              Expanded = False
              FieldName = 'IDAnggota'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nama'
              Width = 89
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MAC'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Member'
              Width = 46
              Visible = True
            end>
        end
        object Panel5: TPanel
          Left = 1
          Top = 206
          Width = 375
          Height = 345
          Align = alBottom
          TabOrder = 1
          object RadioGroup1: TRadioGroup
            Left = 16
            Top = 48
            Width = 345
            Height = 41
            Caption = 'Pencarian / Filter'
            Columns = 4
            ItemIndex = 1
            Items.Strings = (
              'ID'
              'Nama'
              'MAC'
              'Free')
            TabOrder = 1
            OnClick = RadioGroup1Click
          end
          object edSearch: TEdit
            Left = 16
            Top = 16
            Width = 345
            Height = 21
            TabOrder = 0
            OnChange = RadioGroup1Click
          end
          object Memo1: TMemo
            Left = 16
            Top = 104
            Width = 345
            Height = 225
            Lines.Strings = (
              'Cara Menggunakan Form Sewa Hotspot'
              ''
              '- Jika anggota, minta kartunya, dan cari ID nya'
              
                '- Jika bukan anggota tapi pernah melakukan sewa internet, maka t' +
                'anya '
              'nama pelanggan tersebut, cari dengan melakukan filter nama'
              '- Jika belum pernah datang kemari, lakukan pencatatan MAC dan '
              'namanya, tawari jadi anggota'
              ''
              
                'cara mulai sewa, di ID yang telah diinputkan, klik kanan, dan pi' +
                'lih menu '
              'mulai sewa, '
              ''
              'di Mozilla, kopi paste mac dan namanya, pilih save'
              ''
              
                'jika telah selesai, klik kanan di tempat sewa, pilih menu selesa' +
                'i'
              ''
              'jangan lupa menghapus nama pelanggan, di mozilla')
            ReadOnly = True
            TabOrder = 2
          end
        end
        object Panel7: TPanel
          Left = 1
          Top = 1
          Width = 375
          Height = 41
          Align = alTop
          TabOrder = 2
          object BTregister: TButton
            Left = 8
            Top = 8
            Width = 177
            Height = 25
            Caption = 'Register Clien'
            TabOrder = 0
            OnClick = BTregisterClick
          end
        end
      end
      object Panel6: TPanel
        Left = 377
        Top = 0
        Width = 405
        Height = 552
        Align = alClient
        Caption = 'Panel6'
        TabOrder = 1
        object DBGrid2: TDBGrid
          Left = 1
          Top = 270
          Width = 403
          Height = 281
          Align = alBottom
          Color = clBtnShadow
          DataSource = DM1.DSFrontTable
          ReadOnly = True
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Station'
              Width = 111
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'mulai'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'selesai'
              Width = 49
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nilai'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'tipe'
              Width = 85
              Visible = True
            end>
        end
        object DBGrid3: TDBGrid
          Left = 1
          Top = 1
          Width = 403
          Height = 269
          Align = alClient
          DataSource = DM1.DsWTimer
          PopupMenu = PopupMenu4
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'idAnggota'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nama'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Jenis'
              Width = 75
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'mulai'
              Width = 38
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Jam'
              Title.Caption = 'Sewa (jam)'
              Width = 56
              Visible = True
            end>
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Pemasukan'
      ImageIndex = 1
      OnShow = TabSheet2Show
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 782
        Height = 552
        Align = alClient
        BevelOuter = bvNone
        Color = cl3DLight
        TabOrder = 0
        object GroupBox5: TGroupBox
          Left = 32
          Top = 16
          Width = 609
          Height = 129
          Caption = 'Printing'
          TabOrder = 0
          object Label4: TLabel
            Left = 296
            Top = 12
            Width = 33
            Height = 13
            Caption = 'Jumlah'
          end
          object Label5: TLabel
            Left = 384
            Top = 68
            Width = 41
            Height = 16
            Caption = 'Label5'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label17: TLabel
            Left = 8
            Top = 91
            Width = 505
            Height = 26
            Caption = 
              'Jangan lupa mengingatkan pada customer, bahwa untuk print gambar' +
              ' yang besar, kita mempunyai standar sebagaimana terlampir pada k' +
              'atalog harga. Ditanya dulu, apakah dia setuju dengan harga terse' +
              'but!'
            WordWrap = True
          end
          object Button12: TButton
            Left = 384
            Top = 32
            Width = 81
            Height = 25
            Caption = 'Hitung'
            TabOrder = 4
            OnClick = Button12Click
          end
          object SpinEdit1: TSpinEdit
            Left = 296
            Top = 32
            Width = 73
            Height = 22
            MaxValue = 0
            MinValue = 0
            TabOrder = 2
            Value = 0
          end
          object RadioGroup6: TRadioGroup
            Left = 136
            Top = 24
            Width = 145
            Height = 57
            Caption = 'Jenis'
            Columns = 2
            ItemIndex = 0
            Items.Strings = (
              'Biasa'
              'Spesial')
            TabOrder = 0
          end
          object RadioGroup7: TRadioGroup
            Left = 8
            Top = 24
            Width = 121
            Height = 57
            Caption = 'Kertas'
            ItemIndex = 0
            Items.Strings = (
              'B-NET'
              'Sendiri')
            TabOrder = 1
          end
          object CheckBox1: TCheckBox
            Left = 296
            Top = 64
            Width = 81
            Height = 17
            Caption = 'Discount'
            TabOrder = 3
          end
        end
        object GroupBox6: TGroupBox
          Left = 32
          Top = 160
          Width = 609
          Height = 81
          Caption = 'Scan'
          TabOrder = 1
          object Label19: TLabel
            Left = 16
            Top = 20
            Width = 33
            Height = 13
            Caption = 'Jumlah'
          end
          object Label20: TLabel
            Left = 200
            Top = 44
            Width = 41
            Height = 16
            Caption = 'Label5'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Button20: TButton
            Left = 104
            Top = 37
            Width = 81
            Height = 25
            Caption = 'Hitung'
            TabOrder = 1
            OnClick = Button20Click
          end
          object SpinEdit4: TSpinEdit
            Left = 16
            Top = 40
            Width = 73
            Height = 22
            MaxValue = 0
            MinValue = 0
            TabOrder = 0
            Value = 0
          end
        end
        object GroupBox1: TGroupBox
          Left = 32
          Top = 256
          Width = 609
          Height = 185
          Caption = 'Extra Inclome'
          TabOrder = 2
          object RadioGroup9: TRadioGroup
            Left = 16
            Top = 24
            Width = 241
            Height = 113
            Caption = 'Snack'
            ItemIndex = 0
            Items.Strings = (
              '100 rupiah (permen, dll)'
              '500 rupiah (gery, momogi, dll)'
              '1000 rupiah (beng-beng, slay olay, dll)'
              'spesial')
            TabOrder = 0
          end
          object RadioGroup8: TRadioGroup
            Left = 272
            Top = 24
            Width = 313
            Height = 113
            Caption = 'Non Snack'
            Columns = 2
            ItemIndex = 0
            Items.Strings = (
              'Service Komputer / Laptop'
              'Jual Beli Hardware'
              'Desain Grafis'
              'Pengetikan'
              'Software Order')
            TabOrder = 3
          end
          object Button14: TButton
            Left = 344
            Top = 144
            Width = 75
            Height = 25
            Caption = 'Hitung'
            TabOrder = 5
            OnClick = Button14Click
          end
          object SpinEdit3: TSpinEdit
            Left = 274
            Top = 143
            Width = 63
            Height = 26
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            MaxValue = 0
            MinValue = 0
            ParentFont = False
            TabOrder = 4
            Value = 0
          end
          object Button13: TButton
            Left = 88
            Top = 144
            Width = 75
            Height = 25
            Caption = 'Hitung'
            TabOrder = 2
            OnClick = Button13Click
          end
          object SpinEdit2: TSpinEdit
            Left = 18
            Top = 143
            Width = 63
            Height = 26
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            MaxValue = 0
            MinValue = 0
            ParentFont = False
            TabOrder = 1
            Value = 0
          end
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Pengeluaran'
      ImageIndex = 2
      OnShow = TabSheet3Show
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 782
        Height = 552
        Align = alClient
        BevelOuter = bvNone
        Color = cl3DLight
        TabOrder = 0
        object Label11: TLabel
          Left = 663
          Top = 64
          Width = 38
          Height = 13
          Alignment = taRightJustify
          Caption = 'Label11'
        end
        object Label12: TLabel
          Left = 32
          Top = 24
          Width = 118
          Height = 13
          Caption = 'Keterangan Pengeluaran'
        end
        object Label10: TLabel
          Left = 361
          Top = 64
          Width = 119
          Height = 13
          Caption = 'Total Pengeluaran bulan '
        end
        object Label13: TLabel
          Left = 361
          Top = 120
          Width = 90
          Height = 13
          Caption = 'Detail Pengeluaran'
        end
        object Label14: TLabel
          Left = 361
          Top = 48
          Width = 115
          Height = 13
          Caption = 'Total Pemasukan bulan '
        end
        object Label16: TLabel
          Left = 663
          Top = 48
          Width = 38
          Height = 13
          Alignment = taRightJustify
          Caption = 'Label11'
        end
        object Label15: TLabel
          Left = 361
          Top = 88
          Width = 59
          Height = 13
          Caption = 'Saldo bulan '
        end
        object Label18: TLabel
          Left = 663
          Top = 88
          Width = 38
          Height = 13
          Alignment = taRightJustify
          Caption = 'Label11'
        end
        object Shape1: TShape
          Left = 361
          Top = 83
          Width = 350
          Height = 1
        end
        object Label9: TLabel
          Left = 361
          Top = 264
          Width = 62
          Height = 13
          Caption = 'Gaji Operator'
        end
        object ComboBox1: TComboBox
          Left = 32
          Top = 47
          Width = 289
          Height = 21
          ItemHeight = 13
          TabOrder = 0
          OnKeyPress = ComboBox1KeyPress
        end
        object Button11: TButton
          Left = 32
          Top = 82
          Width = 89
          Height = 25
          Caption = 'Catat'
          TabOrder = 1
          OnClick = Button11Click
        end
        object GKeluar: TDBGrid
          Left = 32
          Top = 144
          Width = 289
          Height = 289
          Align = alCustom
          DataSource = DM1.DSHarianKeluar
          ReadOnly = True
          TabOrder = 2
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnCellClick = GKeluarCellClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Tanggal'
              Width = 154
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Outcome'
              Width = 78
              Visible = True
            end>
        end
        object GDetailKeluar: TDBGrid
          Left = 361
          Top = 144
          Width = 342
          Height = 113
          DataSource = DM1.DSDetailKeluar
          ReadOnly = True
          TabOrder = 3
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Jam'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'keterangan'
              Width = 168
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'jumlah'
              Width = 76
              Visible = True
            end>
        end
        object GGajiOp: TDBGrid
          Left = 361
          Top = 288
          Width = 342
          Height = 144
          DataSource = DM1.DSGajiOP
          ReadOnly = True
          TabOrder = 4
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'nip'
              Title.Caption = 'NIP'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nama'
              Width = 162
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'gaji'
              Title.Caption = 'Fee'
              Width = 80
              Visible = True
            end>
        end
      end
    end
  end
  object Timer1: TTimer
    OnTimer = Timer1Timer
    Left = 553
    Top = 152
  end
  object XPManifest1: TXPManifest
    Left = 504
    Top = 168
  end
  object MainMenu1: TMainMenu
    Left = 264
    Top = 8
    object File1: TMenuItem
      Caption = '&File'
      object EnableEditMenu1: TMenuItem
        Caption = 'Enable Edit Menu'
        OnClick = EnableEditMenu1Click
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object Exit1: TMenuItem
        Caption = '&Keluar'
        OnClick = Exit1Click
      end
    end
    object Edit21: TMenuItem
      Caption = '&Edit'
      Enabled = False
      object ransaksi1: TMenuItem
        Caption = '&Transaksi'
        OnClick = ransaksi1Click
      end
      object Operator2: TMenuItem
        Caption = '&Operator'
        OnClick = Operator2Click
      end
      object arifSewa1: TMenuItem
        Caption = 'Tarif Sewa'
        OnClick = arifSewa1Click
      end
      object arifWifi1: TMenuItem
        Caption = 'Tarif Wifi'
        OnClick = arifWifi1Click
      end
      object PrintScan1: TMenuItem
        Caption = 'Print/Scan'
        OnClick = PrintScan1Click
      end
      object WifiMember1: TMenuItem
        Caption = 'Wifi Member'
        OnClick = WifiMember1Click
      end
    end
    object Report1: TMenuItem
      Caption = '&Laporan'
      Enabled = False
      object Harian1: TMenuItem
        Caption = '&Bulanan'
        OnClick = Harian1Click
      end
      object GajiOperator1: TMenuItem
        Caption = '&Gaji Operator'
        OnClick = GajiOperator1Click
      end
    end
    object User1: TMenuItem
      Caption = '&Pengguna'
      object Operator1: TMenuItem
        Caption = '&Operator'
        OnClick = Operator1Click
      end
    end
    object Option1: TMenuItem
      Caption = '&Opsi'
      object HideProgram1: TMenuItem
        Caption = '&Sembunyikan saat minimize'
        OnClick = HideProgram1Click
      end
    end
    object Lainlain1: TMenuItem
      Caption = '&Help'
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 300
    Top = 8
    object Show1: TMenuItem
      Caption = 'Show Main Window'
      OnClick = Show1Click
    end
    object HideProgramtoTray1: TMenuItem
      Caption = '&Hide Program to Tray'
      OnClick = HideProgramtoTray1Click
    end
    object Close1: TMenuItem
      Caption = 'E&xit Program'
      OnClick = Close1Click
    end
  end
  object IdAntiFreeze: TIdAntiFreeze
    Left = 462
    Top = 450
  end
  object ThreadManager: TIdThreadMgrDefault
    Left = 400
    Top = 456
  end
  object TCPServer: TIdTCPServer
    Bindings = <>
    CommandHandlers = <>
    DefaultPort = 7676
    Greeting.NumericCode = 0
    MaxConnectionReply.NumericCode = 0
    OnConnect = TCPServerConnect
    OnExecute = TCPServerExecute
    OnDisconnect = TCPServerDisconnect
    ReplyExceptionCode = 0
    ReplyTexts = <>
    ReplyUnknownCommand.NumericCode = 0
    ThreadMgr = ThreadManager
    Left = 412
    Top = 392
  end
  object IdTCPServer1: TIdTCPServer
    Bindings = <>
    CommandHandlers = <>
    DefaultPort = 0
    Greeting.NumericCode = 0
    MaxConnectionReply.NumericCode = 0
    OnConnect = IdTCPServer1Connect
    OnExecute = IdTCPServer1Execute
    OnDisconnect = IdTCPServer1Disconnect
    ReplyExceptionCode = 0
    ReplyTexts = <>
    ReplyUnknownCommand.NumericCode = 0
    Left = 638
    Top = 307
  end
  object TimerKiller: TTimer
    Enabled = False
    Interval = 500
    OnTimer = TimerKillerTimer
    Left = 400
    Top = 427
  end
  object ChatServer: TIdTCPServer
    Bindings = <>
    CommandHandlers = <>
    DefaultPort = 0
    Greeting.NumericCode = 0
    MaxConnectionReply.NumericCode = 0
    OnExecute = ChatServerExecute
    OnDisconnect = ChatServerDisconnect
    ReplyExceptionCode = 0
    ReplyTexts = <>
    ReplyUnknownCommand.NumericCode = 0
    Left = 662
    Top = 171
  end
  object ChatSender: TIdTCPClient
    MaxLineAction = maException
    ReadTimeout = 0
    Port = 0
    Left = 630
    Top = 171
  end
  object PopupMenu2: TPopupMenu
    Left = 104
    Top = 117
    object Chat1: TMenuItem
      Caption = 'Chat'
      OnClick = Chat1Click
    end
  end
  object PopupMenu3: TPopupMenu
    Left = 296
    Top = 112
    object StartRent1: TMenuItem
      Caption = 'Mulai Sewa'
      OnClick = StartRent1Click
    end
    object CopyMAC1: TMenuItem
      Caption = 'Copy MAC'
      OnClick = CopyMAC1Click
    end
    object CopyNama1: TMenuItem
      Caption = 'Copy Nama'
      OnClick = CopyNama1Click
    end
  end
  object PopupMenu4: TPopupMenu
    Left = 501
    Top = 96
    object SelesaiSewa1: TMenuItem
      Caption = 'Selesai Sewa'
      OnClick = SelesaiSewa1Click
    end
    object Batal1: TMenuItem
      Caption = 'Batal'
      OnClick = Batal1Click
    end
    object Update1: TMenuItem
      Caption = 'Update'
      OnClick = Update1Click
    end
  end
  object Timer2: TTimer
    Left = 104
    Top = 8
  end
  object Timer3: TTimer
    Interval = 30000
    OnTimer = Timer3Timer
    Left = 373
    Top = 41
  end
end
