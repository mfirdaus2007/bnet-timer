unit timerkecil;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DB, ADODB, ExtCtrls, jpeg, XPMan;

type
  TFChanger = class(TForm)
    ADOConnection1: TADOConnection;
    ADOQuery1: TADOQuery;
    Timer1: TTimer;
    Image1: TImage;
    XPManifest1: TXPManifest;
    Label2: TLabel;
    Label1: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
  net      : array [1..10,1..10] of double;
  id ,timewindows      : integer;
  mulai    : ttime;
  procedure connectAdo;
  procedure disconnectAdo;
    procedure loadTimer;
    procedure loadTarif;

    function  setHargaSewa(tipe:byte;temp1:string):string;
    procedure Drag(var klik : TWMNCHitTest);message WM_NCHitTest;
    { Public declarations }
  end;

type  TWaktu = Record
  jam,menit:integer;
  end;
  
var
  FChanger: TFChanger;

Function  GetUserFromWindows: string;
function GetComputerNetName: string;
implementation

{$R *.dfm}
function GetUserFromWindows: string;
Var
   UserName : string;
   UserNameLen : Dword;
Begin
   UserNameLen := 255;
   SetLength(userName, UserNameLen) ;
   If GetUserName(PChar(UserName), UserNameLen) Then
     Result := Copy(UserName,1,UserNameLen - 1)
   Else
     Result := 'Unknown';
End;

function TFChanger.setHargaSewa(tipe:byte;temp1:string):string;
var
jam_sewa:TWaktu;
harga_sewa:double;
begin
 if length(temp1)=7 then
       temp1:='0'+temp1;
//    jam_sewa.jam:=strtoint(temp1[4]+temp1[5]);
//    jam_sewa.menit:=strtoint(temp1[7]+temp1[8]);
    jam_sewa.jam:=strtoint(temp1[1]+temp1[2]);
    jam_sewa.menit:=strtoint(temp1[4]+temp1[5]);
    harga_sewa:=jam_sewa.jam*net[tipe,4]+net[tipe,(jam_sewa.menit div 15)+1];
    if tipe = 4 then //tipe paket
      result:=Floattostr(net[tipe,4])
    else   result:=floattostr(harga_sewa);
end;

procedure TFChanger.disconnectAdo;
begin
 ADOConnection1.Connected:=false;
end;

procedure TFChanger.connectAdo;
begin
 ADOConnection1.Connected:=false;

 ADOConnection1.ConnectionString:='Provider=Microsoft.Jet.OLEDB.4.0;'+'Data Source='+ExtractFilePath(Application.ExeName)+'\db.mdb;'+'Persist Security Info=False';

ADOConnection1.Connected:=true;
end;

procedure TFChanger.LoadTarif;
var i:byte;
begin
connectAdo;

ADOQuery1.SQL.Text:='select * from tarif order by ID';
adoquery1.Open;
i:=1;
while not (adoquery1.Eof) do
  begin
      net[i,1]:=adoquery1['first'];
      net[i,2]:=adoquery1['second'];
      net[i,3]:=adoquery1['Third'];
      net[i,4]:=adoquery1['fourth'];

      adoquery1.Next;
      inc(i);
  end;

  ADOQuery1.Close;
  disconnectAdo;
end;

procedure TFChanger.loadTimer;
var
f:textfile;
waktu:string;
begin
  AssignFile(f,GetComputerNetName+'-'+GetUserFromWindows+'.txt');
  reset(f);
  readln(f,id);
  readln(f,waktu);
  closefile(f);
  mulai:=strtotime(waktu);
end;

procedure TFChanger.FormClose(Sender: TObject; var Action: TCloseAction);
begin
action:=cafree;
end;

procedure TFChanger.Drag(var klik : TWMNCHitTest);
begin
inherited; 
if (klik.Result = htClient) then 
  begin 
  if (klik.YPos <= Top + 26) then 
      klik.Result := htCaption; 
 
  end; 
end;

procedure TFChanger.FormCreate(Sender: TObject);
begin

  loadtarif;
  timewindows:=0;
  mulai:=strtotime('00:00:00');
  id:=5;
  if FileExists(GetComputerNetName+'-'+GetUserFromWindows+'.txt') then
    begin
        loadTimer;
    end;
end;

function GetComputerNetName: string;
var
  buffer: array[0..255] of char;
  size: dword;
begin
  size := 256;
  if GetComputerName(buffer, size) then
    Result := buffer
  else
    Result := ''
end;

procedure TFChanger.Timer1Timer(Sender: TObject);
begin
inc(timewindows);
  if (timewindows mod 2=0) then
  begin
    if not FileExists( GetComputerNetName+'-'+GetUserFromWindows+'.txt') then
       begin
          {if Visible then Hide;
          exit;}
          close;
       end;
  end;
  label1.Caption:=timetostr(now-mulai);
  label2.Caption:=format('%8.0m',[floattocurr(strtofloat( setHargaSewa(id,label1.Caption)))]);
end;

procedure TFChanger.FormShow(Sender: TObject);
begin

  SetWindowPos(Handle, // handle to window
               HWND_TOPMOST, // placement-order handle {*}
               700,  // horizontal position
               2,   // vertical position
               Width,
               Height,
               // window-positioning options
               SWP_NOSIZE);
ShowWindow(Application.Handle, SW_HIDE);
ShowWindow(FindWindow(nil, @Application.Title[1]), SW_HIDE);

end;

end.
