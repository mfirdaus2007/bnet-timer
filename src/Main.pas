unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, PsAPI,  StdCtrls, ExtCtrls;

type
  TForm1 = class(TForm)
    Timer1: TTimer;
    procedure Button1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Timer1Timer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
  timerwindows:integer;
    { Private declarations }
  public
    { Public declarations }
    num:string[3];
    filetoKill:array [1..100] of string[40];
    function FindProcesses (aKey : String; aList : TList = nil) : integer;
    function KillProcess (aProcessId : Cardinal) : boolean;
  end;

  function GetComputerNetName: string;
  function GetUserFromWindows: string;
  
var
  Form1: TForm1;

implementation

{$R *.dfm}

function GetUserFromWindows: string;
Var
   UserName : string;
   UserNameLen : Dword;
Begin
   UserNameLen := 255;
   SetLength(userName, UserNameLen) ;
   If GetUserName(PChar(UserName), UserNameLen) Then
     Result := Copy(UserName,1,UserNameLen - 1)
   Else
     Result := 'Unknown';
End;

function GetComputerNetName: string;
var
  buffer: array[0..255] of char;
  size: dword;
begin
  size := 256;
  if GetComputerName(buffer, size) then
    Result := buffer
  else
    Result := ''
end;

procedure TForm1.Button1Click(Sender: TObject);
var i:integer;
begin
i:=1;
while not (i>strtoint(num)) do
  begin
   KillProcess(FindProcesses(filetokill[i]));
   inc(i);
  end;
end;

function TForm1.FindProcesses (aKey : String; aList : TList = nil) : integer;
var
 szProcessName : Array [0..1024] of Char;
 ProcessName   : String;
 hProcess      : Integer;
 aProcesses    : Array [0..1024] of DWORD;
 cProcesses    : DWORD;
 cbNeeded      : Cardinal;
 i             : UINT;
 hMod          : HMODULE;
begin

 Result:= 0;
 aKey:= lowercase(akey);

 if not (EnumProcesses(@aProcesses, sizeof(aProcesses), cbNeeded)) then
    exit;

 // Calculate how many process identifiers were returned.
 cProcesses := cbNeeded div sizeof(DWORD);

 // Print the name and process identifier for each process.
 for i:= 0 to cProcesses - 1 do
 begin
   szProcessName := 'unknown';
   hProcess := OpenProcess(PROCESS_QUERY_INFORMATION or PROCESS_VM_READ,
                           FALSE, aProcesses[i]);

   // Get the process name.
   if (hProcess <> 0) then
      if (EnumProcessModules(hProcess, @hMod, sizeof(hMod),cbNeeded)) then
         GetModuleBaseName (hProcess, hMod, szProcessName, sizeof(szProcessName));

   ProcessName:= lowercase (szProcessName);

   CloseHandle(hProcess);
   if pos (aKey, ProcessName) <> 0 then
   begin
     result:=aProcesses[i];
     if aList <> nil then
        aList.add(Pointer (aProcesses[i]))
     else
       exit;
   end;
 end;

 if aList <> nil then
    Result:= aList.count
 else
    Result:=0;
end;

function TForm1.KillProcess (aProcessId : Cardinal) : boolean;
var
  hProcess : integer;
begin

  hProcess:= OpenProcess(PROCESS_ALL_ACCESS, TRUE, aProcessId);
  Result:= false;

  if (hProcess <>0 ) then
  begin
    Result:= TerminateProcess(hProcess, 0);
    exit;
  end;
end;


procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
button1Click(self);
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
if form1.Visible then form1.Hide;

inc(timerwindows);
  if timerwindows mod 2 = 0 then begin
    if not FileExists( GetComputerNetName+'-'+GetUserFromWindows+'.txt') then
       begin
          close;
       end;
  end;
button1click(self);
{if timerwindows mod 20 = 0 then begin
   timer1.Enabled:=false;
   close;
  end;}
end;

procedure TForm1.FormCreate(Sender: TObject);
var f:textfile;

i:byte;
begin
timerwindows:=0;

i:=1;
filetoKill[i]:='iexplore.exe';

if not (FileExists('kill.txt')) then
    begin
    num:=InputBox('Time Killer Configuration','Jumlah Service','4');
    assignfile(f,'kill.txt');
    rewrite(F);
    writeln(F,num);
      while not (i>strtoint(num)) do
      begin
       filetoKill[i]:=InputBox('First Time Killer Configuration','Nama Service','iexplore.exe');
       writeln(F,filetokill[i]);
       inc(i);
      end;
        closefile(F);
    end;
        assignfile(f,'kill.txt');
        reset(F);
        readln(f,num);
      while not (i>strtoint(num)) do
      begin
       //filetoKill[i]:=InputBox('First Time Killer Configuration','Nama Service','iexplore.exe');
       readln(F,filetokill[i]);
       inc(i);
      end;


        closefile(F);

    timer1.Enabled:=true;



end;

procedure TForm1.Button2Click(Sender: TObject);
begin
hide;
end;

procedure TForm1.FormShow(Sender: TObject);
begin
//ShowWindow(form1.Handle,SWP_HIDEWINDOW);
ShowWindow(Application.Handle, SW_HIDE);
ShowWindow(FindWindow(nil, @Application.Title[1]), SW_HIDE);
end;

end.
