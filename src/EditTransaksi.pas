unit EditTransaksi;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, ExtCtrls, DBCtrls, Grids, DBGrids, ComCtrls, StdCtrls;

type
  TFedit = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    Panel1: TPanel;
    DBNavigator1: TDBNavigator;
    GMasuk: TDBGrid;
    Panel3: TPanel;
    Gjamkerja: TDBGrid;
    DBNavigator3: TDBNavigator;
    Panel2: TPanel;
    DBNavigator2: TDBNavigator;
    Panel4: TPanel;
    Label1: TLabel;
    ComboBox1: TComboBox;
    ComboBox2: TComboBox;
    Button1: TButton;
    Panel5: TPanel;
    Button2: TButton;
    Edit1: TEdit;
    Label2: TLabel;
    Gkeluar: TDBGrid;
    Panel6: TPanel;
    Label3: TLabel;
    ComboBox3: TComboBox;
    Button3: TButton;
    ComboBox4: TComboBox;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Button1Click(Sender: TObject);
    procedure TabSheet1Show(Sender: TObject);
    procedure ComboBox2Change(Sender: TObject);
    procedure TampilkanSemua(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
    procedure TabSheet3Show(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure ComboBox4Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure GMasukDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure Label5Click(Sender: TObject);
    procedure GMasukCellClick(Column: TColumn);
    procedure GMasukKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Fedit: TFedit;

implementation

uses MainForm, DMserver;

{$R *.dfm}

procedure TFedit.FormClose(Sender: TObject; var Action: TCloseAction);
begin
action:=cafree;

fmain.updateIncome;
end;

procedure TFedit.Button1Click(Sender: TObject);
begin
if ComboBox2.ItemIndex=-1 then
  begin
  //showmessage('Item yang anda cari tidak ada!');
  exit;
  end;
dm1.QPemasukan.Close;
dm1.QPemasukan.SQL.Text:='select  pemasukan.* from pemasukan where id='+combobox1.Text+' order by pemasukan.No desc';
dm1.QPemasukan.Open;
end;

procedure TFedit.TabSheet1Show(Sender: TObject);
begin
ComboBox1.Clear;
ComboBox2.Clear;
Dm1.TJenis.First;
while not (Dm1.TJenis.Eof) do
  begin
    ComboBox1.Items.Add(inttostr(Dm1.TJenis['ID']));
    ComboBox2.Items.Add(Dm1.TJenis['nama']);
    Dm1.TJenis.Next;
  end;

end;

procedure TFedit.ComboBox2Change(Sender: TObject);
begin
combobox1.ItemIndex:=combobox2.ItemIndex;
Button1Click(self);
end;

procedure TFedit.TampilkanSemua(Sender: TObject);
begin
dm1.QPemasukan.Close;
dm1.QPemasukan.SQL.Text:='select  pemasukan.* from pemasukan  order by pemasukan.No desc';
dm1.QPemasukan.Open;
end;

procedure TFedit.Button2Click(Sender: TObject);
begin
dm1.QPengeluaran.Close;
dm1.QPengeluaran.SQL.Text:='select  pengeluaran.* from pengeluaran  where keterangan like '+#39+'%'+edit1.Text+'%'+#39+' order by pengeluaran.id desc';
dm1.QPengeluaran.Open;
end;

procedure TFedit.Edit1KeyPress(Sender: TObject; var Key: Char);
begin
if key=#13 then Button2Click(self);
end;

procedure TFedit.TabSheet3Show(Sender: TObject);
begin
ComboBox3.Items.Clear;
ComboBox4.Items.Clear;
dm1.TOperator.First;
while not(dm1.TOperator.Eof) do
  begin
    ComboBox3.Items.Add(dm1.TOperator['ID']);
    ComboBox4.Items.Add(dm1.TOperator['ID']+' '+dm1.tOperator['Nama']);
    dm1.TOperator.Next;
  end;
ComboBox3.Text:='';
ComboBox4.Text:='';
label4.Caption:=format('%8.2m',[floattocurr(0)]);
end;

procedure TFedit.Button3Click(Sender: TObject);
begin
dm1.QJamKerja.Close;
dm1.QJamKerja.SQL.Text :='select Jamkerja.* from jamkerja order by Jamkerja.id Desc ';
dm1.QJamKerja.Open;
label4.Caption:=format('%8.2m',[floattocurr(0)]);
end;

procedure TFedit.ComboBox4Change(Sender: TObject);
var jumlah:double;
begin
ComboBox3.ItemIndex:=ComboBox4.ItemIndex;
dm1.QJamKerja.Close;
dm1.QJamKerja.SQL.Text:='select Jamkerja.* from jamkerja where NIP = '+#31+ComboBox3.text+#31+ ' order by Jamkerja.id Desc';
dm1.QJamKerja.Open;
jumlah:=0;
dm1.QJamKerja.DisableControls;
while not (dm1.QJamKerja.Eof) do
  begin
  jumlah:=jumlah+dm1.QJamKerja['fee'];
  dm1.QJamKerja.Next;
  end;

dm1.QJamKerja.First;
dm1.QJamKerja.EnableControls;
label4.Caption:=format('%8.2m',[floattocurr(jumlah)]);
end;

procedure TFedit.FormCreate(Sender: TObject);
begin
dm1.QPemasukan.Close;
dm1.QPemasukan.Open;
dm1.QPengeluaran.Close;
dm1.QPengeluaran.Open;
dm1.QJamKerja.Close;
dm1.QJamKerja.Open;

end;

procedure TFedit.GMasukDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
if dm1.QPemasukan['no']mod 2=0 then
         GMasuk.Canvas.Brush.Color:=ClGreen
else
         GMasuk.Canvas.Brush.Color:=ClTeal;

  GMasuk.DefaultDrawColumnCell (Rect, DataCol, Column, State);
end;

procedure TFedit.Label5Click(Sender: TObject);
var
  i: Integer;
  sum : Single;
begin
  if Gmasuk.SelectedRows.Count > 0 then
  begin
    sum := 0;
    with Gmasuk.DataSource.DataSet do
    begin
      for i := 0 to Gmasuk.SelectedRows.Count-1 do
      begin
        GotoBookmark(Pointer(Gmasuk.SelectedRows.Items[i]));
        sum:= sum + dm1.qpemasukan.FieldByName('Nilai').AsFloat;
      end;
    end;
    Label5.Caption :=  formatFloat(',0',sum);
  end
end;


procedure TFedit.GMasukCellClick(Column: TColumn);
var
  i: Integer;
  sum : Single;
begin
  if Gmasuk.SelectedRows.Count > 0 then
  begin
    sum := 0;
    with Gmasuk.DataSource.DataSet do
    begin
      for i := 0 to Gmasuk.SelectedRows.Count-1 do
      begin
        GotoBookmark(Pointer(Gmasuk.SelectedRows.Items[i]));
        sum:= sum + dm1.qpemasukan.FieldByName('Nilai').AsFloat;
      end;
    end;
    Label5.Caption :=  formatFloat(',0',sum);
  end
end;

procedure TFedit.GMasukKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
  var
  i: Integer;
  sum : single;
begin


 if Gmasuk.SelectedRows.Count > 0 then
  begin
    sum := 0;
    with Gmasuk.DataSource.DataSet do
    begin
      for i := 0 to Gmasuk.SelectedRows.Count-1 do
      begin
        GotoBookmark(Pointer(Gmasuk.SelectedRows.Items[i]));
        sum:= sum + dm1.qpemasukan.FieldByName('Nilai').AsFloat;
      end;
    end;
    Label5.Caption :=  formatFloat(',0',sum);
  end


end;

end.
