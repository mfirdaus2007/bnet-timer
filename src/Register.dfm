object FRegister: TFRegister
  Left = 402
  Top = 191
  BorderStyle = bsDialog
  Caption = 'FRegister'
  ClientHeight = 164
  ClientWidth = 312
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 40
    Top = 28
    Width = 28
    Height = 13
    Caption = 'Nama'
  end
  object Label3: TLabel
    Left = 40
    Top = 60
    Width = 23
    Height = 13
    Caption = 'MAC'
  end
  object Label2: TLabel
    Left = 40
    Top = 92
    Width = 40
    Height = 13
    Caption = 'Anggota'
  end
  object EdNama: TEdit
    Left = 96
    Top = 24
    Width = 160
    Height = 21
    TabOrder = 0
    OnKeyPress = EdNamaKeyPress
  end
  object EdMac: TEdit
    Left = 96
    Top = 56
    Width = 160
    Height = 21
    TabOrder = 1
    OnKeyPress = EdMacKeyPress
  end
  object Btsimpan: TButton
    Left = 96
    Top = 120
    Width = 75
    Height = 25
    Caption = 'Simpan'
    TabOrder = 2
    OnClick = BtsimpanClick
  end
  object BtBatal: TButton
    Left = 176
    Top = 120
    Width = 75
    Height = 25
    Caption = 'Batal'
    TabOrder = 3
    OnClick = BtBatalClick
  end
  object RBYa: TRadioButton
    Left = 96
    Top = 88
    Width = 65
    Height = 17
    Caption = 'Ya'
    Checked = True
    TabOrder = 4
    TabStop = True
    OnKeyPress = RBYaKeyPress
  end
  object RBTidak: TRadioButton
    Left = 176
    Top = 88
    Width = 65
    Height = 17
    Caption = 'Tidak'
    TabOrder = 5
    OnKeyPress = RBYaKeyPress
  end
end
