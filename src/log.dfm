object Flog: TFlog
  Left = 314
  Top = 178
  BorderStyle = bsDialog
  Caption = 'Log Terakhir Koneksi Klien'
  ClientHeight = 418
  ClientWidth = 451
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object StringGrid1: TStringGrid
    Left = 0
    Top = 0
    Width = 451
    Height = 200
    Align = alClient
    ColCount = 6
    DefaultColWidth = 80
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
    TabOrder = 0
    ColWidths = (
      32
      101
      95
      58
      59
      80)
  end
  object Panel1: TPanel
    Left = 0
    Top = 200
    Width = 451
    Height = 218
    Align = alBottom
    TabOrder = 1
    DesignSize = (
      451
      218)
    object Button1: TButton
      Left = 16
      Top = 8
      Width = 417
      Height = 33
      Anchors = [akLeft, akTop, akRight]
      Caption = 'Simpan Semua Log ke Database'
      TabOrder = 0
      OnClick = Button1Click
    end
    object Memo1: TMemo
      Left = 1
      Top = 48
      Width = 449
      Height = 169
      Align = alBottom
      Lines.Strings = (
        
          'Jika melihat form ini secara otomatis, berarti terjadi masalah d' +
          'engan server / client. '
        ''
        
          '-   Jika server mengalami restart, dan client masih hidup, maka ' +
          'abaikan / tutup form ini. '
        ''
        
          '-   Jika client yang melakukan restart, maka tutup program B-Net' +
          ' Server dan nyalakan ulang '
        'program. '
        ''
        '-   Jika keduanya mengalami restart, maka ada 2 kemungkinan, '
        
          '1. Jika ke2 komputer menyala kurang dari 10 menit, maka abaikan/' +
          'tutup form ini. '
        
          '2. Jika lebih dari 10 menit, maka tekan tombol simpan log terseb' +
          'ut. dan pastikan jumlah bayar '
        'pelanggan sesuai dengan yang tercatat di pendapatan.')
      TabOrder = 1
    end
  end
end
