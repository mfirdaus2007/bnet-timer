object FReportharian: TFReportharian
  Left = 459
  Top = 177
  Width = 374
  Height = 440
  BorderIcons = [biSystemMenu]
  Caption = 'Report Bulanan ::: ...'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 366
    Height = 349
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Pemasukan'
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 358
        Height = 35
        Align = alTop
        TabOrder = 0
        object Label2: TLabel
          Left = 16
          Top = 12
          Width = 3
          Height = 13
        end
        object Label3: TLabel
          Left = 256
          Top = 12
          Width = 88
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'Label3'
        end
      end
      object DBGrid1: TDBGrid
        Left = 0
        Top = 35
        Width = 358
        Height = 286
        Align = alClient
        DataSource = DM1.DSHARIANmasuk
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Tanggal'
            Width = 188
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Income'
            Width = 125
            Visible = True
          end>
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Pengeluaran'
      ImageIndex = 1
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 358
        Height = 35
        Align = alTop
        TabOrder = 0
        object Label4: TLabel
          Left = 16
          Top = 12
          Width = 3
          Height = 13
        end
        object Label5: TLabel
          Left = 256
          Top = 12
          Width = 88
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'Label3'
        end
      end
      object DBGrid2: TDBGrid
        Left = 0
        Top = 35
        Width = 358
        Height = 286
        Align = alClient
        DataSource = DM1.DSHarianKeluar
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Tanggal'
            Width = 185
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Outcome'
            Visible = True
          end>
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Fee Operator'
      ImageIndex = 2
      object DBGrid3: TDBGrid
        Left = 0
        Top = 35
        Width = 358
        Height = 286
        Align = alClient
        DataSource = DM1.DSGajiOP
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'nip'
            Title.Caption = 'NIP'
            Width = 66
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nama'
            Width = 146
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'gaji'
            Title.Caption = 'Fee'
            Width = 72
            Visible = True
          end>
      end
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 358
        Height = 35
        Align = alTop
        TabOrder = 1
        object Label6: TLabel
          Left = 16
          Top = 12
          Width = 3
          Height = 13
        end
        object Label7: TLabel
          Left = 256
          Top = 12
          Width = 88
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'Label3'
        end
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 349
    Width = 366
    Height = 57
    Align = alBottom
    TabOrder = 1
    object Label1: TLabel
      Left = 317
      Top = 24
      Width = 32
      Height = 13
      Alignment = taRightJustify
      Caption = 'Label1'
    end
    object Button2: TButton
      Left = 120
      Top = 16
      Width = 50
      Height = 25
      Caption = '&Next'
      TabOrder = 2
      OnClick = Button2Click
    end
    object Button1: TButton
      Left = 8
      Top = 16
      Width = 50
      Height = 25
      Caption = '&Prev'
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button3: TButton
      Left = 64
      Top = 16
      Width = 50
      Height = 25
      Caption = '&Current'
      TabOrder = 1
      OnClick = Button3Click
    end
  end
end
