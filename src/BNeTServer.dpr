program BNeTServer;

uses
  Forms,
  MainForm in 'MainForm.pas' {Fmain},
  reportgaji in 'reportgaji.pas' {FreportGaji},
  GajiOperator in 'GajiOperator.pas' {Fgaji},
  ReportHarian in 'ReportHarian.pas' {FReportharian},
  JamKerja in 'JamKerja.pas' {FJamKerja},
  EditTransaksi in 'EditTransaksi.pas' {Fedit},
  log in 'log.pas' {Flog},
  chatForm in 'chatForm.pas' {FChat},
  Operator in 'Operator.pas' {FOperator},
  DMserver in 'DMserver.pas' {DM1: TDataModule},
  editTarif in 'editTarif.pas' {FEditTarif},
  editSewa in 'editSewa.pas' {FPSGO},
  passwords in 'passwords.pas' {Fpasswords},
  editTarifWifi in 'editTarifWifi.pas' {FWifi},
  Register in 'Register.pas' {FRegister},
  Member in 'Member.pas' {FMember},
  rekapGaji in 'rekapGaji.pas' {QRRekap: TQuickRep};

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'B-NET Server';
  Application.CreateForm(TFmain, Fmain);
  Application.CreateForm(TDM1, DM1);
  Application.CreateForm(TFChat, FChat);
  Application.CreateForm(TFpasswords, Fpasswords);
  Application.CreateForm(TQRRekap, QRRekap);
  Application.Run;
end.
