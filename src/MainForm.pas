unit MainForm;

interface

uses
  Windows,inifiles, Jpeg, winsock, IdStack, IdSocketHandle, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, XPMan, DB, ADODB, Menus, Spin, ComCtrls,
  Grids, DBGrids, ShellApi,AppEvnts,ReportHarian, Buttons, IdComponent,
  IdTCPServer, IdThreadMgr, IdThreadMgrDefault, IdBaseComponent,
  IdAntiFreezeBase, IdAntiFreeze, IdTCPConnection, IdTCPClient, DBCtrls;

const
  WM_NOTIFYICON  = WM_USER+333;

type TRECChat = record
jam:TTime;
nama:string[15];
textChat:string[200];
end;
  
type TRECpemasukan = record
tanggal,mulai,selesai:TDateTime;
station:string[15];
id:byte;

end;



type
  PClient   = ^TClient;
  TClient   = record
    PeerIP      : string[15];            { Cleint IP address }
    HostName    : String[40];
    username,
    mulai,tipe  : string[15];
    id          : byte;
           { Hostname }
    kick,TakeShot    : boolean;
    connection       : boolean;
    Connected,                           { Time of connect }
    LastAction  : TDateTime;             { Time of last transaction }
    Thread      : Pointer;              { Pointer to thread }
    chatPort:integer;
  end;

type Tuserrecord=record
  username:string[35];
  mulai:string[15];
  id:byte;
  tipe:string[15];
  chatPort:integer;
end;

type
  TFmain = class(TForm)
    Timer1: TTimer;
    XPManifest1: TXPManifest;
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    Exit1: TMenuItem;
    Edit21: TMenuItem;
    ransaksi1: TMenuItem;
    Report1: TMenuItem;
    Harian1: TMenuItem;
    User1: TMenuItem;
    Operator1: TMenuItem;
    Lainlain1: TMenuItem;
    GajiOperator1: TMenuItem;
    HideProgram1: TMenuItem;
    Option1: TMenuItem;
    PopupMenu1: TPopupMenu;
    Show1: TMenuItem;
    Close1: TMenuItem;
    HideProgramtoTray1: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    Panel2: TPanel;
    Label11: TLabel;
    Label12: TLabel;
    Label10: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label16: TLabel;
    Label15: TLabel;
    Label18: TLabel;
    Shape1: TShape;
    Label9: TLabel;
    ComboBox1: TComboBox;
    Button11: TButton;
    GKeluar: TDBGrid;
    GDetailKeluar: TDBGrid;
    GGajiOp: TDBGrid;
    Panel3: TPanel;
    GroupBox5: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    Label17: TLabel;
    Button12: TButton;
    SpinEdit1: TSpinEdit;
    RadioGroup6: TRadioGroup;
    RadioGroup7: TRadioGroup;
    CheckBox1: TCheckBox;
    GroupBox6: TGroupBox;
    Label19: TLabel;
    Label20: TLabel;
    Button20: TButton;
    SpinEdit4: TSpinEdit;
    GroupBox1: TGroupBox;
    RadioGroup9: TRadioGroup;
    RadioGroup8: TRadioGroup;
    Button14: TButton;
    SpinEdit3: TSpinEdit;
    Button13: TButton;
    SpinEdit2: TSpinEdit;
    Panel1: TPanel;
    Label3: TLabel;
    Label7: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label8: TLabel;
    pnlLeft: TPanel;
    ClientsBox: TGroupBox;
    ClientsListBox: TListBox;
    DetailsBox: TGroupBox;
    DetailsMemo: TMemo;
    ActionPanel: TPanel;
    GetImageNowButton: TBitBtn;
    btnDisconnect: TBitBtn;
    pnlMain: TPanel;
    ImageScrollBox: TScrollBox;
    Image: TImage;
    InfoLabel: TStaticText;
    Protocol: TMemo;
    IdAntiFreeze: TIdAntiFreeze;
    ThreadManager: TIdThreadMgrDefault;
    TCPServer: TIdTCPServer;
    IdTCPServer1: TIdTCPServer;
    GFront: TDBGrid;
    Label21: TLabel;
    BitBtn2: TBitBtn;
    N1: TMenuItem;
    TimerKiller: TTimer;
    Label22: TLabel;
    ChatServer: TIdTCPServer;
    ChatSender: TIdTCPClient;
    PopupMenu2: TPopupMenu;
    Chat1: TMenuItem;
    Operator2: TMenuItem;
    TabSheet4: TTabSheet;
    arifSewa1: TMenuItem;
    PrintScan1: TMenuItem;
    EnableEditMenu1: TMenuItem;
    Panel4: TPanel;
    GMember: TDBGrid;
    Panel5: TPanel;
    arifWifi1: TMenuItem;
    Panel6: TPanel;
    DBGrid2: TDBGrid;
    DBGrid3: TDBGrid;
    RadioGroup1: TRadioGroup;
    edSearch: TEdit;
    PopupMenu3: TPopupMenu;
    StartRent1: TMenuItem;
    Memo1: TMemo;
    PopupMenu4: TPopupMenu;
    SelesaiSewa1: TMenuItem;
    CopyMAC1: TMenuItem;
    CopyNama1: TMenuItem;
    Batal1: TMenuItem;
    Panel7: TPanel;
    BTregister: TButton;
    WifiMember1: TMenuItem;
    Update1: TMenuItem;
    Timer2: TTimer;
    Timer3: TTimer;
    procedure kickKlien(Sender: TObject);
    procedure ransaksi1Click(Sender: TObject);
    procedure Exit1Click(Sender: TObject);
    procedure Harian1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure GajiOperator1Click(Sender: TObject);
    procedure HideProgram1Click(Sender: TObject);
    procedure Show1Click(Sender: TObject);
    procedure Close1Click(Sender: TObject);
    procedure HideProgramtoTray1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Timer1Timer(Sender: TObject);
    procedure TabSheet3Show(Sender: TObject);
    procedure Button12Click(Sender: TObject);
    procedure Button11Click(Sender: TObject);
    procedure Button20Click(Sender: TObject);
    procedure TabSheet1Show(Sender: TObject);
    procedure TabSheet2Show(Sender: TObject);
    procedure Button13Click(Sender: TObject);
    procedure Button14Click(Sender: TObject);
    procedure ComboBox1KeyPress(Sender: TObject; var Key: Char);
    procedure GetImageNowButtonClick(Sender: TObject);
    procedure btnDisconnectClick(Sender: TObject);
    procedure ClientsListBoxClick(Sender: TObject);
    procedure AutoCaptureCheckBoxClick(Sender: TObject);
    procedure TCPServerDisconnect(AThread: TIdPeerThread);
    procedure TCPServerExecute(AThread: TIdPeerThread);
    procedure TCPServerConnect(AThread: TIdPeerThread);
    procedure IdTCPServer1Execute(AThread: TIdPeerThread);
    procedure IdTCPServer1Connect(AThread: TIdPeerThread);
    procedure BitBtn2Click(Sender: TObject);
    procedure ShowErrorrLog1Click(Sender: TObject);
    procedure IdTCPServer1Disconnect(AThread: TIdPeerThread);
    procedure TimerKillerTimer(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure GKeluarCellClick(Column: TColumn);
    procedure Chat1Click(Sender: TObject);
    procedure ChatServerDisconnect(AThread: TIdPeerThread);
    procedure ChatServerExecute(AThread: TIdPeerThread);
    procedure Operator2Click(Sender: TObject);
    procedure Operator1Click(Sender: TObject);
    procedure arifSewa1Click(Sender: TObject);
    procedure PrintScan1Click(Sender: TObject);
    procedure EnableEditMenu1Click(Sender: TObject);
    procedure arifWifi1Click(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
    procedure StartRent1Click(Sender: TObject);
    procedure CopyMAC1Click(Sender: TObject);
    procedure SelesaiSewa1Click(Sender: TObject);
    procedure CopyNama1Click(Sender: TObject);
    procedure Batal1Click(Sender: TObject);
    procedure BTregisterClick(Sender: TObject);
    procedure WifiMember1Click(Sender: TObject);
    procedure GFrontDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure Update1Click(Sender: TObject);
    procedure Timer3Timer(Sender: TObject);
  private
    tnid: TNotifyIconData;
    HMainIcon: HICON;
    nilai_pemasukan,nilai_pengeluaran : double;

  procedure CMClickIcon(var msg: TMessage); message WM_NOTIFYICON;
    { Private declarations }
  protected
    procedure WMSyscommand(Var msg: TWmSysCommand); message WM_SYSCOMMAND;

  public
  klienterputus : Tuserrecord;
  net           : array [1..10,1..10] of double;
  timewindows2:integer;
  nama_bulan:array [1..12]of string;
  tampilkanpesan:boolean;
  
  procedure showPesan;
  procedure simpanSewa(station,nip:string;id : integer;mulai,selesai,sewa:string);
  procedure updatePengeluaran;
  procedure updateIncome;
  procedure connectAdo;
  procedure DisConnectAdo;
  procedure LoadOperator;
  procedure showpopup;
  procedure RemoveDeadIcons;
  procedure isicombo;
  procedure isiDetailPengeluaran;
  procedure updateSaldo;
  procedure updateGaji;
  procedure RefreshListDisplay;
  procedure RefreshImage(const ClientDNS, ImageName : string);
  procedure simpanSewarecord(Tanggal:TDaTE;station,nip:string;id : integer;mulai,selesai:TTIME);
  function  setHargaSewa(tipe:byte;temp1:string):string;
  procedure writeLog;
  procedure showlog;
  procedure loadTarif;
  procedure SendChat(const Chat:TRECChat);
    { Public declarations }
  end;
type  TWaktu = Record
  jam,menit:integer;
  end;



type TuserLOG=record
  username:string[15];
  mulai:string[15];
  error:string[15];
  tipe:string[15];
  id:byte;
end;
TFileUserLOG=file of TuserLOG;

var
  Fmain: TFmain;
  Clients         : TThreadList;

  ipAddress,chatWith,ClientChatipAddress:String[15];
  DefaultServerPort,
  receiverPort,  
  chatWithPort,ChatServerPort,ChatClientPort : integer;
  NIPoperator:string;

Function GetIPAddress():String;
function StrANumber(const S: string): Boolean;



implementation

uses  ClipBrd,jamKerja,GajiOperator,  StrUtils, Math, EditTransaksi, chatForm, Operator,
  log, DMserver, editTarif, editSewa, passwords, editTarifWifi, Register,
  Member;

{$R *.dfm}

procedure TFmain.SendChat(const Chat:TRECChat);
begin
   try
    ChatSender.Host:= ClientChatipAddress;
    ChatSender.Port:= chatwithport;
    ChatSender.Connect;

    sleep(100);
    ChatSender.Writebuffer(chat,sizeof(chat),true);
    sleep(100);
    ChatSender.DisconnectSocket;
    ChatSender.Disconnect;

    Except
      on E:Exception do
    end;


end;

procedure TFmain.kickKlien(Sender: TObject);
var
SelClient: PClient;
begin
  if ClientsListBox.ItemIndex = -1 then
  begin
    ShowMessage('Pilih client dulu!');
    Exit;
  end;

  if MessageDlg('Paksa klien ini keluar?',mtConfirmation,[MByes,MBNo],0) = IDNo then
      begin
        exit;
      end;

  try
   SelClient := PClient(Clients.LockList.Items[ClientsListBox.ItemIndex]);
   SelClient.kick:=true;
   finally
       Clients.UnlockList;
   end;

end;

procedure tFmain.showPesan;
begin
     if tampilkanpesan then
       begin
         beep;
         SetWindowPos(Handle,HWND_TOPMOST,0, 0, 0, 0,SWP_NOMOVE or SWP_NOSIZE or SWP_SHOWWINDOW);
         SetWindowPos(Handle,HWND_NOTOPMOST,0, 0, 0, 0,SWP_NOMOVE or SWP_NOSIZE or SWP_SHOWWINDOW);
         if not visible then show;
         label22.Caption:='Jam : '+timetostr(time)+#13+
                          'Client : '+klienterputus.username+#13+
                          'Tipe : '+klienterputus.tipe+#13+
                          'Telah memutus koneksi,'+#13+
                          'cek pemakaian user di tabel transaksi terakhir!';
         beep;
       end;
end;

function TFmain.setHargaSewa(tipe:byte;temp1:string):string;
var
jam_sewa:TWaktu;
harga_sewa:double;
jam,menit,deti,mili:word;
begin
     DecodeTime(strtotime(temp1),jam,menit,deti,mili);
    jam_sewa.jam:= jam;
    jam_sewa.menit:= menit;
    harga_sewa:=jam_sewa.jam*net[tipe,4]+net[tipe,(jam_sewa.menit div 15)+1];
    if tipe = 4 then //tipe paket
      result:=Floattostr(net[tipe,4])
    else   result:=floattostr(harga_sewa);
end;

procedure TFmain.LoadTarif;
var i:byte;
begin


dm1.Qtarif.Close;
dm1.Qtarif.Open;
i:=1;
while not (dm1.Qtarif.Eof) do
  begin
      net[i,1]:=dm1.Qtarif['first'];
      net[i,2]:=dm1.Qtarif['second'];
      net[i,3]:=dm1.Qtarif['Third'];
      net[i,4]:=dm1.Qtarif['fourth'];

      dm1.Qtarif.Next;
      inc(i);
  end;

  dm1.Qtarif.Close;  

end;

procedure TFmain.showlog;
var

  temprec:TuserLOG;
  f:TFileUserLOG;
  i:integer;
  harga:string[20];
begin
if not (FileExists('log.dat'))then
begin
  showmessage('Tidak terdeteksi gagal shutdown');
  exit;
end;
Flog:=TFlog.Create(self);
i:=1;
  AssignFile(f,'log.dat');
  reset(f);
  while not eof(f) do
      begin
      read(f,temprec);
      Flog.StringGrid1.Cells[0,i]:=inttostr(i);
      Flog.StringGrid1.Cells[1,i]:=temprec.username;
      Flog.StringGrid1.Cells[2,i]:=temprec.tipe;
      Flog.StringGrid1.Cells[3,i]:=temprec.mulai;
      Flog.StringGrid1.Cells[4,i]:=temprec.error;
      harga:=fmain.setHargaSewa(temprec.id,timetostr(strtotime(temprec.error)-strtotime(temprec.mulai)));
      harga:= format('%8.2m',[floattocurr(strtofloat(harga))]);
      Flog.StringGrid1.Cells[5,i]:=harga;
      Flog.StringGrid1.RowCount:=i+1;
      inc(i);
      end;
  closefile(f);

 Flog.Show;


end;

procedure TFmain.writeLog;
var
  SelClient: PClient;
  i:integer;
  temprec:TUserLog;
  f:TFileUserLOG;
begin
  if ClientsListBox.Items.Count > 0 then
    begin
    AssignFile(f,'log.dat');
    Rewrite(f);

     for i:=0 to ClientsListBox.Items.Count-1 do
      begin
        try
          SelClient := PClient(Clients.LockList.Items[i]);
          temprec.username:=SelClient.username;
          temprec.mulai:=SelClient.mulai;
          temprec.id:=SelClient.id;
          temprec.tipe:=selclient.tipe;
          temprec.error:=timetostr(time);
//          temprec.harga:=seth
          write(f,temprec);
        finally
          Clients.UnlockList;
        end;
      end;
    closefile(f);
    end;
end; (* ClientsListBox Click *)

procedure TFmain.RefreshImage(const ClientDNS, ImageName : string);
begin
    Image.Picture.LoadFromFile(ImageName);
    InfoLabel.Caption := 'Screen shot from ' + ClientDNS;
    if BitBtn2.Caption= 'Tampilkan Hasil Tangkapan' then BitBtn2Click(self);
end; (* RefreshImage *)

procedure TFmain.RefreshListDisplay;
var
  AClient :PClient;
  i:integer;
begin
  GetImageNowButton.Enabled := False;
  btnDisconnect.Enabled:=false;
  

  ClientsListBox.Clear;
  DetailsMemo.Clear;

  with Clients.LockList do
  try
    for i := 0 to Count-1 do
    begin
      AClient := Items[i];
      ClientsListBox.AddItem(AClient.HostName+' : '+AClient.username,TObject(AClient));
    end;
  finally
    Clients.UnlockList;
  end;
  btnDisconnect.Enabled:=ClientsListBox.Items.Count > 0;
  GetImageNowButton.Enabled := ClientsListBox.Items.Count > 0;

end; (* RefreshListDisplay *)

procedure TFmain.updateGaji;
var temp : string;
bulan,tahun,tahun2,bulan2:integer;
bulandepan,bulanini:string;
begin
temp:=datetostr(date);
bulan:=strtoint(temp[4]+temp[5]);
tahun:=strtoint(temp[7]+temp[8]+temp[9]+temp[10]);

bulan2:=bulan+1;
tahun2:=tahun;

if bulan=12 then
   begin
    bulan2:=1;
    tahun2:=tahun+1;
   end;



   bulandepan:='1/'+inttostr(bulan2)+'/'+inttostr(tahun2);
   bulanini:='1/'+inttostr(bulan)+'/'+inttostr(tahun);
   dm1.QGajiOp.Close;
   dm1.QGajiOp.Parameters[0].Value:=strtodate(bulandepan);
   dm1.QGajiOp.Parameters[1].Value:=strtodate(bulanini);
   dm1.QGajiOp.Open;

end;

procedure TFmain.disconnectAdo;
begin
    dm1.ADOConnection1.Connected:=false;
end;

procedure TFmain.connectAdo;

var g:TextFile;
test:string;
begin
dm1.ADOConnection1.Connected:=false;
assignfile(g,'koneksi.txt');
reset(g);
read(g,test);
CloseFile(g);
dm1.ADOConnection1.ConnectionString:=test;
dm1.ADOConnection1.Connected:=true;
end;

procedure TFmain.RemoveDeadIcons;
begin
 Shell_NotifyIcon(NIM_DELETE, @tnid);
       Application.ProcessMessages;
end;

procedure TFmain.showpopup;
var
CurPos: TPoint;
begin
GetCursorPos(CurPos);
PopupMenu1.Popup(CurPos.x, CurPos.y);
end;

procedure TFmain.WMSyscommand(Var msg: TWmSysCommand);
begin 
  inherited;   
  if (msg.cmdtype and $FFF0) = SC_MINIMIZE then 
      begin
        if HideProgram1.Checked then
           hide
        else
           ShowWindow(Fmain.Handle, SW_HIDE);
//           ShowWindow(theForm.Handle, SW_RESTORE) ;
      end;
end;

procedure tFmain.CMClickIcon(var msg: TMessage);
begin 
  case msg.lparam of 
    WM_LBUTTONDOWN : Show;
    WM_RBUTTONdown : showpopup;
  end; 
end; 

procedure TFmain.updateIncome;
var income:currency;
tanggal:string;
begin
tanggal:=datetostr(date);
DM1.QFront.Close;
Dm1.QFront.Parameters[0].Value:=date;
Dm1.QFront.Open;


DM1.QupdateInc.close;
DM1.QupdateInc.Parameters[0].Value:=date;
DM1.QupdateInc.Open;
if DM1.QupdateInc['income']=null then
  income:=floattocurr(0)
  else income:=DM1.QupdateInc['income'];
label8.Caption:=format('%8.2m',[income]);

DM1.QupdateInc.Close;

end;



procedure TFmain.ransaksi1Click(Sender: TObject);
var Fedit:TFedit;
begin
Fedit:=TFedit.Create(self);
Fedit.ShowModal;
end;

procedure TFmain.Exit1Click(Sender: TObject);
begin
close;
end;

procedure TFmain.Harian1Click(Sender: TObject);
var
 fReportHarian: tfReportHarian;
begin
 fReportHarian:=TfReportHarian.Create(self);
 fReportHarian.ShowMOdal;

end;

procedure TFmain.LoadOperator;
begin
 dm1.QUmum.SQL.Text:='select * from jamkerja order by id desc';
 dm1.QUmum.Open;
 dm1.QUmum.First;
 if dm1.QUmum['selesai']=null then
  nipOperator:=dm1.qumum['nip']
 else
  nipOperator:='001001';

  //showmessage(NIPoperator);
  dm1.QUmum.Close;
end;

procedure TFmain.FormCreate(Sender: TObject);
var
  Bindings,Bindings2,Bindings3: TIdSocketHandles;
  appINI:TIniFIle;

begin
//NIPoperator:='';
tampilkanpesan:=true;
if FileExists('log.dat') then showlog;
label1.Caption:=formatdateTime('dddd, dd mmmm yyyy',date);
nama_bulan[1]:='Januari';
nama_bulan[2]:='Februari';
nama_bulan[3]:='Maret';
nama_bulan[4]:='April';
nama_bulan[5]:='Mei';
nama_bulan[6]:='Juni';
nama_bulan[7]:='Juli';
nama_bulan[8]:='Agustus';
nama_bulan[9]:='September';
nama_bulan[11]:='November';
nama_bulan[10]:='Oktober';
nama_bulan[12]:='Desember';

ClientChatipAddress:='0.0.0.0';
chatWithport:=0;

timewindows2:=0;
  HMainIcon                := LoadIcon(MainInstance, 'MAINICON');

  Shell_NotifyIcon(NIM_DELETE, @tnid);

  tnid.cbSize              := sizeof(TNotifyIconData);
  tnid.Wnd                 := handle;
  tnid.uID                 := 123;
  tnid.uFlags              := NIF_MESSAGE or NIF_ICON or NIF_TIP;
  tnid.uCallbackMessage    := WM_NOTIFYICON;
  tnid.hIcon               := HMainIcon;
  tnid.szTip               := 'B-Net Timer';

  Shell_NotifyIcon(NIM_ADD, @tnid);


  appINI := TIniFile.Create(ChangeFileExt(Application.ExeName,'.ini')) ;
  try
    ipAddress:= appIni.ReadString('IP','IPAddreess','192.168.1.2');
    DefaultServerPort:= appini.ReadInteger('Port','DefaultServerPort',7676);
    receiverPort:=appini.ReadInteger('port','receiverPort',115);
    ChatServerPort:=appini.ReadInteger('port','ChatServerPort',300);
    ChatClientPort:=appini.ReadInteger('port','ChatClientPort',200);
  finally
    appINI.Free;
  end;


    Bindings2:= TIdSocketHandles.Create(IdTCPServer1);

    try
    Bindings2.Add.Port:=receiverPort;
    bindings2.Add.IP:=IPAddress;
    IdTCPServer1.Bindings:=bindings2;
    IdTCPServer1.Active:=true;
    Protocol.Lines.Add('Record receiver is listening on port : '+inttostr(receiverPort));
    except on E:Exception do
      Protocol.Lines.Add('Record receiver failed!');
    end;

    Bindings3:= TIdSocketHandles.Create(ChatServer);
    try
    Bindings3.Add.Port:=ChatServerPort;
    bindings3.Add.IP:=IPAddress;
    ChatServer.Bindings:=bindings3;
    ChatServer.Active:=true;
    Protocol.Lines.Add('Chat listener is listening on port : '+inttostr(ChatServerPort))
    except on E:Exception do
      Protocol.Lines.Add('Chat listener failed!');
    end;

  //setup and start TCPServer
  Bindings := TIdSocketHandles.Create(TCPServer);
  //
  try
    with Bindings.Add do
    begin
      IP := IPAddress;
      Port := DefaultServerPort;
    end;

    
    try
      TCPServer.Bindings:=Bindings;

      TCPServer.Active:=True;
    except on E:Exception do
      ShowMessage(E.Message);
    end;
  finally
    Bindings.Free;
    
  end;
  //setup TCPServer

  //other startup settings
  Clients := TThreadList.Create;
  Clients.Duplicates := dupAccept;



  DetailsMemo.Clear;
  InfoLabel.Caption := 'Waiting...';

  RefreshListDisplay;

  if TCPServer.Active then
  begin
    Protocol.Lines.Add('B-NET Server running on ' + TCPServer.Bindings[0].IP + ':' + IntToStr(TCPServer.Bindings[0].Port));
  end;
end; (* Form Create *)





procedure TFmain.GajiOperator1Click(Sender: TObject);
var Fgaji:TFgaji;
begin
  Fgaji:=TFgaji.Create(self);
  Fgaji.ShowModal;

end;

procedure TFmain.HideProgram1Click(Sender: TObject);
begin
  if HideProgram1.Checked then
      HideProgram1.Checked:=false
  else
      HideProgram1.Checked:=true;
end;

procedure TFmain.Show1Click(Sender: TObject);
begin
 show;
end;

procedure TFmain.Close1Click(Sender: TObject);
begin
close;
end;

procedure TFmain.HideProgramtoTray1Click(Sender: TObject);
begin
 hide;
end;

procedure TFmain.FormClose(Sender: TObject; var Action: TCloseAction);
var
  ClientsCount : integer;
   appINI  :TIniFile; 

begin
  appINI := TIniFile.Create(ChangeFileExt(Application.ExeName,'.ini')) ;
  try
    appIni.WriteString('IP','IPAddreess',IPAddress);
    appIni.WriteInteger('Port','DefaultServerPort',DefaultServerPort);
    appIni.WriteInteger('port','receiverPort',receiverPort);
    appIni.WriteInteger('port','ChatServerPort',ChatServerPort);
    appIni.WriteInteger('port','ChatClientPort',ChatClientPort);
  finally
    appINI.Free;
  end;

  try
    ClientsCount := Clients.LockList.Count;
  finally
    Clients.UnlockList;
  end;

  if (ClientsCount > 0) then //and (TCPServer.Active) then
    begin
      Action := caNone;
      ShowMessage('Disconnect dulu semua client!');
    end
  else
  begin

    TCPServer.Active := False;
   IdTCPServer1.Active:=false;
    Clients.Free;
  end;
deletefile('log.dat');  
RemoveDeadIcons;
end; (* Form Close *)




procedure TFmain.Timer1Timer(Sender: TObject);
begin
   label2.Caption:=timetostr(time);

end;

procedure TFmain.TabSheet3Show(Sender: TObject);
begin
    isicombo;
    updatePengeluaran;
    updateSaldo;
    updateGaji;
end;

procedure TFmain.isicombo;
begin
    ComboBox1.Items.Clear;
    ComboBox1.text:='';
    ComboBox1.SetFocus;
    dm1.QUmum.Close;
    dm1.QUmum.SQL.Text:='select distinct keterangan from pengeluaran';
    dm1.QUmum.Open;

  while not (dm1.QUmum.Eof) do
  begin
     ComboBox1.Items.Add(dm1.QUmum['keterangan']);
     dm1.QUmum.Next;
  end;
  dm1.QUmum.Close;
end;



procedure TFmain.Button12Click(Sender: TObject);
var
harga,disc : integer;
total:string;
lembar:integer;
begin
dm1.QUmum.Close;
dm1.QUmum.SQL.Text:='select * from printing where id=1';
dm1.QUmum.Open;
Harga:=dm1.QUmum['harga'];

if radiogroup6.ItemIndex = 1 then
      harga:=strtoint(InputBox('Harga printing warna ::..','Masukkan Harga (rupiah) ','0'));
if RadioGroup7.ItemIndex = 1 then
      harga:=harga-50;
lembar:=spinedit1.Value;
if (lembar <= 0) or (harga<=0) then
   begin
     exit;
   end;
if CheckBox1.Checked then
   begin
      disc:=strtoint(InputBox('Discount!','Gratis 1 lembar tiap : ','5'));;
      lembar:=lembar-(lembar div disc);
   end;
   //if total=null then total:=0;
   total:=format('%8.2m',[floattocurr(lembar*harga)]);
   label5.Caption:=intTostr(lembar)+' x '+inttostr(Harga)+' = '+total;
   showmessage('Printing Value : '+ total);
   simpanSewa(inttostr(lembar),nipoperator,0,timetostr(time),timetostr(time),floattostr(lembar*harga));

   spinedit1.Value:=0;
   RadioGroup7.ItemIndex:=0;
   RadioGroup6.ItemIndex:=0;
   CheckBox1.Checked:=false;
   updateIncome;

end;

procedure TFmain.Button11Click(Sender: TObject);
var jumlah:string;
begin
if ComboBox1.Text='' then
 begin
   messagedlg('Keterangan pengeluaran harus diisi!',mtError,[mbok],0);
   exit;
 end;
 jumlah:=InputBox('Input Jumlah Pengeluaran','Jumlah kas keluar (rupiah) :','0');

 if (strtofloat(jumlah)<0) then
    begin
      messagedlg('Jumlah pengeluaran salah',mtError,[mbok],0);
      exit;
    end;

 if jumlah = '0' then
     exit;
  dm1.qsimpankeluar.Close;
  dm1.qsimpankeluar.Parameters[0].Value:=date;
  dm1.qsimpankeluar.Parameters[1].Value:=time;
  dm1.qsimpankeluar.Parameters[2].Value:=combobox1.Text;
  dm1.qsimpankeluar.Parameters[3].Value:=jumlah;
  dm1.qsimpankeluar.ExecSQL;
 isicombo;
 updatePengeluaran;
 updateSaldo;

end;
procedure TFmain.updateSaldo;
var temp : string;
total : double;
bulan,tahun,tahun2,bulan2:integer;
bulanini,bulandepan:string;
begin
temp:=datetostr(date);
bulan:=strtoint(temp[4]+temp[5]);
tahun:=strtoint(temp[7]+temp[8]+temp[9]+temp[10]);
total:=0;


bulan2:=bulan+1;
tahun2:=tahun;

if bulan=12 then
   begin
    bulan2:=1;
    tahun2:=tahun+1;
   end;


   bulandepan:='1/'+inttostr(bulan2)+'/'+inttostr(tahun2);
   bulanini:='1/'+inttostr(bulan)+'/'+inttostr(tahun);
   dm1.QHarianMasuk.Close;
   dm1.QHarianMasuk.Parameters[0].Value:=strtodate(bulandepan);
   dm1.QHarianMasuk.Parameters[1].Value:=strtodate(bulanini);
   dm1.QHarianMasuk.Open;
   dm1.QHarianMasuk.DisableControls;

   if dm1.QHarianMasuk['Income']=null then
      begin
      label16.Caption:=format ('%8.2m',[floattocurr(total)]);
      label18.Caption:=format ('%8.2m',[floattocurr(total)]);
        exit;
      end;

   while not (dm1.QHarianMasuk.Eof) do
   begin
      total:=total+dm1.QHarianMasuk['Income'];
      dm1.QHarianMasuk.Next;
   end;
   dm1.QHarianMasuk.First;
   dm1.QHarianMasuk.EnableControls;
  if total=null then total:=0;
  nilai_pemasukan:=total;
  label16.Caption:=format ('%8.2m',[floattocurr(total)]);
label18.Caption:=format ('%8.2m',[floattocurr(nilai_pemasukan-nilai_pengeluaran)]);



end;

procedure TFmain.updatePengeluaran;
var temp : string;
total : double;
bulan,tahun,tahun2,bulan2:integer;
bulanini,bulandepan:string;
begin
temp:=datetostr(date);
bulan:=strtoint(temp[4]+temp[5]);
tahun:=strtoint(temp[7]+temp[8]+temp[9]+temp[10]);
total:=0;
bulan2:=bulan+1;
tahun2:=tahun;

if bulan=12 then
   begin
    bulan2:=1;
    tahun2:=tahun+1;
   end;

   bulandepan:='1/'+inttostr(bulan2)+'/'+inttostr(tahun2);
   bulanini:='1/'+inttostr(bulan)+'/'+inttostr(tahun);
   DM1.QHarianKeluar.Close;
   DM1.QharianKeluar.Parameters[0].Value:=strtodate(bulandepan);
   DM1.QharianKeluar.Parameters[1].Value:=strtodate(bulanini);
   DM1.QharianKeluar.Open;
   DM1.QharianKeluar.DisableControls;

   if DM1.QharianKeluar['Outcome']=null then
      begin
      label11.Caption:=format ('%8.2m',[floattocurr(0)]);
        exit;
      end;

   while not (DM1.QharianKeluar.Eof) do
   begin
      total:=total+DM1.QharianKeluar['Outcome'];
      DM1.QharianKeluar.Next;
   end;
   DM1.QharianKeluar.First;
   DM1.QharianKeluar.EnableControls;
  if total=null then total:=0;
  nilai_pengeluaran:=total;
  label11.Caption:=format ('%8.2m',[floattocurr(total)]);
  label10.Caption:='Total Pengeluaran bulan  '+nama_bulan[bulan]+' : ';
  isiDetailPengeluaran;

  label14.Caption:='Total Pemasukan bulan '+nama_bulan[bulan]+' : ';
  label15.Caption:='Saldo bulan '+nama_bulan[bulan]+' : ';

end;

procedure TFmain.isiDetailPengeluaran;

begin
dm1.QDetailKeluar.Close;
if DM1.QharianKeluar['tanggal']=null then exit;
dm1.QDetailKeluar.Parameters[0].Value:=DM1.QharianKeluar['tanggal'];
dm1.QDetailKeluar.Open;
end;

procedure TFmain.simpanSewaRecord(Tanggal:TDaTE;station,nip:string;id : integer;mulai,selesai:TTIME);
var
harga,waktu:string[15];
begin

dm1.QSimpanSewa.Close;
dm1.QSimpanSewa.Parameters[0].Value:=tanggal;
dm1.QSimpanSewa.Parameters[1].Value:=station;
dm1.QSimpanSewa.Parameters[2].Value:=id;
dm1.QSimpanSewa.Parameters[3].Value:=mulai;
dm1.QSimpanSewa.Parameters[4].Value:=selesai;

waktu:=timetostr(selesai-mulai);
harga:=setHargaSewa(id,waktu);
//showmessage(datetostr(tanggal)+station+inttostr(id)+timetostr(mulai)+timetostr(selesai)+harga);
dm1.QSimpanSewa.Parameters[5].Value:=strtofloat(harga);
dm1.QSimpanSewa.Parameters[6].Value:=nipoperator;
dm1.QSimpanSewa.ExecSQL;
end;

procedure TFmain.simpanSewa(station,nip:string;id : integer;mulai,selesai,sewa:string);
begin
dm1.QSimpanSewa.Close;
dm1.QSimpanSewa.Parameters[0].Value:=date;
dm1.QSimpanSewa.Parameters[1].Value:=station;
dm1.QSimpanSewa.Parameters[2].Value:=id;
dm1.QSimpanSewa.Parameters[3].Value:=strtotime(mulai);
dm1.QSimpanSewa.Parameters[4].Value:=strtotime(selesai);
dm1.QSimpanSewa.Parameters[5].Value:=strtofloat(sewa);
dm1.QSimpanSewa.Parameters[6].Value:=nipoperator;
dm1.QSimpanSewa.ExecSQL;
end;

procedure TFmain.Button20Click(Sender: TObject);
var
harga : integer;
total:string;
lembar:integer;
begin
dm1.QUmum.Close;
dm1.QUmum.SQL.Text:='select harga from printing where id=2';
dm1.QUmum.Open;
Harga:=dm1.QUmum['harga'];
dm1.QUmum.Close;

lembar:=spinedit4.Value;
if (lembar <= 0) or (harga<=0) then
   begin
     exit;
   end;

   //if total=null then total:=0;
   total:=format('%8.2m',[floattocurr(lembar*harga)]);
   label20.Caption:=intTostr(lembar)+' x '+inttostr(Harga)+' = '+total;
   ShowMessage('Scan Value : '+ total);

   simpansewa(inttostr(lembar),nipoperator,13,timetostr(time),timetostr(time),floattostr(lembar*harga));

   spinedit4.Value:=0;


   updateIncome;

end;

procedure TFmain.TabSheet1Show(Sender: TObject);
begin
updateIncome; 
end;

procedure TFmain.TabSheet2Show(Sender: TObject);
begin
label5.Caption:='Rp0,00';label20.Caption:='Rp0,00';
end;

procedure TFmain.Button13Click(Sender: TObject);
var
id : string;
harga,lembar : double;
begin
if spinedit2.Value <= 0 then
    begin
        showmessage('Beli berapa?');
        exit;
    end;

 harga:=0;
case RadioGroup9.ItemIndex of
    0 : begin
        id:='14';
        harga:=100;
        end;
    1 : begin
        id:='15';
        harga:=500;
        end;
    2 : begin
        id:='16';
        harga:=1000;
        end;
    3 : begin
        id:='99';
        harga:=strtoFloat(InputBox('Harga ','Masukkan barang spesial (rupiah)','0'));
        end;
    end;
  lembar:=  spinedit2.Value;
  if harga <= 0 then exit;
      showmessage('Total = '+ format('%8.2m',[floattocurr(lembar*harga)]));

   simpanSewa(inttostr(spinedit2.Value),nipoperator,strtoint(id),timetostr(time),timetostr(time),Floattostr(lembar*harga));

   spinedit2.Value:=0;
   updateIncome;
end;

procedure TFmain.Button14Click(Sender: TObject);
var
id : string;
harga,lembar : double;
begin
if spinedit3.Value <= 0 then
    begin
        showmessage('beli berapa?');
        exit;
    end;

    harga:=0;
case RadioGroup8.ItemIndex of
    0 : begin
        id:='11';
        harga:=strtoFloat(InputBox('Keuntungan Service','Masukkan Keuntungan service (rupiah)','0'));
        end;
    1 : begin
        id:='7';
        harga:=strtoFloat(InputBox('Keuntungan Hardware','Masukkan Keuntungan Hardware (rupiah)','0'));
        end;
    2 : begin
        id:='8';
        harga:=strtoFloat(InputBox('Keuntungan Desain','Masukkan Keuntungan Desain (rupiah)','0'));
        end;
    3 : begin
        id:='9';
        harga:=strtoFloat(InputBox('Keuntungan Pengetikan','Masukkan Keuntungan Pengetikan (rupiah)','0'));
        end;
    4 : begin
        id:='10';
        harga:=strtoFloat(InputBox('Keuntungan Software','Masukkan Keuntungan Software (rupiah)','0'));
        end;

    end;

  lembar:=  spinedit3.Value;
  if harga <= 0 then exit;
  showmessage('total = '+ format('%8.2m',[floattocurr(lembar*harga)]));

  simpansewa(floattostr(spinedit3.Value),nipoperator,strtoint(id),timetostr(time),timetostr(time),floattostr(lembar*harga));

   spinedit3.Value:=0;
   updateIncome;

end;

procedure TFmain.ComboBox1KeyPress(Sender: TObject; var Key: Char);
begin
if key = #13 then
button11click(self);
end;

procedure TFmain.GetImageNowButtonClick(Sender: TObject);
var
  SelClient : PClient;
begin
  if ClientsListBox.ItemIndex = -1 then
  begin
    if Sender is TBitBtn then ShowMessage('Pilih client dulu!');
    Exit;
  end;

  try
    SelClient := PClient(Clients.LockList.Items[ClientsListBox.ItemIndex]);
    SelClient.TakeShot := True;
  finally
    Clients.UnLockList
  end;
  
  //refresh DetailsMemo
  ClientsListBoxClick(Sender);
end; (* GetImageNowButton Click *)




procedure TFmain.btnDisconnectClick(Sender: TObject);
var
SelClient: PClient;
begin
  if ClientsListBox.ItemIndex = -1 then
  begin
    ShowMessage('Pilih client dulu!');
    Exit;
  end;
  try

   SelClient := PClient(Clients.LockList.Items[ClientsListBox.ItemIndex]);
   SelClient.connection:=false;
   finally
       Clients.UnlockList;
   end;

end;

procedure TFmain.ClientsListBoxClick(Sender: TObject);
var
  SelClient: PClient;
begin
    DetailsMemo.Clear;

    if ClientsListBox.ItemIndex <> -1 then
    begin
      try
        SelClient := PClient(Clients.LockList.Items[ClientsListBox.ItemIndex]);
        with DetailsMemo do
        begin
          Lines.Add('IP : ' + SelClient.PeerIP);
          //Lines.Add('Host name : ' + SelClient.HostName);
          //Lines.Add('User name : ' + SelClient.username);
          Lines.Add('Mulai : ' + SelClient.mulai);
          //Lines.Add('ID Sewa : ' + inttostr(SelClient.id));
          Lines.Add('Tipe Sewa : ' + SelClient.tipe);
          Lines.Add('Connected : ' + TimeToStr(SelClient.Connected));
          //Lines.Add('Last shot : ' + DateTimeToStr(SelClient.LastAction));
          //Lines.Add('Waiting : ' + BoolToStr(SelClient.TakeShot, True));

          ClientChatipAddress:=SelClient.PeerIP;
          chatWithport:=selclient.chatPort;
          chatwith:=selclient.username;
          fchat.Caption:='Melakukan chating dengan "'+chatwith+'"';
        end;
      finally
        Clients.UnlockList;
      end;
    end;
end; (* ClientsListBox Click *)

procedure TFmain.AutoCaptureCheckBoxClick(Sender: TObject);
begin

end; (* AutoCaptureCheckBoxClick *)

procedure TFmain.TCPServerDisconnect(AThread: TIdPeerThread);
var
  Client: PClient;

begin
  Client := PClient(AThread.Data);

  try
    Clients.LockList.Remove(Client);
    Protocol.Lines.Add (TimeToStr(Time)+' Disconnect from "' + Client.HostName+' : '+Client.username+'"');
    klienTerputus.username :=client.HostName+'-'+client.username;
    klienTerputus.id:= client.id;
    klienTerputus.tipe:=client.tipe;

    showPesan;
    if BitBtn2.Caption='Sembunyikan Hasil Tangkapan' then BitBtn2Click(self);

       
  finally
    Clients.UnlockList;
  end;
  FreeMem(Client);
  AThread.Data := nil;

  RefreshListDisplay;
  

end; (* TCPServer Disconnect *)

procedure TFmain.TCPServerExecute(AThread: TIdPeerThread);
var
  Client : PClient;
  Command : string;
  Size : integer;
  PicturePathName : string;
  ftmpStream : TFileStream;
begin
  if not AThread.Terminated and AThread.Connection.Connected then
  begin
    Client := PClient(AThread.Data);
    Client.LastAction := Now;

    Command := AThread.Connection.ReadLn;
    if Command = 'CheckMe' then
    begin

    if client.connection=false then
        begin
         AThread.Connection.WriteLn('disconnect');
       Client.connection:=true;
         AThread.Connection.WriteLn('DONE');
         ClientsListBoxClick(nil);
         exit;
        end;

     if client.kick then
        begin
         AThread.Connection.WriteLn('kick');
          Client.kick:=false;
         AThread.Connection.WriteLn('DONE');
         ClientsListBoxClick(nil);
         exit;
        end;
        
      if Client.TakeShot = True then
      begin
        Client.TakeShot := False;

        AThread.Connection.WriteLn('TakeShot');

        PicturePathName := ExtractFileDir(ParamStr(0)) + '\' + Client.HostName +'('+Client.username+')'+ '-Screen.JPG';

        if FileExists (PicturePathName) then DeleteFile(PicturePathName);
        ftmpStream := TFileStream.Create(PicturePathName,fmCreate);
        Size := AThread.Connection.ReadInteger;
        AThread.Connection.ReadStream(fTmpStream,Size,False);
        FreeAndNil(fTmpStream);

        AThread.Connection.WriteLn('DONE');

        RefreshImage('"'+Client.HostName+' : '+client.username+'"', PicturePathName);
        ClientsListBoxClick(nil);
      end
      else
        AThread.Connection.WriteLn('DONE');

    end;
  end;
end;

procedure TFmain.TCPServerConnect(AThread: TIdPeerThread);
var
username:tuserrecord ;
  NewClient: PClient;
begin
  GetMem(NewClient, SizeOf(TClient));

  AThread.Connection.ReadBuffer(username,sizeof(username));
  NewClient.chatPort    := username.chatPort;
  NewClient.connection  := true;
  NewClient.PeerIP      := AThread.Connection.Socket.Binding.PeerIP;
  NewClient.HostName    := GStack.WSGetHostByAddr(NewClient.PeerIP);
  NewClient.username    := username.username;   //'Operator';
  NewClient.mulai       := username.mulai;
  NewClient.id          := username.id;
  NewClient.tipe        := username.tipe;
  NewClient.TakeShot    := False;
  NewClient.kick        := False;
  NewClient.Connected   := Now;
  NewClient.LastAction  := NewClient.Connected;
  NewClient.Thread      := AThread;

  AThread.Data := TObject(NewClient);

  try
    Clients.LockList.Add(NewClient);
  finally
    Clients.UnlockList;
  end;

  Protocol.Lines.Add(TimeToStr(Time)+' Connection from "' + NewClient.HostName + ' : '+newclient.username+'" on ' + NewClient.PeerIP);
  RefreshListDisplay;
  //ShowMessage('asdfasdf');
end; (* TCPServer Connect *)

Function GetIPAddress():String;
type
  pu_long = ^u_long;
var
  varTWSAData : TWSAData;
  varPHostEnt : PHostEnt;
  varTInAddr : TInAddr;
  namebuf : Array[0..255] of char;
begin
  If WSAStartup($101,varTWSAData) <> 0 Then
  Result := '0.0.0.0'
  Else Begin
    gethostname(namebuf,sizeof(namebuf));
    varPHostEnt := gethostbyname(namebuf);
    varTInAddr.S_addr := u_long(pu_long(varPHostEnt^.h_addr_list^)^);
    Result := inet_ntoa(varTInAddr);
  End;
  WSACleanup;
end;
procedure TFmain.IdTCPServer1Execute(AThread: TIdPeerThread);
var tempREc:TRECpemasukan;
begin
    Athread.Connection.ReadBuffer(tempREC,sizeof(tempREC));
    simpansewarecord(temprec.tanggal,temprec.station,NIPoperator,temprec.id,temprec.mulai,temprec.selesai);
    updateIncome;
end;

procedure TFmain.IdTCPServer1Connect(AThread: TIdPeerThread);
begin
protocol.Lines.Add(timetostr(time)+' : Accept record from record agent...');
end;

procedure TFmain.BitBtn2Click(Sender: TObject);
begin
if BitBtn2.Caption='Sembunyikan Hasil Tangkapan' then
    begin
      BitBtn2.Caption:='Tampilkan Hasil Tangkapan';
      image.Hide;
      label22.Show;
    end
else
    begin
      BitBtn2.Caption:='Sembunyikan Hasil Tangkapan';
      image.Show;
      label22.Hide;
    end;
end;

procedure TFmain.ShowErrorrLog1Click(Sender: TObject);
begin
showlog;
end;

procedure TFmain.IdTCPServer1Disconnect(AThread: TIdPeerThread);
begin
protocol.Lines.Add(timetostr(time)+' : Agent disconnected..');
end;

procedure TFmain.TimerKillerTimer(Sender: TObject);
begin
inc (timewindows2);
if ClientsListBox.Items.Count=0 then
    begin
    timerkiller.Enabled:=false;
    close;
    exit;
    end;

if tampilkanpesan then
     tampilkanpesan:=false;
if not (ClientsListBox.Items.Count=0) then
    begin
    ClientsListBox.ItemIndex:=0;
    btnDisconnectClick(self);
    end;
if (timewindows2 mod 30*2=0) then application.Terminate; //mengatasi saat gagal disconeksi


end;

procedure TFmain.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
if ClientsListBox.Items.Count>0 then
  begin
  CanClose:=false;
   if MessageDlg('Putuskan semua klien dan tutup aplikasi?',mtConfirmation,[MByes,MBNo],0) = IDYes then
    begin

   canclose:=true;
   protocol.Lines.Add('Memutus semua client...');
   Timer1.Enabled:=false;
   TimerKiller.Enabled:=true;
      end;
  end;


end;

procedure TFmain.GKeluarCellClick(Column: TColumn);
begin
isiDetailPengeluaran;
end;

procedure TFmain.Chat1Click(Sender: TObject);
begin
//if Not ClientsListBox.Items.Count=0 then 
    FChat.Show;
end;

procedure TFmain.ChatServerDisconnect(AThread: TIdPeerThread);
begin
athread.Data.Free;
AThread.Data := Nil;
end;

procedure TFmain.ChatServerExecute(AThread: TIdPeerThread);
var Chat:TRECChat;
begin
AThread.Connection.ReadBuffer(Chat,SIZEOF(Chat));
FChat.Memo1.Lines.Add(timetostr(chat.jam)+' '+chat.nama+ ' -> '+ chat.textChat);


end;

procedure TFmain.Operator2Click(Sender: TObject);
var Foperator:TFoperator;
begin
  Foperator:=TFOperator.Create(self);
  Foperator.ShowModal;
end;

procedure TFmain.Operator1Click(Sender: TObject);
var FJamkerja:tFjamkerja;
begin
  FJamkerja:=tFJamkerja.Create(self);
  FJamkerja.ShowModal;
end;

procedure TFmain.arifSewa1Click(Sender: TObject);
var FeditTarif:TFEditTarif;
begin
   FeditTarif:=TFEditTarif.Create(self);
   FeditTarif.ShowModal;
end;

procedure TFmain.PrintScan1Click(Sender: TObject);
var FPSGO:TFPSGO;
begin
     FPSGO:=TFPSGO.Create(self);
     Fpsgo.ShowModal;
end;

procedure TFmain.EnableEditMenu1Click(Sender: TObject);
var passkar:string;
begin
dm1.QUmum.Close;
dm1.QUmum.SQL.Text:='select * from printing where id=0';
dm1.QUmum.open;
passkar:=dm1.QUmum['nama'];
dm1.QUmum.Close;
fpasswords.Edit1.Text:='';
Fpasswords.Position:=poScreenCenter;

if EnableEditMenu1.Checked then
   begin
     EnableEditMenu1.Checked:=false;
     report1.Enabled:=false;
     edit21.Enabled:=false;
   end
else
  begin
     Fpasswords.ShowModal;
     if not(passkar=Fpasswords.Edit1.Text)then exit;
     EnableEditMenu1.Checked:=true;
     edit21.Enabled:=true;
     report1.Enabled:=true;
  end;

end;

procedure TFmain.arifWifi1Click(Sender: TObject);
var Fwifi:TFWifi;
begin
   Fwifi:=TFWifi.Create(self);
   fwifi.ShowModal;
end;

procedure TFmain.RadioGroup1Click(Sender: TObject);
begin
if (edsearch.Text='')and not(radiogroup1.ItemIndex=3) then exit;
dm1.QMember.Close;

case radiogroup1.ItemIndex of
0 : begin
      if StrANumber(edSearch.Text) then
        dm1.QMember.SQL.Text:='select * from member where id like '+edSearch.Text
      else
        begin
        dm1.QMember.SQL.Text:='select * from member';
        showmessage('Masukkan angka, bukan huruf');
        end;
    end;

1 : dm1.QMember.SQL.Text:='select * from member where nama like '+QuotedStr('%'+edSearch.Text+'%');

2 : dm1.QMember.SQL.Text:='select * from member where mac like '+QuotedStr('%'+edSearch.Text+'%');

3 : begin
    edSearch.Text:='';
    RadioGroup1.ItemIndex:=1;
    dm1.QMember.SQL.Text:='select * from member';

    end;

end;


dm1.QMember.Open;
    dm1.TWifiTimer.Close;
    dm1.TWifiTimer.open; 
end;

function StrANumber(const S: string): Boolean;
var
  P: PChar;
begin
  P      := PChar(S);
  Result := False;
  while P^ <> #0 do
  begin
    if not (P^ in ['0'..'9']) then Exit;
    Inc(P);
  end;
  Result := True;
end;
procedure TFmain.StartRent1Click(Sender: TObject);
begin
//showmessage('Jangan lupa pilih add new pada browser, dan paste mac address');
Clipboard.AsText:=dm1.QMember['mac'];
dm1.QUmum.Close;
dm1.QUmum.SQL.Text:='insert into wifitimer (id,mulai,tipe) values (:id,:mulai,:tipe)';
dm1.QUmum.Parameters[0].Value:=dm1.QMember['idanggota'];
dm1.QUmum.Parameters[1].Value:=time;
if dm1.QMember['member']='y' then
  dm1.QUmum.Parameters[2].Value:=40
else
  dm1.QUmum.Parameters[2].Value:=41;
dm1.QUmum.ExecSQL;

dm1.TWifiTimer.Close;
dm1.TWifiTimer.Open;

end;

procedure TFmain.CopyMAC1Click(Sender: TObject);
begin
Clipboard.AsText:=dm1.QMember['mac'];
end;

procedure TFmain.SelesaiSewa1Click(Sender: TObject);
var 
jumlahJam:integer;
jumlahBayar:double;
begin
    jumlahJam:= (dm1.TWifiTimerlama.Value div 30 )+1 ;
    //if jumlahjam=48 then jumlahjam:=1 else


     //showmessage(inttostr(jumlahjam));
dm1.QUmum.Close;
dm1.QUmum.SQL.Text:='select * from tarifwifi where jam= :jam2';

dm1.QUmum.Parameters.ParamByName('jam2').Value:=jumlahjam;
dm1.QUmum.Open;

    if dm1.QUmum['jam']=null then
    begin
    dm1.QUmum.Close;
    dm1.QUmum.SQL.Text:='select * from tarifwifi where jam= :jam2';
    dm1.QUmum.Parameters.ParamByName('jam2').Value:=1;
    dm1.QUmum.Open;
    end;

if dm1.TWifiTimer['tipe']=40 then
    jumlahBayar:=dm1.QUmum['anggota']
else
    jumlahBayar:=dm1.QUmum['reguler'];
dm1.QUmum.Close;

simpansewa(dm1.TwifiTimer['idanggota'],NIPoperator,dm1.Twifitimer['tipe']
,timetostr(dm1.TWifiTimer['mulai']),timetostr(time),floattostr(jumlahBayar));

showmessage('Jumlah sewa dibayar : Rp'+floattostr(jumlahBayar)+#13+#13+'NB: Jangan lupa menghapus data client di browser ya :)'+#13);


dm1.QUmum.SQL.Text:='delete from wifitimer where id=:id';
dm1.QUmum.Parameters[0].Value:=dm1.TWifiTimer['id'];

dm1.QUmum.ExecSQL;
dm1.QUmum.Close;
dm1.TWifiTimer.Close;
dm1.TWifiTimer.Open;
updateIncome;
end;

procedure TFmain.CopyNama1Click(Sender: TObject);
begin
Clipboard.AsText:=dm1.QMember['nama'];
end;

procedure TFmain.Batal1Click(Sender: TObject);
begin
dm1.QUmum.SQL.Text:='delete from wifitimer where id=:id';
dm1.QUmum.Parameters[0].Value:=dm1.TWifiTimer['id'];

dm1.QUmum.ExecSQL;
dm1.QUmum.Close;
dm1.TWifiTimer.Close;
dm1.TWifiTimer.Open;
updateIncome;
end;

procedure TFmain.BTregisterClick(Sender: TObject);
var FRegister:TFRegister;
begin
    FRegister:=TFRegister.Create(self);
    FRegister.ShowModal;
end;

procedure TFmain.WifiMember1Click(Sender: TObject);
var fmember:TFmember;
begin
    fmember:=TFmember.Create(self);
    fmember.ShowModal;
end;

procedure TFmain.GFrontDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);

begin


if dm1.QFront['no'] mod 2 =0 then
         GFront.Canvas.Brush.Color:=clSkyBlue
else
         GFront.Canvas.Brush.Color:=clMedGray;

  GFront.DefaultDrawColumnCell (Rect, DataCol, Column, State);
end;

procedure TFmain.Update1Click(Sender: TObject);
begin
DM1.TWifiTimer.post;
end;

procedure TFmain.Timer3Timer(Sender: TObject);
begin
 dm1.TWifiTimer.Close;
dm1.TWifiTimer.Open;
end;

end.
