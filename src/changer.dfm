object FChanger: TFChanger
  Left = 192
  Top = 73
  Width = 387
  Height = 482
  Caption = 'Password & Tariff Form'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 32
    Top = 24
    Width = 78
    Height = 13
    Caption = 'Password Admin'
  end
  object Label2: TLabel
    Left = 32
    Top = 56
    Width = 87
    Height = 13
    Caption = 'Password Member'
  end
  object Label3: TLabel
    Left = 32
    Top = 88
    Width = 44
    Height = 13
    Caption = 'IP Server'
  end
  object Label4: TLabel
    Left = 32
    Top = 120
    Width = 53
    Height = 13
    Caption = 'Port Server'
  end
  object Label5: TLabel
    Left = 32
    Top = 152
    Width = 35
    Height = 13
    Caption = 'Interval'
  end
  object Label6: TLabel
    Left = 32
    Top = 184
    Width = 57
    Height = 13
    Caption = 'Tame Mode'
  end
  object Label7: TLabel
    Left = 32
    Top = 216
    Width = 57
    Height = 13
    Caption = 'Rent Before'
  end
  object Label8: TLabel
    Left = 32
    Top = 248
    Width = 44
    Height = 13
    Caption = 'Chat Port'
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 328
    Width = 379
    Height = 120
    TabStop = False
    Align = alBottom
    DataSource = DataSource1
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object edadmin: TEdit
    Left = 144
    Top = 24
    Width = 185
    Height = 21
    PasswordChar = '*'
    TabOrder = 1
    Text = 'edadmin'
  end
  object edMember: TEdit
    Left = 144
    Top = 56
    Width = 185
    Height = 21
    PasswordChar = '*'
    TabOrder = 2
    Text = 'Edit1'
  end
  object Button1: TButton
    Left = 192
    Top = 275
    Width = 139
    Height = 25
    Caption = 'Save and Exit'
    TabOrder = 10
    OnClick = Button1Click
  end
  object edIPserver: TEdit
    Left = 144
    Top = 88
    Width = 185
    Height = 21
    TabOrder = 3
    Text = 'edIPserver'
  end
  object edPortServer: TEdit
    Left = 144
    Top = 120
    Width = 185
    Height = 21
    TabOrder = 4
    Text = 'Edit3'
  end
  object Edinterval: TEdit
    Left = 144
    Top = 152
    Width = 185
    Height = 21
    TabOrder = 5
    Text = 'Edit3'
  end
  object edRent: TEdit
    Left = 144
    Top = 216
    Width = 185
    Height = 21
    TabOrder = 8
    Text = 'Edit3'
  end
  object RBT: TRadioButton
    Left = 144
    Top = 184
    Width = 81
    Height = 17
    Caption = 'true'
    TabOrder = 6
  end
  object RBF: TRadioButton
    Left = 240
    Top = 184
    Width = 89
    Height = 17
    Caption = 'false'
    Checked = True
    TabOrder = 7
    TabStop = True
  end
  object EdChat: TEdit
    Left = 144
    Top = 248
    Width = 185
    Height = 21
    TabOrder = 9
    Text = 'Edit3'
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=db.mdb;Persist Secu' +
      'rity Info=False'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 40
    Top = 336
  end
  object ADOQuery1: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from tarif')
    Left = 72
    Top = 336
  end
  object DataSource1: TDataSource
    DataSet = ADOQuery1
    Left = 104
    Top = 336
  end
  object XPManifest1: TXPManifest
    Left = 352
    Top = 168
  end
end
