unit reecordsender;

interface

uses
  Windows, inifiles, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, IdTCPConnection, IdTCPClient, IdBaseComponent,
  IdComponent, IdTCPServer, ExtCtrls, StdCtrls;

type TRECpemasukan = record
tanggal,mulai,selesai:TDateTime;
station:string[15];
id:byte;

end;

type
  TFSender = class(TForm)
    ADOConnection1: TADOConnection;
    IdTCPClient1: TIdTCPClient;
    Timer1: TTimer;
    ADOTable1: TADOTable;
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public

  defaultIP :string[15];
  interval,timewindows:integer;
  isconnected:boolean;
  procedure connectAdo;
  procedure disconnectAdo;
  procedure OpenRecord;
  procedure sendRecord(rec:TRECpemasukan);
  procedure AppException(Sender: TObject; E: Exception);
    { Public declarations }
  end;

var
  FSender: TFSender;

implementation

{$R *.dfm}
procedure TFSender.AppException(Sender: TObject; E: Exception);
begin

end;

procedure TFSender.sendRecord(rec:TRECpemasukan);
begin
if not isconnected then exit;
    IdTCPClient1.WriteBuffer(rec,sizeof(rec),true);
 


end;

procedure TfSender.OpenRecord;
var tempRec:TRECpemasukan;
begin

if not isconnected then exit;
connectAdo;
ADOTable1.Close;

ADOTable1.Open;
if ADOTable1['tanggal']=null then
   begin
      ADOTable1.Close;
      disconnectAdo;
       exit;
   end;

try
   IdTCPClient1.Host:=defaultIP;
   IdTCPClient1.Port:=115;
   IdTCPClient1.Connect;






while not (ADOTable1.Eof) do
  begin

      temprec.tanggal:=ADOTable1['tanggal'];
      temprec.mulai:=ADOTable1['mulai'];
      temprec.selesai :=ADOTable1['selesai'];
      temprec.station :=ADOTable1['station'];
      temprec.id:=ADOTable1['id'];
      //memo1.Lines.Add(temprec.station+inttostr(temprec.id));
      sendRecord(temprec);
      ADOTable1.Delete;
  end;

  ADOTable1.Close;
  disconnectAdo;
  isconnected:=false;

     IdTCPClient1.Disconnect;
  except
    on
     E: Exception
     do begin
     isconnected:=false;
        //memo1.Lines.Add('gagal kirim');
     end;
  end;
end;


procedure TFSender.disconnectAdo;
begin
 ADOConnection1.Connected:=false;
end;

procedure TFSender.connectAdo;
begin
 ADOConnection1.Connected:=false;
 ADOConnection1.ConnectionString:='Provider=Microsoft.Jet.OLEDB.4.0;'+'Data Source='+ExtractFilePath(Application.ExeName)+'\db.mdb;'+'Persist Security Info=False';


ADOConnection1.Connected:=true;
end;
procedure TFSender.FormCreate(Sender: TObject);
var 
IP,intervaltimer:string[15];
fileIni:TIniFIle;
F:TextFile;
begin
assignFile(f,ChangeFileExt(application.ExeName,'.txt'));
rewrite(f);
write(f,'asdfasfd');
closeFile(f);

Application.OnException:= AppException; //buang semua error message


timewindows:=0;
isconnected:=false;

  FileIni:=TIniFile.Create(ChangeFileExt(Application.ExeName,'.ini'));
   try
     defaultIP:=FileIni.ReadString('Sender','IP','');
     interval := fileIni.ReadInteger('Sender','Interval',0);
   finally
   fileIni.Free;
   end;

  if (defaultIp='')or (Interval=0) then
    begin

    IP:=InputBox('First Time Client Configuration','Masukkan Alamat IP Server','');
    intervaltimer:=InputBox('First Time Client Configuration','Masukkan Interval Cek Otomatis Server (detik)','10');

    if not (IP='') then
        defaultIP:=IP
    else  defaultIP:='192.168.1.2';
    if not (intervaltimer='') then
        interval:=strtoint(intervaltimer)
    else interval:=10;

   FileIni:=TIniFile.Create(ChangeFileExt(Application.ExeName,'.ini'));
   try
     FileIni.WriteString('Sender','IP',defaultIP);
     fileIni.WriteInteger('Sender','Interval',interval);
   finally
   fileIni.Free;
   end;
  end;//end if



end;

procedure TFSender.Timer1Timer(Sender: TObject);
begin
if not FileExists(ChangeFileExt(application.ExeName,'.txt')) then
    application.Terminate;

inc( timewindows);
 if FSender.Visible then FSender.Hide;
if timewindows mod (interval*2)=0 then
  begin
  OpenRecord;
  isconnected:=true;
 // memo1.Lines.Add(inttostr(timewindows));
  timewindows:=0;
  end;
end;

procedure TFSender.FormShow(Sender: TObject);
begin
ShowWindow(Application.Handle, SW_HIDE);
ShowWindow(FindWindow(nil, @Application.Title[1]), SW_HIDE);
end;

end.
