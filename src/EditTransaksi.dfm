object Fedit: TFedit
  Left = 333
  Top = 197
  Width = 579
  Height = 406
  BorderIcons = [biSystemMenu]
  Caption = 'Edit Transaksi'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 571
    Height = 372
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Pemasukan'
      OnShow = TabSheet1Show
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 563
        Height = 264
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object DBNavigator1: TDBNavigator
          Left = 0
          Top = 0
          Width = 563
          Height = 41
          DataSource = DM1.DsPemasukan
          Align = alTop
          TabOrder = 0
        end
        object GMasuk: TDBGrid
          Left = 0
          Top = 41
          Width = 563
          Height = 223
          TabStop = False
          Align = alClient
          DataSource = DM1.DsPemasukan
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDrawColumnCell = GMasukDrawColumnCell
          Columns = <
            item
              Expanded = False
              FieldName = 'Tanggal'
              Width = 168
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Station'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Jenis'
              Width = 61
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'mulai'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'selesai'
              Width = 46
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nilai'
              Width = 49
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Operator'
              Width = 81
              Visible = True
            end>
        end
      end
      object Panel4: TPanel
        Left = 0
        Top = 264
        Width = 563
        Height = 80
        Align = alBottom
        TabOrder = 1
        object Label1: TLabel
          Left = 8
          Top = 16
          Width = 69
          Height = 13
          Caption = 'Filter menurut :'
        end
        object Label5: TLabel
          Left = 505
          Top = 16
          Width = 32
          Height = 13
          Alignment = taRightJustify
          Caption = 'Label5'
          OnClick = Label5Click
        end
        object Label6: TLabel
          Left = 296
          Top = 16
          Width = 96
          Height = 13
          Caption = 'Total Nilai selected :'
        end
        object ComboBox1: TComboBox
          Left = 81
          Top = 12
          Width = 152
          Height = 21
          ItemHeight = 13
          TabOrder = 0
          Text = 'ComboBox1'
          Visible = False
        end
        object ComboBox2: TComboBox
          Left = 84
          Top = 12
          Width = 145
          Height = 21
          ItemHeight = 13
          TabOrder = 1
          Text = 'ComboBox2'
          OnChange = ComboBox2Change
        end
        object Button1: TButton
          Left = 80
          Top = 39
          Width = 113
          Height = 25
          Caption = 'Tampilkan Semua'
          TabOrder = 2
          OnClick = TampilkanSemua
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Pengeluaran'
      ImageIndex = 1
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 563
        Height = 303
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object DBNavigator2: TDBNavigator
          Left = 0
          Top = 0
          Width = 563
          Height = 41
          DataSource = DM1.DsPengeluaran
          Align = alTop
          TabOrder = 0
        end
        object Gkeluar: TDBGrid
          Left = 0
          Top = 41
          Width = 563
          Height = 262
          TabStop = False
          Align = alClient
          DataSource = DM1.DsPengeluaran
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'ID'
              Title.Caption = 'No'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'tanggal'
              Title.Caption = 'Tanggal'
              Width = 153
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Jam'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Keterangan'
              Title.Caption = 'Keterangan Pengeluaran'
              Width = 149
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Jumlah'
              Width = 73
              Visible = True
            end>
        end
      end
      object Panel5: TPanel
        Left = 0
        Top = 303
        Width = 563
        Height = 41
        Align = alBottom
        TabOrder = 1
        object Label2: TLabel
          Left = 8
          Top = 14
          Width = 109
          Height = 13
          Caption = 'Keyword pengeluaran :'
        end
        object Button2: TButton
          Left = 304
          Top = 8
          Width = 113
          Height = 25
          Caption = 'Cari'
          TabOrder = 1
          OnClick = Button2Click
        end
        object Edit1: TEdit
          Left = 128
          Top = 8
          Width = 161
          Height = 21
          TabOrder = 0
          OnKeyPress = Edit1KeyPress
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Jam Kerja'
      ImageIndex = 2
      OnShow = TabSheet3Show
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 563
        Height = 298
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Gjamkerja: TDBGrid
          Left = 0
          Top = 41
          Width = 563
          Height = 257
          TabStop = False
          Align = alClient
          DataSource = DM1.DsJamKerja
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'NIP'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nama'
              Width = 85
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Tanggal'
              Width = 199
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Mulai'
              Width = 43
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Selesai'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Fee'
              Width = 53
              Visible = True
            end>
        end
        object DBNavigator3: TDBNavigator
          Left = 0
          Top = 0
          Width = 563
          Height = 41
          DataSource = DM1.DsJamKerja
          Align = alTop
          TabOrder = 1
        end
      end
      object Panel6: TPanel
        Left = 0
        Top = 298
        Width = 563
        Height = 46
        Align = alBottom
        TabOrder = 1
        object Label3: TLabel
          Left = 16
          Top = 16
          Width = 85
          Height = 13
          Caption = 'Filter Menurut NIP'
        end
        object Label4: TLabel
          Left = 280
          Top = 16
          Width = 32
          Height = 13
          Caption = 'Label4'
        end
        object ComboBox3: TComboBox
          Left = 152
          Top = 12
          Width = 81
          Height = 21
          ItemHeight = 13
          TabOrder = 0
          Text = 'ComboBox3'
        end
        object Button3: TButton
          Left = 408
          Top = 11
          Width = 105
          Height = 25
          Caption = 'Tampilkan Semua'
          TabOrder = 1
          OnClick = Button3Click
        end
        object ComboBox4: TComboBox
          Left = 119
          Top = 12
          Width = 146
          Height = 21
          ItemHeight = 13
          TabOrder = 2
          Text = 'ComboBox4'
          OnChange = ComboBox4Change
        end
      end
    end
  end
end
