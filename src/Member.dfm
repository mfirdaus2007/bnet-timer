object FMember: TFMember
  Left = 470
  Top = 268
  BorderStyle = bsDialog
  Caption = 'FMember'
  ClientHeight = 266
  ClientWidth = 518
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object DBNavigator1: TDBNavigator
    Left = 0
    Top = 0
    Width = 518
    Height = 41
    DataSource = DM1.DSMember
    Align = alTop
    TabOrder = 0
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 41
    Width = 518
    Height = 225
    Align = alClient
    DataSource = DM1.DSMember
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'ID'
        Width = 56
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Nama'
        Width = 99
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'MAC'
        Width = 111
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Member'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'IDAnggota'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'stamp'
        Visible = True
      end>
  end
end
