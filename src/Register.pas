unit Register;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TFRegister = class(TForm)
    Label1: TLabel;
    Label3: TLabel;
    EdNama: TEdit;
    EdMac: TEdit;
    Btsimpan: TButton;
    BtBatal: TButton;
    Label2: TLabel;
    RBYa: TRadioButton;
    RBTidak: TRadioButton;
    procedure EdNamaKeyPress(Sender: TObject; var Key: Char);
    procedure EdMacKeyPress(Sender: TObject; var Key: Char);
    procedure BtsimpanClick(Sender: TObject);
    procedure RBYaKeyPress(Sender: TObject; var Key: Char);
    procedure BtBatalClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FRegister: TFRegister;

implementation

uses DMserver;

{$R *.dfm}

procedure TFRegister.EdNamaKeyPress(Sender: TObject; var Key: Char);
begin
if key=#13 then
    edMac.SetFocus;
end;

procedure TFRegister.EdMacKeyPress(Sender: TObject; var Key: Char);
begin
if key=#13 then
    rbya.SetFocus;
end;

procedure TFRegister.BtsimpanClick(Sender: TObject);
begin
dm1.QUmum.Close;
dm1.QUmum.sql.Text:='insert into member (nama,mac,member,stamp) values(:nama,:mac,:member,:stamp)';
dm1.QUmum.Parameters[0].Value:=edNama.Text;
dm1.QUmum.Parameters[1].Value:=edMAc.Text;
if rbya.Checked then
    dm1.QUmum.Parameters[2].Value:='y'
else
    dm1.QUmum.Parameters[2].Value:='t';
dm1.QUmum.Parameters[3].Value:=now;    
dm1.QUmum.execSql;
dm1.QUmum.Close;
showmessage('data sukses diinputkan');
close;
dm1.QMember.Close;
dm1.QMember.Open;
end;

procedure TFRegister.RBYaKeyPress(Sender: TObject; var Key: Char);
begin
if key=#13 then
    BtSimpan.SetFocus;
end;

procedure TFRegister.BtBatalClick(Sender: TObject);
begin
close;
end;

procedure TFRegister.FormClose(Sender: TObject; var Action: TCloseAction);
begin
action:=cafree;
end;

end.
