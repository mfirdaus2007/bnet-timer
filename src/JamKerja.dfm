object FJamKerja: TFJamKerja
  Left = 351
  Top = 230
  Width = 502
  Height = 356
  BorderIcons = [biSystemMenu]
  Caption = 'Jam Kerja'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 16
    Top = 69
    Width = 18
    Height = 13
    Caption = 'NIP'
  end
  object Label1: TLabel
    Left = 16
    Top = 51
    Width = 47
    Height = 13
    Caption = 'Operator :'
  end
  object Label3: TLabel
    Left = 16
    Top = 88
    Width = 28
    Height = 13
    Caption = 'Nama'
  end
  object Label4: TLabel
    Left = 82
    Top = 69
    Width = 18
    Height = 13
    Caption = 'NIP'
  end
  object Label5: TLabel
    Left = 82
    Top = 88
    Width = 18
    Height = 13
    Caption = 'NIP'
  end
  object Label6: TLabel
    Left = 16
    Top = 109
    Width = 11
    Height = 13
    Caption = 'ID'
  end
  object Label7: TLabel
    Left = 82
    Top = 109
    Width = 18
    Height = 13
    Caption = 'NIP'
  end
  object Label8: TLabel
    Left = 16
    Top = 131
    Width = 25
    Height = 13
    Caption = 'Mulai'
  end
  object Label9: TLabel
    Left = 82
    Top = 131
    Width = 18
    Height = 13
    Caption = 'NIP'
  end
  object ComboBox1: TComboBox
    Left = 16
    Top = 25
    Width = 217
    Height = 21
    ItemHeight = 13
    TabOrder = 0
    OnChange = ComboBox1Change
  end
  object Button1: TButton
    Left = 248
    Top = 23
    Width = 105
    Height = 25
    Caption = 'Start'
    TabOrder = 1
    OnClick = Button1Click
  end
  object DBGrid1: TDBGrid
    Left = 16
    Top = 160
    Width = 457
    Height = 145
    DataSource = DM1.DsJamKerja
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'ID'
        Width = 38
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NIP'
        Width = 48
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Nama'
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Tanggal'
        Width = 137
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Mulai'
        Width = 46
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Selesai'
        Width = 50
        Visible = True
      end>
  end
end
