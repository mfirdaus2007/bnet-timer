unit Operator;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, Grids, DBGrids, ExtCtrls, DBCtrls;

type
  TFOperator = class(TForm)
    DBNavigator1: TDBNavigator;
    DBGrid1: TDBGrid;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FOperator: TFOperator;

implementation

uses mainform, DMserver;

{$R *.dfm}

procedure TFOperator.FormClose(Sender: TObject; var Action: TCloseAction);
begin
action:=cafree;
end;

procedure TFOperator.FormCreate(Sender: TObject);
begin
dm1.TOperator.Close;
dm1.TOperator.Open;
end;

end.
