unit ReportHarian;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, StdCtrls, ExtCtrls, Grids, DBGrids, ComCtrls;

type
  TFReportharian = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel1: TPanel;
    Button2: TButton;
    Button1: TButton;
    Button3: TButton;
    Panel2: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    Label1: TLabel;
    Panel4: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    DBGrid2: TDBGrid;
    DBGrid1: TDBGrid;
    TabSheet3: TTabSheet;
    DBGrid3: TDBGrid;
    Panel3: TPanel;
    Label6: TLabel;
    Label7: TLabel;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    bulan,tahun:word;
    nama_bulan:array [1..12]of string;
    { Private declarations }
  public
   procedure loaddata;

    { Public declarations }
  end;

var
  FReportharian: TFReportharian;

implementation

uses  MainForm, DMserver;

{$R *.dfm}


procedure TFReportharian.loaddata;
var
total : double;
tahun2,bulan2:word;
bulandepan,bulanini:Tdate;
begin




bulan2:=bulan+1;
tahun2:=tahun;

if bulan=12 then
   begin
    bulan2:=1;
    tahun2:=tahun+1;
   end
else
if bulan=13 then
   begin
    bulan:=1;
    bulan2:=bulan+1;
    tahun:=tahun+1;
    tahun2:=tahun;
   end
else
if bulan=0 then
   begin
    bulan:=12;
    bulan2:=1;
    tahun:=tahun-1;
   end;

   bulandepan:= encodedate(tahun2,bulan2,1);

   bulanini:= EncodeDate(tahun,bulan,1);

   dm1.QharianMasuk.Close;
   dm1.QharianMasuk.Parameters[0].Value:=(bulandepan);
   dm1.QharianMasuk.Parameters[1].Value:=(bulanini);
   dm1.QharianMasuk.Open;

   dm1.QharianKeluar.Close;
   dm1.QharianKeluar.Parameters[0].Value:=(bulandepan);
   dm1.QharianKeluar.Parameters[1].Value:=(bulanini);
   dm1.QharianKeluar.Open;

   dm1.QgajiOP.Close;
   dm1.QgajiOP.Parameters[0].Value:=(bulandepan);
   dm1.QgajiOP.Parameters[1].Value:=(bulanini);
   dm1.QgajiOP.Open;


  label1.Caption:=nama_bulan[bulan] + ' ' + inttostr(tahun);
  total:=0;
  dm1.QharianMasuk.DisableControls;
  while not (dm1.QharianMasuk.Eof) do
   begin
      total:=total+dm1.QharianMasuk['income'];
      dm1.QharianMasuk.Next;
   end;
     dm1.QharianMasuk.First;
  dm1.QharianMasuk.EnableControls;
  label3.Caption:=format ('%8.2m',[floattocurr(total)]);

  total:=0;
  dm1.QharianKeluar.DisableControls;
  while not (dm1.QharianKeluar.Eof) do
   begin
      total:=total+dm1.QharianKeluar['Outcome'];
      dm1.QharianKeluar.Next;
   end;
     dm1.QharianKeluar.First;
  dm1.QharianKeluar.EnableControls;
  label5.Caption:=format ('%8.2m',[floattocurr(total)]);

    total:=0;
  dm1.QgajiOP.DisableControls;
  while not (dm1.QgajiOP.Eof) do
   begin
      total:=total+dm1.QgajiOP['gaji'];
      dm1.QgajiOP.Next;
   end;
     dm1.QgajiOP.First;
  dm1.QgajiOP.EnableControls;
  label7.Caption:=format ('%8.2m',[floattocurr(total)]);


  label2.Caption:='Jumlah pendapatan bulan '+nama_bulan[bulan]+' : ';
  label4.Caption:='Jumlah pengeluaran bulan '+nama_bulan[bulan]+' : ';
  label6.Caption:='Jumlah fee operator bulan '+nama_bulan[bulan]+' : ';
end;

procedure TFReportharian.FormShow(Sender: TObject);
var tanggal:word;
begin
decodedate(date,tahun,bulan,tanggal);

loadData;




end;

procedure TFReportharian.FormClose(Sender: TObject; var Action: TCloseAction);
begin
Action:=cafree;
end;

procedure TFReportharian.Button1Click(Sender: TObject);
begin
dec (bulan);


  loaddata;

end;

procedure TFReportharian.Button2Click(Sender: TObject);
begin
inc (bulan);

      loaddata;
end;

procedure TFReportharian.Button3Click(Sender: TObject);
begin
FormShow(self);
loaddata;
end;

procedure TFReportharian.FormCreate(Sender: TObject);
begin
nama_bulan[1]:='Januari'           ;
nama_bulan[2]:='Februari'          ;
nama_bulan[3]:='Maret'         ;
nama_bulan[4]:='April'        ;
nama_bulan[5]:='Mei'       ;
nama_bulan[6]:='Juni'      ;
nama_bulan[7]:='Juli'     ;
nama_bulan[8]:='Agustus'    ;
nama_bulan[9]:='September'   ;
nama_bulan[11]:='November'  ;
nama_bulan[10]:='Oktober' ;
nama_bulan[12]:='Desember';
end;

end.
