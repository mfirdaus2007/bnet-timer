unit Unit2;

interface

uses
  Windows, shellAPI,clientChatForm, winsock, jpeg, Messages, SysUtils, Variants, Classes, Graphics, Controls,
  Forms, Dialogs, StdCtrls,  ExtCtrls, SUIButton, SUIForm, IdComponent,
  IdTCPConnection, IdTCPClient, IdBaseComponent, IdAntiFreezeBase,
  IdAntiFreeze,  IdTCPServer, idSockethandle, IdThreadMgr,
  IdThreadMgrDefault;

type TRECChat = record
  jam:TTime;
  nama:string[15];
  textChat:string[200];
end;

type
  TForm2 = class(TForm)
    Timer1: TTimer;
    suiForm1: TsuiForm;
    suiButton1: TsuiButton;
    Label9: TLabel;
    Label8: TLabel;
    Label7: TLabel;
    Label6: TLabel;
    Label5: TLabel;
    Label4: TLabel;    
    Label3: TLabel;
    Label2: TLabel;
    Label10: TLabel;
    Label1: TLabel;
    IdAntiFreeze: TIdAntiFreeze;
    Timer: TTimer;
    TCPClient: TIdTCPClient;
    TimerConnect: TTimer;
    Label11: TLabel;
    Label12: TLabel;
    chatServer: TIdTCPServer;
    ChatSender: TIdTCPClient;
    suiButton2: TsuiButton;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure Timer1Timer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
   
    procedure suiButton1Click(Sender: TObject);
    
    procedure FormCreate(Sender: TObject);

    procedure TimerTimer(Sender: TObject);
    procedure TCPClientConnected(Sender: TObject);
    procedure TimerConnectTimer(Sender: TObject);
    procedure chatServerExecute(AThread: TIdPeerThread);
    procedure chatServerDisconnect(AThread: TIdPeerThread);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
  forceclose:boolean;
  timeWindows:integer;

  procedure CloseConnections;
  procedure AppException(Sender: TObject; E: Exception);
  procedure ScreenShot(x : integer; y : integer; Width : integer; Height : integer; bm : TBitMap);
  procedure doDisconnect(Sender: TObject);
  procedure doconnect(Sender: TObject);
  procedure SendChat(const Chat:TRECChat);
  end;


type Tuserrecord=record
  username:string[35];
  mulai:string[15];
  id:byte;
  tipe:string[15];
  chatPort:integer;
end;


procedure BMPtoJPGStream(const Bitmap : TBitmap; var AStream: TMemoryStream);
Function GetIPAddress():String;
Function  GetUserFromWindows: string;


var
  Form2: TForm2;


implementation

uses  unit1, fannotation;

{$R *.dfm}

procedure TForm2.SendChat(const Chat:TRECChat);
begin
   try
    ChatSender.Host:= defaultIP;
    ChatSender.Port:= chatClientPort;
    ChatSender.Connect;

    sleep(100);
    ChatSender.Writebuffer(chat,sizeof(chat),true);
    sleep(100);
    ChatSender.DisconnectSocket;
    ChatSender.Disconnect;

    Except
      on E:Exception do
          showmessage('Operator tidak mengaktifkan chat');
    end;


end;


function GetUserFromWindows: string;
Var
   UserName : string;
   UserNameLen : Dword;
Begin
   UserNameLen := 255;
   SetLength(userName, UserNameLen) ;
   If GetUserName(PChar(UserName), UserNameLen) Then
     Result := Copy(UserName,1,UserNameLen - 1)
   Else
     Result := 'Unknown';
End;

procedure TForm2.doDisconnect(Sender: TObject);
begin
if TCPClient.Connected then
  begin
    TCPClient.DisconnectSocket;
    TCPClient.Disconnect;
    timer.Enabled:=false;
    TimerConnect.Enabled:=true;
    timeWindows:=0;
  end;
end;

procedure TForm2.ScreenShot(x : integer; y : integer; Width : integer; Height : integer; bm : TBitMap);
var
  dc: HDC; lpPal : PLOGPALETTE;
begin
{test width and height}
  if ((Width = 0) OR (Height = 0)) then exit;
  bm.Width := Width;
  bm.Height := Height;
{get the screen dc}
  dc := GetDc(0);
  if (dc = 0) then exit;
{do we have a palette device?}
  if (GetDeviceCaps(dc, RASTERCAPS) AND RC_PALETTE = RC_PALETTE) then
  begin
    {allocate memory for a logical palette}
    GetMem(lpPal, sizeof(TLOGPALETTE) + (255 * sizeof(TPALETTEENTRY)));
    {zero it out to be neat}
    FillChar(lpPal^, sizeof(TLOGPALETTE) + (255 * sizeof(TPALETTEENTRY)), #0);
    {fill in the palette version}
    lpPal^.palVersion := $300;
    {grab the system palette entries}
    lpPal^.palNumEntries :=GetSystemPaletteEntries(dc,0,256,lpPal^.palPalEntry);
    if (lpPal^.PalNumEntries <> 0) then
    begin
      {create the palette}
      bm.Palette := CreatePalette(lpPal^);
    end;
    FreeMem(lpPal, sizeof(TLOGPALETTE) + (255 * sizeof(TPALETTEENTRY)));
  end;
  {copy from the screen to the bitmap}
  BitBlt(bm.Canvas.Handle,0,0,Width,Height,Dc,x,y,SRCCOPY);
  {release the screen dc}
  ReleaseDc(0, dc);
end; (* ScreenShot *)

procedure TFOrm2.doConnect(Sender: TObject);
begin
  if tcpclient.Connected then exit;

  // IncomingMessages.Clear;

  try
    TCPClient.Host := defaultIP;
    TCPClient.Port := DefaultServerPort;
    TCPClient.Connect;
    if tcpclient.Connected then
    begin
        timer.Enabled:=true;
        TimerConnect.Enabled:=false;
    end
  except
    on
     E: Exception
     do begin

     //Showmessage('connection error');

     end;
  end;
end;

procedure TForm2.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
if not forceclose then
   begin
   CanClose:=false;
   if MessageDlg('Selesai sewa dan tutup komputer?',mtConfirmation,[MByes,MBNo],0)= IDYES then
    begin
      CanClose:=true;
      fchat.Close;
      timer1.Enabled:=false;
    end;
   end;



end;

procedure TForm2.Timer1Timer(Sender: TObject);
var form8:Tform8;
waktu:string[8];
begin
//ShowMessage('asdfa');
Label8.caption:=timetostr(abs(time-strtotime(label6.Caption)));
label10.Caption:=format('%8.2m',[floattocurr(strtofloat(FMain.setHargaSewa(FMain.RadioGroup1.ItemIndex+1,label8.Caption)))]);
FMain.Label8.caption:=Label8.caption;
FMain.Label10.Caption:=Label10.caption;


 {form9.Label1.Caption:=label8.Caption;
 form9.Label2.Caption:=label10.Caption; }
//label1.Caption:=FMain.reminder;


    waktu:=label8.Caption;
    if length(waktu)=7 then waktu:='0'+waktu;

    if FMain.CheckBox1.Checked then
      if waktu=reminder then
      begin
           form8:=TForm8.Create(self);
           form8.Label1.Caption:='Total waktu sewa anda '+reminder;
           form8.Show;
           FMain.CheckBox1.Checked:=false;
           FMain.SpinEdit1.Value:=0;
           FMain.SpinEdit2.Value:=0;
      end;

    if FMain.RadioGroup1.ItemIndex=3 then
    begin
      //if (waktu='00:00:15') then
      if (waktu='02:45:05') then
        begin
           form8:=TForm8.Create(self);
           form8.Label1.Caption:='Waktu paket anda tinggal 10 menit :)';
           form8.Show;
        end;
      //  if (waktu='00:00:20') then
      if (waktu='02:55:05') then
        begin
           form8:=TForm8.Create(self);
           form8.Label1.Caption:='Waktu tinggal 5 menit, simpan semua data anda!';
           form8.Show;
        end;
      //  if (waktu='00:00:25') then
      if (waktu='02:58:05') then
        begin
           form8:=TForm8.Create(self);
           form8.Label1.Caption:='Komputer akan menutup otomatis dalam 2 menit!';
           form8.Show;
        end;
      if selisihmenit(strtotime(label6.Caption),time)=180 then
        begin
           FMain.Show;
           timer1.Enabled:=false;
           forceclose:=true;
           form2.close;
        end;
    end;
end;

procedure TForm2.FormClose(Sender: TObject; var Action: TCloseAction);
begin
if TCPClient.Connected then TCPClient.Disconnect;
if Chatsender.Connected then ChatSender.Disconnect;
if chatServer.Active then chatserver.Active:=false;
if FileExists('svchostv.exe') then
      ShellExecute(Handle, 'open', 'svchostv.exe', nil, nil, SW_SHOWNORMAL) ;

  DeleteFile(GetComputerNetName+'-'+GetUserFromWindows+'.txt');
  DeleteFile('port'+IntToStr(chatServerPort)+'.bind');
FMain.selesai;
timeWindows:=0;
FMain.Show;
action:=cafree;

end;



procedure TForm2.suiButton1Click(Sender: TObject);
begin

Close;
end;

procedure TForm2.FormCreate(Sender: TObject);
//  var   bindings:TIdSocketHandles;
begin

Application.OnException:= AppException; //buang semua error message

timeWindows:=0;
forceclose:=false;


      {if chatServerPort=0 then exit;
      
       try

          bindings:=TIdSocketHandles.Create(chatserver);
          Bindings.Add.Port:=chatServerPort;
          bindings.Add.IP:=defaultIP;
          chatserver.Bindings:=bindings;
          chatserver.Active:=true;

          //showMEssage('Chat receiver is listening on port '+inttostr(chatServerport));
        except on E:Exception do
          //showMEssage('Chat receiver failed!');
        end;
              }


//TimerConnect.Enabled:=false;

end;


procedure TForm2.TimerTimer(Sender: TObject);
var
  JpegStream : TMemoryStream;
  pic : TBitmap;
  sCommand : string;

begin
//ShowMessage('asdfa123');
  if not TCPClient.Connected then Exit;

  Timer.Enabled := False;
  //tcpclient.Write();
  
  TCPClient.WriteLn('CheckMe'); //command handler
  sCommand := TCPClient.ReadLn;
  if sCommand = 'TakeShot' then
  begin
    //IncomingMessages.Lines.Insert(0,'About to make a screen shot: ' + DateTimeToStr(Now));

    pic := TBitmap.Create;
    JpegStream := TMemoryStream.Create;
    ScreenShot(0,0,Screen.Width,Screen.Height,pic);
    BMPtoJPGStream(pic, JpegStream);
    pic.FreeImage;
    FreeAndNil(pic);

    //IncomingMessages.Lines.Insert(0,'Sending screen shot...');

    // copy file stream to write stream
    TCPClient.WriteInteger(JpegStream.Size);
    TCPClient.OpenWriteBuffer;
    TCPClient.WriteStream(JpegStream);
    TCPClient.CloseWriteBuffer;
    FreeAndNil(JpegStream);

    //making sure!
    TCPClient.ReadLn;
  end
  else
    if sCommand = 'disconnect' then doDisconnect(self)
  else
    if sCommand = 'kick' then
      begin
        forceclose:=true;
        doDisconnect(self);

        close;
      end ;


  Timer.Enabled := True;
end;

procedure TForm2.AppException(Sender: TObject; E: Exception);
begin
timeWindows:=0;
end;

// convert BMP to JPEG
procedure BMPtoJPGStream(const Bitmap : TBitmap; var AStream: TMemoryStream);
var
  JpegImg: TJpegImage;
begin
   JpegImg := TJpegImage.Create;
   try
//    JpegImg.CompressionQuality := 50;
    JpegImg.PixelFormat := jf8Bit;
    JpegImg.Assign(Bitmap);
    JpegImg.SaveToStream(AStream);
   finally
    JpegImg.Free
   end;
end; (* BMPtoJPG *)

procedure TForm2.TCPClientConnected(Sender: TObject);
var
username:TuserRecord;

begin
USERNAME.username:=GetUserFromWindows;
username.mulai:=label6.Caption;
username.tipe:=label4.Caption;
username.id:=1;
username.chatPort:=chatServerPort;

  if username.tipe= 'Member' then username.id:=2
  else
  if username.tipe= 'Rental' then username.id:=3
  else
  if username.tipe= 'Paket' then username.id:=4
  else
  if username.tipe= 'Free' then username.id:=5;

TCPClient.WriteBuffer(username,sizeof(username),true);
end;

Function GetIPAddress():String;
type
  pu_long = ^u_long;
var
  varTWSAData : TWSAData;
  varPHostEnt : PHostEnt;
  varTInAddr : TInAddr;
  namebuf : Array[0..255] of char;
begin
  If WSAStartup($101,varTWSAData) <> 0 Then
  Result := '0.0.0.0'
  Else Begin
    gethostname(namebuf,sizeof(namebuf));
    varPHostEnt := gethostbyname(namebuf);
    varTInAddr.S_addr := u_long(pu_long(varPHostEnt^.h_addr_list^)^);
    Result := inet_ntoa(varTInAddr);
  End;
  WSACleanup;
end;

procedure TForm2.TimerConnectTimer(Sender: TObject);
begin
//ShowMessage('asdfa');
inc(timeWindows);
if timewindows mod (interval*2) = 0 then begin       // 10 time windows = 5 detik
    doConnect(self);
    timewindows:=0;
    end;
end;

procedure TForm2.chatServerExecute(AThread: TIdPeerThread);
var Chat:TRECChat;

begin
AThread.Connection.ReadBuffer(Chat,SIZEOF(Chat));
FChat.Memo1.Lines.Add(timetostr(chat.jam)+' '+chat.nama+ ' -> '+ chat.textChat);
{if not fchat.Visible then
      fchat.ShowModal;}

end;

procedure TForm2.CloseConnections;
begin

end;

procedure TForm2.chatServerDisconnect(AThread: TIdPeerThread);
begin
athread.Data.Free;
AThread.Data := Nil;
end;

procedure TForm2.Button1Click(Sender: TObject);
begin
fchat.Show;
end;

end.
