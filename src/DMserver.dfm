object DM1: TDM1
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 809
  Top = 37
  Height = 552
  Width = 551
  object ADOConnection1: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security In' +
      'fo=False;Initial Catalog=warnet;Data Source=BISMILLAH;Use Proced' +
      'ure for Prepare=1;Auto Translate=True;Packet Size=4096;Workstati' +
      'on ID=MANAJEMEN;Use Encryption for Data=False;Tag with column co' +
      'llation when possible=False'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'SQLOLEDB.1'
    Left = 48
    Top = 24
  end
  object TJenis: TADOTable
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    TableName = 'Jenis'
    Left = 176
    Top = 25
  end
  object QUmum: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    Left = 280
    Top = 24
  end
  object DSFrontTable: TDataSource
    DataSet = QFront
    Left = 64
    Top = 153
  end
  object DSHarianKeluar: TDataSource
    DataSet = QHarianKeluar
    Left = 132
    Top = 173
  end
  object DSDetailKeluar: TDataSource
    DataSet = QDetailKeluar
    Left = 204
    Top = 152
  end
  object DSGajiOP: TDataSource
    DataSet = QGajiOP
    Left = 276
    Top = 176
  end
  object QTarif: TADOQuery
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from tarif order by ID asc')
    Left = 398
    Top = 347
  end
  object QFront: TADOQuery
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'tanggal'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = 40179d
      end>
    SQL.Strings = (
      
        'select * from pemasukan WHERE ((id = 1) or (id=2) or (id=3)or (i' +
        'd=4)or (id=4)or (id=5) or (id=40) or (id=41)) and (tanggal= :tan' +
        'ggal) order by no desc'
      '')
    Left = 64
    Top = 96
    object QFrontNo: TAutoIncField
      DisplayWidth = 8
      FieldName = 'No'
      ReadOnly = True
    end
    object QFrontStation: TStringField
      FieldName = 'Station'
      FixedChar = True
      Size = 15
    end
    object QFrontID: TSmallintField
      FieldName = 'ID'
    end
    object QFrontmulai: TDateTimeField
      DisplayWidth = 9
      FieldName = 'mulai'
      DisplayFormat = 'hh:mm'
    end
    object QFrontselesai: TDateTimeField
      DisplayWidth = 9
      FieldName = 'selesai'
      DisplayFormat = 'hh:mm'
    end
    object QFrontNilai: TBCDField
      DisplayWidth = 10
      FieldName = 'Nilai'
      DisplayFormat = '#,0'
      Precision = 19
    end
    object QFronttipe: TStringField
      DisplayLabel = 'Tipe'
      DisplayWidth = 15
      FieldKind = fkLookup
      FieldName = 'tipe'
      LookupDataSet = TJenis
      LookupKeyFields = 'ID'
      LookupResultField = 'Nama'
      KeyFields = 'ID'
      Size = 50
      Lookup = True
    end
  end
  object QHarianKeluar: TADOQuery
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'tanggal1'
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = 40179d
      end
      item
        Name = 'tanggal2'
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = 40211d
      end>
    SQL.Strings = (
      ''
      'select  Tanggal,sum(Jumlah) as Outcome from pengeluaran '
      'group by Tanggal having '
      '(Tanggal < :tanggal1) and '
      '(Tanggal >=  :tanggal2) order by '
      'Tanggal desc'
      ''
      '')
    Left = 128
    Top = 112
    object QHarianKeluarTanggal: TDateTimeField
      FieldName = 'Tanggal'
      DisplayFormat = 'dddd, dd mmmm yyyy'
    end
    object QHarianKeluarOutcome: TBCDField
      FieldName = 'Outcome'
      ReadOnly = True
      DisplayFormat = '#,0'
      currency = True
      Precision = 19
    end
  end
  object QDetailKeluar: TADOQuery
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'tanggal'
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select * from pengeluaran where tanggal = :tanggal')
    Left = 200
    Top = 96
    object QDetailKeluarID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QDetailKeluarJam: TDateTimeField
      FieldName = 'Jam'
      DisplayFormat = 'HH:MM'
    end
    object QDetailKeluarketerangan: TWideStringField
      FieldName = 'keterangan'
      Size = 40
    end
    object QDetailKeluarjumlah: TBCDField
      FieldName = 'jumlah'
      DisplayFormat = '#,0'
      Precision = 19
    end
  end
  object QGajiOP: TADOQuery
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'tanggal1'
        DataType = ftDateTime
        Size = 8
        Value = 40211d
      end
      item
        Name = 'tanggal2'
        DataType = ftDateTime
        Size = 8
        Value = 40179d
      end>
    SQL.Strings = (
      'SELECT jamkerja2.nip, sum(jamkerja2.fee) AS gaji FROM '
      #9'('
      #9'select * from jamkerja where (jamkerja.tanggal < :tanggal1) '
      #9#9'and ( jamkerja.tanggal >= :tanggal2)'
      #9')  AS jamkerja2 '
      'GROUP BY jamkerja2.nip'
      ''
      '')
    Left = 280
    Top = 112
    object QGajiOPnip: TWideStringField
      FieldName = 'nip'
      Size = 6
    end
    object QGajiOPgaji: TBCDField
      FieldName = 'gaji'
      DisplayFormat = '#,0'
      Precision = 19
    end
    object QGajiOPNama: TStringField
      FieldKind = fkLookup
      FieldName = 'Nama'
      LookupDataSet = TOperator
      LookupKeyFields = 'ID'
      LookupResultField = 'Nama'
      KeyFields = 'nip'
      Size = 30
      Lookup = True
    end
  end
  object QHarianMasuk: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'tanggal1'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = 40179d
      end
      item
        Name = 'tanggal2'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = 40179d
      end>
    SQL.Strings = (
      ''
      'select  Tanggal,sum(nilai) as Income from pemasukan '
      'group by Tanggal having '
      '(Tanggal < :tanggal1) and '
      '(Tanggal >=  :tanggal2) order by '
      'Tanggal desc'
      '')
    Left = 424
    Top = 112
    object QHarianMasukTanggal: TDateTimeField
      FieldName = 'Tanggal'
      DisplayFormat = 'DDDD, dd mmmm yyyy'
    end
    object QHarianMasukIncome: TBCDField
      FieldName = 'Income'
      ReadOnly = True
      DisplayFormat = '#,0'
      Precision = 19
    end
  end
  object QUpdateInc: TADOQuery
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'tanggal'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = 36892d
      end>
    SQL.Strings = (
      ''
      
        'select tanggal, sum(nilai) as income from Pemasukan group by tan' +
        'ggal having Tanggal = :tanggal'
      '')
    Left = 136
    Top = 232
  end
  object QSimpanSewa: TADOQuery
    Connection = ADOConnection1
    Parameters = <
      item
        Name = 'tanggal'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = 40179d
      end
      item
        Name = 'station'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = 'user3'
      end
      item
        Name = 'id'
        Attributes = [paSigned, paNullable]
        DataType = ftSmallint
        Precision = 5
        Size = 2
        Value = 1
      end
      item
        Name = 'mulai'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = 0.416666666666667d
      end
      item
        Name = 'selesai'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = 0.416666666666667d
      end
      item
        Name = 'nilai'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 19
        Size = 8
        Value = 1000000c
      end
      item
        Name = 'nip'
        Attributes = [paSigned, paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = '100100'
      end>
    SQL.Strings = (
      ''
      
        'insert into Pemasukan (Tanggal,Station,ID,mulai,selesai,nilai,ni' +
        'p) values '
      '(:tanggal,:station,:id,:mulai,:selesai,:nilai,:nip)'
      '')
    Left = 208
    Top = 232
  end
  object QSimpanKeluar: TADOQuery
    Connection = ADOConnection1
    Parameters = <
      item
        Name = 'tanggal'
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'jam'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'keterangan'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'jumlah'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 19
        Size = 8
        Value = Null
      end>
    SQL.Strings = (
      
        'insert into pengeluaran (tanggal,jam,keterangan,jumlah) values (' +
        ':tanggal,:jam,:keterangan,:jumlah)')
    Left = 288
    Top = 232
  end
  object DsPemasukan: TDataSource
    DataSet = QPemasukan
    Left = 72
    Top = 288
  end
  object QPemasukan: TADOQuery
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select pemasukan.* from pemasukan order by pemasukan.No desc')
    Left = 72
    Top = 336
    object QPemasukanNo: TAutoIncField
      FieldName = 'No'
      ReadOnly = True
    end
    object QPemasukanTanggal: TDateTimeField
      FieldName = 'Tanggal'
      DisplayFormat = 'DDDD, DD MMMM YYYY'
    end
    object QPemasukanStation: TStringField
      FieldName = 'Station'
      FixedChar = True
      Size = 15
    end
    object QPemasukanID: TSmallintField
      FieldName = 'ID'
    end
    object QPemasukanmulai: TDateTimeField
      FieldName = 'mulai'
      DisplayFormat = 'HH:MM'
    end
    object QPemasukanselesai: TDateTimeField
      FieldName = 'selesai'
      DisplayFormat = 'HH:MM'
    end
    object QPemasukanNilai: TBCDField
      FieldName = 'Nilai'
      DisplayFormat = '#,0'
      currency = True
      Precision = 19
    end
    object QPemasukanNIP: TStringField
      FieldName = 'NIP'
      FixedChar = True
      Size = 10
    end
    object QPemasukanPetugas: TStringField
      FieldKind = fkLookup
      FieldName = 'Operator'
      LookupDataSet = TOperator
      LookupKeyFields = 'ID'
      LookupResultField = 'Nama'
      KeyFields = 'NIP'
      Size = 40
      Lookup = True
    end
    object QPemasukanJenis: TStringField
      FieldKind = fkLookup
      FieldName = 'Jenis'
      LookupDataSet = TJenis
      LookupKeyFields = 'ID'
      LookupResultField = 'Nama'
      KeyFields = 'id'
      Lookup = True
    end
  end
  object DsPengeluaran: TDataSource
    DataSet = QPengeluaran
    Left = 152
    Top = 288
  end
  object QPengeluaran: TADOQuery
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    AfterCancel = TMemberCalcFields
    Parameters = <>
    SQL.Strings = (
      
        'select pengeluaran.* from pengeluaran order by PEngeluaran.ID de' +
        'sc')
    Left = 152
    Top = 336
    object QPengeluaranID: TAutoIncField
      DisplayWidth = 8
      FieldName = 'ID'
      ReadOnly = True
    end
    object QPengeluarantanggal: TDateTimeField
      DisplayWidth = 25
      FieldName = 'tanggal'
      DisplayFormat = 'DDDD, DD MMMM YY'
    end
    object QPengeluaranJam: TDateTimeField
      FieldName = 'Jam'
      DisplayFormat = 'HH:MM'
    end
    object QPengeluaranKeterangan: TWideStringField
      DisplayWidth = 39
      FieldName = 'Keterangan'
      Size = 40
    end
    object QPengeluaranJumlah: TBCDField
      DisplayWidth = 14
      FieldName = 'Jumlah'
      DisplayFormat = '#,0'
      Precision = 19
    end
  end
  object DsJamKerja: TDataSource
    DataSet = QJamKerja
    Left = 232
    Top = 288
  end
  object QJamKerja: TADOQuery
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select Jamkerja.* from jamkerja order by Jamkerja.id Desc '
      '')
    Left = 232
    Top = 336
    object QJamKerjaID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QJamKerjaNIP: TWideStringField
      FieldName = 'NIP'
      Size = 6
    end
    object QJamKerjaTanggal: TDateTimeField
      FieldName = 'Tanggal'
      DisplayFormat = 'dddd, dd mmmm yyyy'
    end
    object QJamKerjaMulai: TDateTimeField
      FieldName = 'Mulai'
      DisplayFormat = 'hh:mm'
    end
    object QJamKerjaSelesai: TDateTimeField
      FieldName = 'Selesai'
      DisplayFormat = 'hh:mm'
    end
    object QJamKerjaFee: TBCDField
      FieldName = 'Fee'
      DisplayFormat = '#,0'
      Precision = 19
    end
    object QJamKerjaNama: TStringField
      FieldKind = fkLookup
      FieldName = 'Nama'
      LookupDataSet = TOperator
      LookupKeyFields = 'ID'
      LookupResultField = 'Nama'
      KeyFields = 'NIP'
      Lookup = True
    end
  end
  object TOperator: TADOTable
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    TableName = 'Operator'
    Left = 352
    Top = 97
  end
  object DSOperator: TDataSource
    DataSet = TOperator
    Left = 352
    Top = 152
  end
  object QStartJamKerja: TADOQuery
    Connection = ADOConnection1
    Parameters = <
      item
        Name = 'nip'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 6
        Value = Null
      end
      item
        Name = 'tanggal'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'mulai'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      
        'insert into JamKerja (NIP,Tanggal,Mulai) values(:nip,:tanggal,:m' +
        'ulai)')
    Left = 352
    Top = 24
  end
  object QStopJK: TADOQuery
    Connection = ADOConnection1
    Parameters = <
      item
        Name = 'fee'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 19
        Size = 8
        Value = Null
      end
      item
        Name = 'selesai'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nip'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 6
        Value = Null
      end>
    SQL.Strings = (
      ''
      
        'UPDATE JamKerja SET jamkerja.fee =:fee, JamKerja.Selesai =:seles' +
        'ai  WHERE ( JamKerja.ID=:id) and (JamKerja.NIP=:nip)'
      '')
    Left = 416
    Top = 24
  end
  object DSRepGajiOp: TDataSource
    DataSet = QREpGajiOp
    Left = 304
    Top = 288
  end
  object QREpGajiOp: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    OnCalcFields = QREpGajiOpCalcFields
    Parameters = <
      item
        Name = 'NIP'
        DataType = ftString
        Size = 6
        Value = '001002'
      end
      item
        Name = 'awal'
        DataType = ftDateTime
        Size = 16
        Value = 36892d
      end
      item
        Name = 'akhir'
        DataType = ftDateTime
        Size = 5
        Value = 36892d
      end
      item
        Name = 'awal2'
        DataType = ftDateTime
        Size = 16
        Value = 36892d
      end
      item
        Name = 'akhir2'
        DataType = ftDateTime
        Size = 16
        Value = 36892d
      end
      item
        Name = 'NIP2'
        DataType = ftString
        Size = 6
        Value = '001002'
      end>
    SQL.Strings = (
      'SELECT     *'
      'FROM         (SELECT     tanggal, COUNT(*) AS JumlahTransaksi'
      '                       FROM          Pemasukan'
      
        '                       WHERE      (NIP =  :NIP)  AND (Tanggal BE' +
        'TWEEN :awal AND :akhir)'
      '                       GROUP BY tanggal) as tabel1 '
      'INNER JOIN'
      '                          (SELECT  *'
      '                            FROM   JamKerja'
      
        '                            WHERE      (Tanggal BETWEEN :awal2 A' +
        'ND :akhir2) AND (NIP =  :NIP2)) tabel2 ON tabel1.tanggal = tabel' +
        '2.Tanggal')
    Left = 304
    Top = 336
    object QREpGajiOptanggal: TDateTimeField
      FieldName = 'tanggal'
      DisplayFormat = 'dddd, dd/mm/yy'
    end
    object QREpGajiOpJumlahTransaksi: TIntegerField
      FieldName = 'JumlahTransaksi'
    end
    object QREpGajiOpID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QREpGajiOpNIP: TWideStringField
      FieldName = 'NIP'
      Size = 6
    end
    object QREpGajiOpMulai: TDateTimeField
      FieldName = 'Mulai'
      DisplayFormat = 'HH:MM'
    end
    object QREpGajiOpSelesai: TDateTimeField
      FieldName = 'Selesai'
      DisplayFormat = 'HH:MM'
    end
    object QREpGajiOpFee: TBCDField
      FieldName = 'Fee'
      DisplayFormat = '#,0'
      currency = True
      Precision = 19
    end
    object QREpGajiOpJam: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'Jam'
      Calculated = True
    end
    object QREpGajiOpMenit: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'Menit'
      Calculated = True
    end
  end
  object DSHARIANmasuk: TDataSource
    DataSet = QHarianMasuk
    Left = 420
    Top = 176
  end
  object DSEditTarif: TDataSource
    DataSet = QTarif
    Left = 400
    Top = 288
  end
  object DSPrinting: TDataSource
    DataSet = QPrinting
    Left = 480
    Top = 288
  end
  object QPrinting: TADOQuery
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from printing order by id asc')
    Left = 480
    Top = 344
  end
  object DSMember: TDataSource
    DataSet = QMember
    Left = 80
    Top = 416
  end
  object DSTarifWifi: TDataSource
    DataSet = TTarifWifi
    Left = 240
    Top = 424
  end
  object TTarifWifi: TADOTable
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    TableName = 'TarifWifi'
    Left = 312
    Top = 424
    object TTarifWifijam: TIntegerField
      FieldName = 'jam'
    end
    object TTarifWifireguler: TFloatField
      FieldName = 'reguler'
      DisplayFormat = '#,0'
    end
    object TTarifWifianggota: TFloatField
      FieldName = 'anggota'
      DisplayFormat = '#,0'
    end
    object TTarifWifiKeterangan: TStringField
      FieldName = 'Keterangan'
      FixedChar = True
      Size = 30
    end
  end
  object DsWTimer: TDataSource
    DataSet = TWifiTimer
    Left = 384
    Top = 424
  end
  object TWifiTimer: TADOTable
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    OnCalcFields = TWifiTimerCalcFields
    TableName = 'wifiTimer'
    Left = 456
    Top = 424
    object TWifiTimerid: TLargeintField
      FieldName = 'id'
    end
    object TWifiTimermulai: TDateTimeField
      FieldName = 'mulai'
      DisplayFormat = 'HH:mm'
    end
    object TWifiTimertipe: TSmallintField
      FieldName = 'tipe'
    end
    object TWifiTimerJenis: TStringField
      FieldKind = fkLookup
      FieldName = 'Jenis'
      LookupDataSet = TJenis
      LookupKeyFields = 'ID'
      LookupResultField = 'Nama'
      KeyFields = 'tipe'
      Lookup = True
    end
    object TWifiTimeridAnggota: TStringField
      FieldKind = fkCalculated
      FieldName = 'idAnggota'
      Size = 6
      Calculated = True
    end
    object TWifiTimerNama: TStringField
      FieldKind = fkLookup
      FieldName = 'Nama'
      LookupDataSet = tmember
      LookupKeyFields = 'ID'
      LookupResultField = 'Nama'
      KeyFields = 'id'
      Size = 25
      Lookup = True
    end
    object TWifiTimerlama: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'lama'
      Calculated = True
    end
    object TWifiTimerJam: TStringField
      FieldKind = fkCalculated
      FieldName = 'Jam'
      Size = 10
      Calculated = True
    end
  end
  object QMember: TADOQuery
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    OnCalcFields = TMemberCalcFields
    Parameters = <>
    SQL.Strings = (
      'select * from member order by member desc')
    Left = 136
    Top = 416
    object QMemberID: TLargeintField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QMemberNama: TStringField
      FieldName = 'Nama'
      FixedChar = True
      Size = 25
    end
    object QMemberMAC: TStringField
      FieldName = 'MAC'
      FixedChar = True
      Size = 17
    end
    object QMemberMember: TStringField
      FieldName = 'Member'
      FixedChar = True
      Size = 1
    end
    object QMemberIDAnggota: TStringField
      FieldKind = fkCalculated
      FieldName = 'IDAnggota'
      ReadOnly = True
      Size = 6
      Calculated = True
    end
    object QMemberstamp: TDateTimeField
      FieldName = 'stamp'
    end
  end
  object tmember: TADOTable
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    TableName = 'Member'
    Left = 144
    Top = 464
  end
end
